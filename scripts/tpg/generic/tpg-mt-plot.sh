#!/bin/sh
winSize=$1
phase=$2
numTask=$3
numAux=$4
i=0
if ls *p${phase}*.pdf 1> /dev/null 2>&1; then rm *p${phase}*.pdf; fi

for aux in `seq 1 $numAux`; do
   if [ $aux -eq 1 ]; then
      xlab="Fitness"
   elif [ $aux -eq 2 ]; then
      xlab="Teams per decision"
   else
     xlab="Instructions per decision"
   fi 
  for task in `seq 1 $numTask`; do
     i=$((i+1))
     Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg_aux_${aux}_ST_${task}_p${phase}.rslt "$xlab (single-task $task)" "$winSize" "$(printf "%03d" $i)_${task}-${aux}" 0
  done
done

for aux in `seq 1 $numAux`; do
   if [ $aux -eq 1 ]; then
      xlab="Fitness"
   elif [ $aux -eq 2 ]; then
      xlab="Teams per decision"
   else
     xlab="Instructions per decision"
   fi
  for task in `seq 1 $numTask`; do
     i=$((i+1))
     Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg_aux_${aux}_MT_${task}_p${phase}.rslt "$xlab (multi-task $task)" "$winSize" "$(printf "%03d" $i)_${task}-${aux}" 0
  done
done

for task in `seq 1 $numTask`; do
   i=$((i+1))
   Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-tCount-st-${task}.rslt "Teams per Graph (best single-task ${task})" "$winSize" "$(printf "%03d" $i)_st-${task}" 0
done

for task in `seq 1 $numTask`; do
   i=$((i+1))
   Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-pCount-st-${task}.rslt "Programs per Graph (best single-task ${task})" "$winSize" "$(printf "%03d" $i)_st-${task}" 0
done

for task in `seq 1 $numTask`; do
   i=$((i+1))
   Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-age-st-${task}.rslt "Age of Graph (best single-task ${task})" "$winSize" "$(printf "%03d" $i)_st-${task}" 0
done

i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-tCount-mt.rslt "Teams per Graph (best multi-task ${task})" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-pCount-mt.rslt "Programs per Graph (best multi-task ${task})" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-age-mt.rslt "Age of Graph (best multi-task ${task})" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-auxDouble_MT-minThresh.rslt "minThreshold" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-sRTC.rslt "Population-wide Instructions Executed" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-sGsz.rslt "Population-wide Teams Executed" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-genTimeCurve-sec.rslt "Seconds Total" "$winSize" "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-genTimeCurve-eval.rslt "Seconds Evaluation " "$winSize" "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-genTimeCurve-genTeams.rslt "Seconds Replacement" "$winSize" "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-genTimeCurve-selTeams.rslt "Seconds Selection" "$winSize" "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-genTimeCurve-rprt.rslt "Seconds Accounting & Reporting" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-genTimeCurve-tToEvl.rslt "Teams to Evaluate" "$winSize" "$(printf "%03d" $i)" 0
#i=$((i+1))
#Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-cpAFts.rslt "Cumulative pop-wide proportion of active features" "$winSize" "$(printf "%03d" $i)" 0
#i=$((i+1))
#Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-propType0.rslt "Proportion of Action-Value Programs (best graph)" "$winSize" "$(printf "%03d" $i)" 1 
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-osr.rslt "Offspring Survival Rate (numOldDeleted/numDeleted)" "$winSize" "$(printf "%03d" $i)" 0 
#i=$((i+1))
#Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-avp.rslt "Proportion Action-Value Programs in Population" "$winSize" "$(printf "%03d" $i)" 0
#i=$((i+1))
#Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-MODES-change.rslt "MODES - Change" 5 "$(printf "%03d" $i)" 0 
#i=$((i+1))
#Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-MODES-novelty.rslt "MODES - Novelty" 5 "$(printf "%03d" $i)" 0 
#i=$((i+1))
#Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-MODES-complexityA.rslt "MODES - ComplexityA" 5 "$(printf "%03d" $i)" 0 
#i=$((i+1))
#Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-MODES-complexityB.rslt "MODES - ComplexityB" 5 "$(printf "%03d" $i)" 0 
#i=$((i+1))
#Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-MODES-ecology.rslt "MODES - Ecology" 5 "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-meanPIns.rslt "Mean Instructions per Program (best graph)" "$winSize" "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-meanEPIns.rslt "Mean Effective Instructions per Program (best graph)" "$winSize" "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-policyFeatures.rslt "Features (best graph)" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-gt-Msize.rslt "Team Population Size" "$winSize" "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-gt-Lsize.rslt "Program Population Size" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-gt-MemSize.rslt "Memory Population Size" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-gt-Rsize.rslt "Root Population Size" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-gt-pAFts.rslt "Pop-wide proportion active features " "$winSize" "$(printf "%03d" $i)" 0
#i=$((i+1))
#Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-st-Msize.rslt "st Team population size" "$winSize" "$(printf "%03d" $i)" 0
#i=$((i+1))
#Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-st-Lsize.rslt "st Program population size" "$winSize" "$(printf "%03d" $i)" 0
#i=$((i+1))
#Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-st-Rsize.rslt "st Rsize" "$winSize" "$(printf "%03d" $i)" 0
#i=$((i+1))
#Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-st-mRsize.rslt "st mRoot population size" "$winSize" "$(printf "%03d" $i)" 0
#i=$((i+1))
#Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-st-MemSize.rslt "st Memory population size" "$winSize" "$(printf "%03d" $i)" 0
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-tmSizeRoot.rslt "Mean Root Team Size" "$winSize" "$(printf "%03d" $i)" 0 
i=$((i+1))
Rscript  $TPG_PATH/scripts/tpg/plot-tpg-trainingCurves.R tpg-tmSizeSub.rslt "Mean Sub Team Size" "$winSize" "$(printf "%03d" $i)" 0 

wd=$(echo $PWD | rev | cut -d '/' -f 1 | rev)
if [ $phase -eq 0 ]; then phs="train"; fi
if [ $phase -eq 2 ]; then phs="test"; fi

pdfunite *.pdf ${wd}_${phs}.pdf
rm 0*.pdf
rm *rslt
