#!/bin/bash
maxt=-1 #$2
winSize=1 #$3
phs=$1
numTask=1 #$5
numAux=3 #$6

if [ "$phs" = "train" ]; then
   phs=0
elif [ "$phs" = "test" ]; then
   phs=2
fi

c=1
if ls *rslt 1> /dev/null 2>&1; then rm *.rslt; fi
if ls *pdf 1> /dev/null 2>&1; then rm *.pdf; fi
maxT=$maxt
if [ $maxt -eq 0 ] 
then
   maxT=$(grep "gTime t " tpg.*.std | awk -F" t " '{print $2}' | awk '{print $1}' | sort -n | tail -n 1 | tr -d '\n')


elif [ $maxt -eq -1 ]
then
   files=$(ls tpg.*.std | grep -v replay)
   mt=""
   for f in $files; do
      mt="$(grep "gTime t " $f | awk -F" t " '{print $2}' | awk '{print $1}' | sort -n | tail -n 1 | tr -d '\n') $mt"
   done
   maxT=$(echo $mt | tr ' ' '\n' | sort -n | head -n 1)
fi

files=$(ls tpg.*.std | grep -v replay)

for f in $files
do
   echo "Processing $f ..."

   if [ "$phs" -eq 0 ]; then TT="Trn"; fi
   if [ "$phs" -eq 1 ]; then TT="Val"; fi
   if [ "$phs" -eq 2 ]; then TT="Tst"; fi

   #run time stats
   for aux in `seq 1 $numAux`; do
      for task in `seq 1 $numTask`; do
         echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsST " | grep "tsk $task " | grep " phs $phs " | head -n $maxT | awk -F"mnOut$TT" '{print $2}' | \
            awk -F"tsk_${task}_a" '{print $2}' | awk -v var=$(echo "$aux" | bc) '{print $var}' | tr '\n' ' ')  >> tpg_aux_${aux}_ST_${task}_p${phs}.rslt;
         echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F"mnOut$TT" '{print $2}' | \
            awk -F"tsk_${task}_a" '{print $2}' | awk -v var=$(echo "$aux" | bc) '{print $var}' | tr '\n' ' ')  >> tpg_aux_${aux}_MT_${task}_p${phs}.rslt;
      done
   done

  echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F"minThr" '{print $2}' | \
     awk '{print $1}' | tr '\n' ' ')  >> tpg-auxDouble_MT-minThresh.rslt

   #wall time
   echo $(tac $f | sed '/restart/q' | tac | grep "gTime " | head -n $maxT | awk -F" sec " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-genTimeCurve-sec.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "gTime " | head -n $maxT | awk -F" evl " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-genTimeCurve-eval.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "gTime " | head -n $maxT | awk -F" gTms " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-genTimeCurve-genTeams.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "gTime " | head -n $maxT | awk -F" sTms " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-genTimeCurve-selTeams.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "gTime " | head -n $maxT | awk -F" rprt " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-genTimeCurve-rprt.rslt

   #cumulative state
   echo $(tac $f | sed '/restart/q' | tac | grep "cpAFts " | head -n $maxT | awk -F"cpAFts " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-cpAFts.rslt

   echo $(grep "tToEvl " $f | head -n $maxT | awk -F" tToEvl " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-genTimeCurve-tToEvl.rslt

   for task in `seq 1 $numTask`; do
      echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsST " | grep "tsk $task " | grep " phs $phs " | head -n $maxT | awk -F " nP " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-pCount-st-${task}.rslt
      echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsST " | grep "tsk $task " | grep " phs $phs " | head -n $maxT | awk -F " nT " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-tCount-st-${task}.rslt
      echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsST " | grep "tsk $task " | grep " phs $phs " | head -n $maxT | awk -F "age" '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-age-st-${task}.rslt
   done

   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F " nP " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-pCount-mt.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F " nT " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-tCount-mt.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F "age" '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-age-mt.rslt   

   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F " mnProgIns " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-meanPIns.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F " mnEProgIns " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-meanEPIns.rslt

   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F " pF " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-policyFeatures.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F " pF " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-policyFeatures.rslt

   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F " mnTmSzR " '{print $2}' | awk '{print $1}') >> tpg-tmSizeRoot.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep "setElTmsMT " | grep " phs $phs " | head -n $maxT | awk -F " mnTmSzS " '{print $2}' | awk '{print $1}') >> tpg-tmSizeSub.rslt

   echo $(tac $f | sed '/restart/q' | tac | grep genTms | head -n $maxT | awk -F" Msz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-gt-Msize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep genTms | head -n $maxT | awk -F" Lsz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-gt-Lsize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep genTms | head -n $maxT | awk -F" mSz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-gt-MemSize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep genTms | head -n $maxT | awk -F" rSz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-gt-Rsize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep genTms | head -n $maxT | awk -F" pAFts " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-gt-pAFts.rslt

   echo $(tac $f | sed '/restart/q' | tac | grep selTms | head -n $maxT | awk -F" Msz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-st-Msize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep selTms | head -n $maxT | awk -F" Lsz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-st-Lsize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep selTms | head -n $maxT | awk -F" mSz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-st-MemSize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep selTms | head -n $maxT | awk -F" rSz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-st-Rsize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep selTms | head -n $maxT | awk -F" mrSz " '{print $2}' | awk '{print $1}' | tr '\n' ' ') >> tpg-st-mRsize.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep selTms | head -n $maxT | awk -F" nOldDelPr " '{print $2}' | awk '{print $1}') >> tpg-osr.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep selTms | head -n $maxT | awk -F" sRTC " '{print $2}' | awk '{print $1}') >> tpg-sRTC.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep selTms | head -n $maxT | awk -F" sGsz " '{print $2}' | awk '{print $1}') >> tpg-sGsz.rslt

   #MODES
   echo $(tac $f | sed '/restart/q' | tac | grep TPG::MODES | awk -F"change" '{print $2}' | awk '{print $1}') >> tpg-MODES-change.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep TPG::MODES | awk -F"novelty" '{print $2}' | awk '{print $1}') >> tpg-MODES-novelty.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep TPG::MODES | awk -F"complexityA" '{print $2}' | awk '{print $1}') >> tpg-MODES-complexityA.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep TPG::MODES | awk -F"complexityB" '{print $2}' | awk '{print $1}') >> tpg-MODES-complexityB.rslt
   echo $(tac $f | sed '/restart/q' | tac | grep TPG::MODES | awk -F"ecology" '{print $2}' | awk '{print $1}') >> tpg-MODES-ecology.rslt
done

$TPG_PATH/scripts/tpg/generic/tpg-mt-plot.sh $winSize $phs $numTask $numAux
