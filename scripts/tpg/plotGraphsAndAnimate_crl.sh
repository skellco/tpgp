#!/bin/bash
fps=$1
graphs=$(ls graphs | cut -d '.' -f 1);
for g in $graphs; do
   echo $g
   dot -Tpng:cairo:gd -Gsize=1200,1200\! -Gdpi=1 graphs/${g}.dot -o graphs/${g}.png
   #twopi -Tpng:cairo:gd -Gsize=1200,1200\! -Gdpi=1 graphs/${g}.dot -o graphs/${g}.png
   #neato -Tpng:cairo:gd -Gsize=1200,1200\! -Gdpi=1 graphs/${g}.dot -o graphs/${g}.png
   convert graphs/${g}.png -gravity center -background black -extent 1200x1200 graphs/${g}.png
done

ffmpeg -framerate $fps -pattern_type glob -i 'graphs/*.png' -c:v libx264 -pix_fmt yuv420p graphs/out.mp4
ffmpeg -framerate $fps -pattern_type glob -i 'frames/*.tga' -c:v libx264 -pix_fmt yuv420p frames/out.mp4
ffmpeg -i frames/out.mp4 -i graphs/out.mp4 -filter_complex hstack=inputs=2 output.mp4
