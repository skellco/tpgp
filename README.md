This is a very rough work-in-progress version of the Tangled Program Graph (TPG) algorithm as a generic library which you can link to application-specific main programs. 

This repo contains examples for Reinforcement Learning and Symbolic Regression.
More details and different versions of TPG in various languages can be found at the following links:

[Stephen Kelly's research site](http://stephenkelly.ca/?q=research)

[Malcolm Heywood's research site](https://web.cs.dal.ca/~mheywood/)

[GEGELATI is a more robust c++ version of TPG](https://github.com/gegelati/gegelati)

For information about this repository contact kellys27@msu.edu. The following provides a quick start for Linux.

## Directory Structure ##

    src             #source code
    src/cpp/env     #enviroments, currently 6 classic control problems
    src/cpp/exp     #experiment main methods
    scripts         #bash and R scripts for plotting results
    src/cpp/TPG     #the TPG algorithm as a static c++ library
    tpg-classicRL   #experiment parameters and run scripts (reinforcement learning)
    tpg-regression  #experiment parameters and run scripts (symbolic regression)

## Dependencies ##

	boost #sudo apt install libboost-all-dev
	scons #sudo apt install scons
	bz2 #sudo apt install libbz2-dev
	opengl #sudo apt install libglu1-mesa-dev freeglut3-dev mesa-common-dev
	openmpi #sudo apt install -y openmpi-bin
    R #sudo apt install r-base
    caTools #sudo apt-get install r-cran-catools
    xterm #sudo apt install xterm
    graphviz #sudo apt install graphviz
    ffmpeg #sudo apt install ffmpeg
    imagemagick #sudo apt install imagemagick

## Compile ##

	cd tpgp
    export TPG_PATH=`pwd` #add TPG_PATH to ~/.profile to make this permanent
    scons --opt 

## Running Experiments ##
### CartPole Memory Experiment ###

The command below will initiate a 5-process mpi experiment on the partially-observable (PO) CartPole problem. 
PO CartPole implies that the agents do not get velocity information about the pole or cart. See https://gym.openai.com/envs/CartPole-v0/ for details on CartPole.
This should complete in about 5 min. on a modern laptop.

    cd $TPG_PATH/tpg-classicRL
    ./tpg-mpi.sh 3 1 1 5 #arguments: <mode> <task> <seed> <num mpi process>

To skip the memory comparison and just evovle an agent with memory, use mode 0:

    cd $TPG_PATH/tpg-classicRL
    ./tpg-mpi.sh 0 1 1 5 #arguments: <mode> <task> <seed> <num mpi process>

In this case, the jobs will run in the background and evolution will continue indefinitely. To terminated the run use the following:
    
    pkill -f tpgExp 

### Plot Results ###

    $TPG_PATH/scripts/tpg/generic/tpg-mt-stats.sh train #arguments: <train/test>

The above will generate tpg-classicRL.pdf containing various metrics of interest.
The first page of results will be training curves. Since the environment is partially-observable, a TPG run with **temporal memory** will do much better. If you ran the memory experiment, 
theplots should look similar to the following: 

![CartPole training curve](img/cartpole-tc.png)

### Animate Best Program Graph ###

The command below will find the solution with the best test score and replay it in 1 episode of CartPole. 
Drawings of the CartPole and program graph at each timestep are saved in the replay directory.

    cd $TPG_PATH/tpg-classicRL
    ./tpg-mpi.sh 1 1 1 5 #arguments <mode> <task> <seed> <num mpi process>

To combine the replay frames into an animation video and save it to output.mp4:
    
    cd $TPG_PATH/tpg-classicRL/replay
    $TPG_PATH/scripts/tpg/plotGraphsAndAnimate_crl.sh 10 #arguments: <frames per second>
    

    
### Play Cartpole in Manual Mode ###

    cd $TPG_PATH
    ./build/release/cpp/env/classicRL/classicRLPlayer 1 1 200 #arguments: <task> <seed> <step delay in millisecons>

The above command will launch Cartpole in interactive mode.  Use left and right arrows to play Cartpole like a video game.
    
### Symbolic Regression ###

    cd $TPG_PATH/tpg-regression
    ./tpg-mpi.sh 1 5 1 2 #arguments: <seed> <num mpi process> <function> <num inputs>
  
### Plot Results ###

    $TPG_PATH/scripts/tpg/generic/tpg-mt-stats.sh rgr 0 10 #<prefix> <max generation, 0 for all> <runmean>
    
