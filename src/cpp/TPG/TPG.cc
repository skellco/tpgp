#include "TPG.h"

/********************************************************************************************/
TPG::TPG(int seed){
   //defaults
   _activeTask = -1;
   _animate = false;
   _continuousOutput = false;
   _checkpoint = false;
   _checkpointInPhase = 0;
   _dim = 1;
   _diversityMode = 0;
   _distMode = 0;
   _disR = uniform_real_distribution<>(0.0, 1.0);
   _fitMode = 0;
   _function = 0;
   _hostToReplay = -1;
   _id = -1;
   _knnNov = 15;
   _programCount = 0;
   _maxGenMillis = 1.0;
   _memoryCount = 0;
   _numActions = 2;
   _numFitMode = 1;
   _numPointAuxDouble = 0;
   _numProfilePoints = 0;
   _numTask = 1;
   _teamCount = 0;
   _partiallyObservable = false;
   _pointCount = 0;
   _replay = false;
   _reproduce = false;
   _seed = seed;
   _seed2 = 7777;
   _skipIntrons = true;
   _stateful = false;
   _taskSwitchMod = 1;
   _tCurrent = 0;
   _tMain = 0;
   _tPickup = 0;
   _tStart = 1;
   _verbose = false;
   _visual = false;
   _writeCheckpoints = false;
}

TPG::~TPG(){
}

/********************************************************************************************/
void TPG::checkRefCounts(const char *msg){
   int nrefs = 0;
   int sumTeamSizes = 0;

   for(auto leiter = _L.begin(); leiter != _L.end(); leiter++)
      nrefs += (*leiter)->refs();
   for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
      sumTeamSizes += (*teiter)->size();
   if(sumTeamSizes != nrefs){
      cerr << "nrefs " << nrefs << " sumTeamSize " << sumTeamSizes << endl;
      die(__FILE__, __FUNCTION__, __LINE__, msg);
   }
}

/********************************************************************************************/
void TPG::countRefs()
{
   int nrefs = 0;
   int sumTeamSizes = 0;
   int sumNumOutcomes = 0;

   oss << "TPG::debugRefs ";
   oss << _L.size() << " programs, " << _M.size() << " teams";
   for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
   {
      sumTeamSizes += (*teiter)->size();
      sumNumOutcomes += (*teiter)->numOutcomes(_TRAIN_PHASE, -1, -1);
   }

   for(auto leiter = _L.begin(); leiter != _L.end(); leiter++)
      nrefs += (*leiter)->refs();

   oss << ", sumTeamSizes " << sumTeamSizes << ", nrefs " << nrefs << ", sumNumOutcomes " << sumNumOutcomes;
   oss << endl;

   if(sumTeamSizes != nrefs)
      die(__FILE__, __FUNCTION__, __LINE__, "something messed up during cleanup");
}

/***********************************************************************************************************/
void TPG::finalize()
{
   _Mroot.clear();
   _teamMap.clear();
   _phyloGraph.clear();

   for(auto teiter = _M.begin(); teiter != _M.end(); teiter++){
      (*teiter)->resetOutcomes(-1);
      delete *teiter;
   }
   _M.clear();

   for(auto leiter = _L.begin(); leiter != _L.end(); leiter++)
      delete *leiter;
   _L.clear();

   for(auto meiter = _Memory.begin(); meiter != _Memory.end(); meiter++)
      delete *meiter;
   _Memory.clear();

   _phyloGraph.clear();
   _genTimes.clear();
   _miniBatchIndexes.clear();
}

/********************************************************************************************/
void TPG::genTeams(long t, mt19937 &rng)
{
   vector < team * > parent1, parent2;
   team *pm1, *pm2;
   team *cm;

   getTeams(parent1, true);//roots only
   getTeams(parent2, true);
   uniform_int_distribution<int> dis1(0,parent1.size()-1);
   uniform_int_distribution<int> dis2(0,parent2.size()-1);

   size_t numNewTeams = 0;
   while (_Mroot.size() < _eliteTeams.size() + _Rsize){
      pm1 = pm2 = parent1[dis1(rng)];

      ////////////////////////////////////////////////////////////////////////////////////////
      ////get nodes and pick a sub team
      //set <team*, teamIdComp> visitedTeams;
      //pm1->getAllNodes(_teamMap, visitedTeams, -1); 
      //uniform_int_distribution<int> dis(0,visitedTeams.size()-1);
      //auto it = visitedTeams.begin();
      //advance(it, dis(rng));
      //team* teamToMu = *it;

      ////get nodes up to the sub team
      //pm1->getAllNodes(_teamMap, visitedTeams, teamToMu->id());
      ////clone this top section and add to pops

      ////mutate the team

      ////attach the cloned top section

      ////////////////////////////////////////////////////////////////////////////////////////
      bool crossover = _disR(rng) < _pmx ? true : false;
      if (crossover){
         do
         {
            pm2 = parent2[dis1(rng)];
         } 
         while (pm1->id() == pm2->id());
      }
      genTeams(t, pm1, pm2, crossover, &cm, numNewTeams, rng);
      _phyloGraph[pm1->id()].adj.push_back(cm->id());
      if (crossover)
         _phyloGraph[pm2->id()].adj.push_back(cm->id());
      _phyloGraph.insert(pair<long, phyloRecord>(cm->id(), phyloRecord()));
      _phyloGraph[cm->id()].gtime = t;
      _phyloGraph[cm->id()].root = true;
      _M.insert(cm);
      _Mroot.insert(cm);
      _teamMap[cm->id()] = cm;
      numNewTeams++;
   }

   set<long> features;
   policyFeatures(-1, features, true);
   oss << "genTms t " << t << " Msz " << _M.size() << " Lsz " << _L.size() << " rSz " << _Mroot.size() << " mSz " << _Memory.size() << " eLSz " << _eliteTeams.size();
   oss << " nNTms " << numNewTeams << " pAFts " << (double)features.size()/_dim << endl;
}

/********************************************************************************************/
void TPG::genTeams(long t, team *pm1, team *pm2, bool crossover, team **cm, size_t &numNewTeams, mt19937 &rng)
{
   list < program *> * p1programs;
   list < program *> * p2programs;
   list < program * > *cprograms;

   double b;

   program *lr;

   bool changedL;
   bool changedM;

   uniform_int_distribution<int> disL(0, _L.size()-1);
   auto leiter = _L.begin();

   pm1->getMembersRef(p1programs);
   auto p1liter = p1programs->begin();
   *cm = new team(t, _teamCount++);

   (*cm)->addAncestorId(pm1->id());

   if (crossover){
      (*cm)->addAncestorId(pm2->id());
      pm2->getMembersRef(p2programs);
      auto p2liter = p2programs->begin();
      while (p1liter != p1programs->end() || p2liter != p2programs->end()){
         if (p1liter != p1programs->end() && 
               (((*p1liter)->action() < 0 && (*cm)->numAtomic() < 1) || 
                find(p2programs->begin(), p2programs->end(), *p1liter) != p2programs->end()))
            (*cm)->addProgram(*p1liter);
         else 
            if (p1liter != p1programs->end() && _disR(rng) < 0.5)
               (*cm)->addProgram(*p1liter);

         if (p2liter != p2programs->end() && _disR(rng) < 0.5)
            (*cm)->addProgram(*p2liter);

         if (p1liter != p1programs->end())
            p1liter++;
         if (p2liter != p2programs->end())
            p2liter++;
      }
      if ((*cm)->numAtomic() < 1)
         die(__FILE__, __FUNCTION__, __LINE__, "Crossover must leave the fail-safe atomic program!");
   }
   else
      for(p1liter = p1programs->begin(); p1liter != p1programs->end(); p1liter++)
         (*cm)->addProgram(*p1liter);

   (*cm)->getMembersRef(cprograms);
   auto cliter = cprograms->begin();

   uniform_int_distribution<int> disM(0,_M.size()-1);
   auto teiter = _M.begin();

   // Remove programs.
   for(b = 1.0; _disR(rng) < b && ((*cm)->size() > 2); b = b * _pmd) {
      uniform_int_distribution<int> disCprograms(0,cprograms->size()-1);
      do { 
         cliter = cprograms->begin();
         advance(cliter, disCprograms(rng));
      }
      while(((*cliter)->action() < 0 && (*cm)->numAtomic() < 2) || //keep at least one program with an atomic action
            ((*cm)->size() <= 2)); //keep at least two programs

      (*cm)->removeProgram(*cliter);
   }

   // Add programs.
   for(b = 1.0; _disR(rng) < b && (*cm)->size() < _maxTeamSize; b = b * _pma) {
      uniform_int_distribution<int> disTmSize(0,(*cm)->size()-1);
      do 
      { 
         leiter = _L.begin();
         advance(leiter, disL(rng));
      }
      while((*cm)->addProgram(*leiter, disTmSize(rng)) == false);
   }

   // Change program order.
   if (_disR(rng) < _pmw)
   {
      int i, j;
      uniform_int_distribution<int> disMemberList(0,(*cm)->size()-1);
      do
      {
         i = disMemberList(rng);
         j = disMemberList(rng);
      }
      while(i == j); 
      (*cm)->muProgramOrder(i, j);
   }

   // Mutate programs.

   auto memiter = _Memory.begin();

   changedM = false;

   set < team *, teamIdComp > cmTeams;
   set < program *, programIdComp > cprogramsCopy;
   (*cm)->members(cprogramsCopy);//need tp copy for cloning/removing

   while (changedM == false)
      for(auto ccliter = cprogramsCopy.begin(); ccliter != cprogramsCopy.end(); ++ccliter)
         if (_disR(rng)) {
            changedM = true;

            (*cm)->removeProgram(*ccliter);

            (*cm)->getAllNodes(_teamMap, cmTeams, -1, true);

            //clone program
            lr = new linearM(t, *(dynamic_cast<linearM*>(*ccliter)), _programCount++);
            if (lr->action() >= 0)
               _teamMap[(*ccliter)->action()]->inDeg(_teamMap[(*ccliter)->action()]->inDeg() + 1); //only for path programs
            lr->memSet((*ccliter)->memGet()); //copy memory reference
            lr->memGet()->refInc();

            //modify program
            do {
               changedL = lr->muBid(_pBidDelete, _pBidAdd, _pBidSwap, _pBidMutate, _maxProgSize , rng, _disR);
            }
            while(changedL == false);

            //change memory pointer
            if(_disR(rng) < _pms){
               uniform_int_distribution<int> disMemory(0,_Memory.size()-1);
               memory * memNew;
               do {
                  memiter = _Memory.begin();
                  advance(memiter,disMemory(rng));
                  memNew = *memiter;
               } while(lr->memGet()->id() == memNew->id());
               lr->memGet()->refDec();
               lr->memSet(memNew);
               lr->memGet()->refInc();
            }

            //change action pointer
            uniform_int_distribution<int> disAct(0,numAtomicActions()-1);
            if (_disR(rng) < _pmn){
               long a;
               //atomic
               if (t == 1 || //first generation is atomic
                     cmTeams.size() >= _maxTeamsPerGraph || //don't let graph grow larger than _maxTeamsPerGraph
                     (lr->action() < 0 && (*cm)->numAtomic() < 2) || //always mutate the fail-safe atomic program to another atomic
                     _disR(rng) < _pAtomic) {
                  //changing atomic actions has no effect under continous outputs
                  if (!_continuousOutput) {
                     do {
                        a = -1 - disAct(rng);//atomic actions are negatives: -1 down to -numAtomicActions()
                     } while (lr->action() == a);
                     if (lr->action() >= 0)
                        _teamMap[lr->action()]->inDeg(_teamMap[lr->action()]->inDeg() - 1);
                     lr->muAction(a);
                  }
               }
               //path
               else {
                  do {
                     teiter = _M.begin();
                     advance(teiter, disM(rng));
                  } while((*teiter)->gtime() == t || lr->action() == (*teiter)->id());//avoid pointing to a team created this generation or making a redundant change

                  if (lr->action() >= 0)
                     _teamMap[lr->action()]->inDeg(_teamMap[lr->action()]->inDeg() - 1);
                  if (!(*teiter)->root()) {//already subsumed, don't clone
                     lr->muAction((*teiter)->id());
                     (*teiter)->inDeg((*teiter)->inDeg() + 1);
                  }
                  else {//clone when subsumed
                     (*teiter)->prunePrograms();
                     team *sub = new team(t, _teamCount++); //(*teiter)->clone(t, _teamCount++);
                     (*teiter)->clone(&sub);
                     lr->muAction(sub->id());
                     sub->inDeg(1);
                     sub->root(false);
                     _phyloGraph[(*teiter)->id()].adj.push_back(sub->id());
                     _phyloGraph.insert(pair<long, phyloRecord>(sub->id(), phyloRecord()));
                     _phyloGraph[sub->id()].gtime = t;
                     _phyloGraph[sub->id()].root = false;
                     _M.insert(sub);
                     _teamMap[sub->id()] = sub;
                     numNewTeams++;
                  }
               }
            }
            (*cm)->addProgram(lr);
            _L.insert(lr);
         }
   for (cliter = cprograms->begin(); cliter != cprograms->end(); cliter++)
      (*cliter)->refInc();

   //for (auto teiter = _M.begin(); teiter != _M.end(); teiter++){
   //   cout << "depthTally " << setprecision(5) << (*teiter)->depthTally() << endl;
   //   //(*teiter)->clearDepthTally();
   //}
}

/********************************************************************************************/
//Must call setSingleTaskFitness first.
team * TPG::getBestTeam(){

   set < team *, teamFitnessLexicalCompare > teams;

   for (auto teiter = _Mroot.begin(); teiter != _Mroot.end(); teiter++)
      teams.insert(*teiter);

   return *teams.begin();
}

/********************************************************************************************/
void TPG::setEliteTeams(long t, int phase, bool verbose){
   double d;
   size_t numElite = _numElite > 0 ? _numElite : 1;
   _eliteTeams.clear();
   vector <double> minScoresST;
   for (size_t i = 0; i < _numTask; i++) minScoresST.push_back(numeric_limits<double>::max());
   vector <double> maxScoresST;
   for (size_t i = 0; i < _numTask; i++) maxScoresST.push_back(numeric_limits<double>::lowest());

   set <team*, teamFitnessLexicalCompare> teamsRanked;

   //Find the elite single-task policies relative to each objective
   for (size_t o = 0; o < _numTask; o++){

      teamsRanked.clear();
      for(auto teiter = _Mroot.begin(); teiter != _Mroot.end(); teiter++)
         if ((*teiter)->numOutcomes(phase,o) > 0){
            (*teiter)->fit((*teiter)->getMeanOutcome(phase, o, _fitMode, _fitMode,false,d));
            teamsRanked.insert(*teiter);
         }
      if (teamsRanked.size() > 0){
         size_t c = 0;
         for(auto teiter = teamsRanked.begin(); teiter != teamsRanked.end() && c < numElite; teiter++){
            auto ret = _eliteTeams.insert(*teiter);
            if (ret.second && phase == _TRAIN_PHASE){
               (*teiter)->fitnessBin(t, o);
               _phyloGraph[(*teiter)->id()].fitnessBin = (*teiter)->fitnessBin();
            }
            c++;
         }
         _eliteTeamsEachTask[phase][o] = *(teamsRanked.begin());
         if (verbose){
            oss << "setElTmsST t " << t << " phs " << phase << " tsk " << o+1 << " id " << _eliteTeamsEachTask[phase][o]->id() << " ";
            printTeamInfo(t, phase, false, _eliteTeamsEachTask[phase][o]->id());
         }
         maxScoresST[o] = _eliteTeamsEachTask[phase][o]->getMeanOutcome(phase, o, _fitMode, _fitMode,false,d);
         minScoresST[o] = (*(teamsRanked.rbegin()))->getMeanOutcome(phase, o, _fitMode, _fitMode,false,d);
      }
   }

   // Find the team with the highest minimum normalized score over all objectives
   teamsRanked.clear();
   vector <double> normalizedScores;
   double rawMeanScore;
   for(auto teiter = _Mroot.begin(); teiter != _Mroot.end(); teiter++){
      normalizedScores.clear();
      for (size_t o = 0; o < _numTask; o++){
         if ((*teiter)->numOutcomes(phase,o) > 0){
            rawMeanScore = (*teiter)->getMeanOutcome(phase, o, _fitMode, _fitMode,false,d);
            if (!isEqual(minScoresST[o], maxScoresST[o]))
               normalizedScores.push_back((rawMeanScore - minScoresST[o])/(maxScoresST[o] - minScoresST[o])); 
            else 
               normalizedScores.push_back(0);
         }
      }
      if (normalizedScores.size() == _numTask){  
         (*teiter)->fit(*min_element(normalizedScores.begin(), normalizedScores.end()));
         teamsRanked.insert((*teiter));
      }  
   }
   if (teamsRanked.size() > 0){
      size_t c = 0;
      for(auto teiter = teamsRanked.begin(); teiter != teamsRanked.end() && c < numElite; teiter++){
         auto ret = _eliteTeams.insert(*teiter);
         if (ret.second && phase == _TRAIN_PHASE){
            (*teiter)->fitnessBin(t, -1);//-1 is code for multi-task bin
            _phyloGraph[(*teiter)->id()].fitnessBin = (*teiter)->fitnessBin();
         }
         c++; 
      }
      _eliteTeamMT[phase] = *(teamsRanked.begin());
      if (_numTask == 1 && _eliteTeamMT[phase]->id() != _eliteTeamsEachTask[phase][0]->id()){
         cerr << "WARNING elite ST and MT mismatch t " << t << " _numFitMode " << _numFitMode;
         for (size_t o = 0; o < _numTask; o++){
            cerr << " minSTObj " << o << " " << minScoresST[o] << " maxSTObj " << o << " " << maxScoresST[o];
            cerr << " eMTId " << _eliteTeamMT[phase]->id() << " eMTrew " << _eliteTeamMT[phase]->getMeanOutcome(phase, o, _fitMode, _fitMode,false,d) << " eMTFit " << _eliteTeamMT[phase]->fit(); 
            cerr << " eSTId " << _eliteTeamsEachTask[phase][0]->id() << " eSTrew " << _eliteTeamsEachTask[phase][0]->getMeanOutcome(phase, o, _fitMode, _fitMode,false,d) << " eSTFit " << _eliteTeamsEachTask[phase][0]->fit() << endl;
         }
         die(__FILE__, __FUNCTION__, __LINE__, "WTF elite ST and MT mismatch!");
      }
      if (verbose){
         oss << "setElTmsMT t " << t << " phs " << phase << " id " << _eliteTeamMT[phase]->id() << " minThr " << _eliteTeamMT[phase]->fit() << " ";
         printTeamInfo(t, phase, false, _eliteTeamMT[phase]->id());
      }
   }
   if (_numElite == 0)
      _eliteTeams.clear();
}

/*********************************************************************************************
 * Helper struct for distance comparisons.
 */

struct distanceInstance {
   double distance;
   bool fromArchive;
   distanceInstance(double d,bool a):distance(d),fromArchive(a) {}
} ;

bool compareByDistance(const distanceInstance &a, const distanceInstance &b)
{
   return a.distance < b.distance;
}

/********************************************************************************************/
void TPG::initTeams(mt19937 &rng)
{
   long a1;
   team *m;
   program *l;
   memory *mem1;

   // Number of teams to initialize.
   int keep = _Rgap < 1.0 ? _Rsize - floor(_Rsize * _Rgap) : _Rsize * 2;

   int tc;

   size_t i, j, k;

   vector < program * > programvec;

   size_t size;

   set < program *, programIdComp > lused;

   size_t tsize = 10;

   uniform_int_distribution<int> disA(0, numAtomicActions() - 1);

   for(tc = 0; tc < keep; tc++)
   {
      a1 = -1;

      // Create a new team containing two programs and two stateful memory banks.

      m = new team(1, _teamCount++);

      for (size_t n = 0; n < _initialTeamSize; n++){

         //pick a different atomic action
         long a1Prev = a1;
         do
         {
            a1 = -1 - disA(rng); //atomic actions are negatives -1 to -numAtomicActions()
         } while (a1 == a1Prev);

         mem1 = new memory(_memoryCount++);
         _Memory.insert(mem1);

         l = new linearM(1, a1, _maxInitialProgSize, _dim, _programCount++, rng);
         l->memSet(mem1);
         l->memGet()->refInc();
         l->stateful(_stateful);
         m->addProgram(l);
         l->refInc();
         _L.insert(l);
      }

      //genUniqueProgram(l, _L);  //Make sure bid unique.

      _M.insert(m);
      _Mroot.insert(m);
      _phyloGraph.insert(pair<long, phyloRecord>(m->id(), phyloRecord()));
      _phyloGraph[m->id()].gtime = 0;
      _teamMap[m->id()] = m;

#ifdef MYDEBUG
      oss << "TPG::initTeams added " << *m << endl;
#endif
   }

   // Mix the programs up.
   programvec.insert(programvec.begin(), _L.begin(), _L.end());//could eliminate programvec someday

   uniform_int_distribution<int> disI(4, _maxInitialTeamSize);

   for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
   {
      // Get the programs in the team.
      lused.clear();
      (*teiter)->members(&lused);
      // Get the final team size, make sure not too big.
      size = disI(rng);
      if(size > programvec.size() - (*teiter)->size())
         size = programvec.size() - (*teiter)->size();

      // Build the team up.
      size_t numTries = programvec.size()*2;
      size_t t = 0;
      uniform_int_distribution<int> disLV(0, programvec.size() -1);
      while((*teiter)->size() < size && t <= numTries){
         // Get first program, make sure not in the team

         i = disLV(rng);

         while(lused.find(programvec[i]) != lused.end())
            i = (i + 1) % programvec.size();

         for(k = 0; k < tsize; k++){
            // Get another program, make sure not in the team

            j = disLV(rng);

            while(lused.find(programvec[j]) != lused.end())
               j = (j + 1) % programvec.size();

            // Pick second program if it has fewer refs.
            if(programvec[j]->refs() < programvec[i]->refs())
               i = j;
         }
         (*teiter)->addProgram(programvec[i]);
         lused.insert(programvec[i]);
         programvec[i]->refInc();
      }
      (*teiter)->shuff(rng);
   }
}

/**********************************************************************************************************/
void TPG::policyFeatures(int hostId, set<long> &features, bool active){
   features.clear();
   set <team*, teamIdComp> visitedTeams;
   if (hostId == -1){
      for (auto it = _Mroot.begin(); it != _Mroot.end(); it++){
         visitedTeams.clear();
         (*it)->policyFeatures(_teamMap, visitedTeams, features, active);
      }
   }
   else
   _teamMap[hostId]->policyFeatures(_teamMap, visitedTeams, features, active);
}

/**********************************************************************************************************

  Print program graph defined by <rootTeam> in DOT format for visualization with GraphViz

*/
void TPG::printGraphDot(team* rootTeam, 
      size_t frame, 
      int episode, 
      int step, 
      size_t depth, 
      vector <program*> allPrograms, 
      vector <program*> winningPrograms, 
      vector < set <long> > decisionFeatures, 
      vector< set< memory*, memoryIdComp> > decisionMemories, 
      vector <team*> teamPath, 
      bool drawPath){

   //unused arguments
   (void)decisionFeatures;
   (void)decisionMemories;
   (void)allPrograms;

   //just use winning programs up to a specific graph depth
   //vector<program*> winningProgramsDepth(winningPrograms.begin(), winningPrograms.begin()+depth);

   vector<program*> winningProgramsDepth(winningPrograms.begin(), winningPrograms.end());

   //default sizes
   double nodeWidth = 2.0;//0.1;//0.5;//2.0;
   double edgeWidth_1 = 1;//0.1;//1;//5;
   double edgeWidth_2 = 30;//10;//30;
   double arrowSize_1 = 1;//0.1;//1;//0.1;
   double arrowSize_2 = 1;//0.1;//1;//0.2;

   char outputFilename[80];
   ofstream ofs;

   set <team*, teamIdComp> teams;
   set <program*, programIdComp> programs;
   set <memory*, memoryIdComp> memories;

   rootTeam->getAllNodes(_teamMap, teams, programs, memories, false);

   sprintf(outputFilename, "replay/graphs/gv_%d_%05d_%03d_%05d_%05d%s", (int)rootTeam->id(), (int)frame, episode, step, (int)depth, ".dot");
   ofs.open(outputFilename, ios::out);
   if (!ofs)
      die(__FILE__, __FUNCTION__, __LINE__, "Can't open file.");

   ofs << "digraph G {" << endl;
   ofs << "ratio=1" << endl;
   ofs << "root=t_" << rootTeam->id() << endl;

   ////atomic actions
   //for(auto leiter = programs.begin(); leiter != programs.end(); leiter++)
   //   if ((*leiter)->action() < 0)
   //      ofs << " a_" << ((*leiter)->action()*-1)-1 << "_" << (*leiter)->id() << " [shape=point, label=\"\", regular=1, width=0.1]" << endl;

   //programs
   for(auto leiter = programs.begin(); leiter != programs.end(); leiter++){
      if (step > 0 && find(winningProgramsDepth.begin(), winningProgramsDepth.end(), *leiter) != winningProgramsDepth.end()){
         //ofs << " p_" << (*leiter)->id() << " [shape=box, style=filled, color=black, label=\"\", fontsize=200, regular=1, width=" << nodeWidth << "]" << endl;
         //ofs << " p_" << (*leiter)->id() << " [shape=box, style=filled, color=green, label=\"\", fontsize=200, regular=1, width=" << nodeWidth << "]" << endl;
      }
      else if (step > 0 && find(allPrograms.begin(), allPrograms.end(), *leiter) != allPrograms.end())
         ofs << " p_" << (*leiter)->id() << " [shape=box, style=filled, color=black, label=\"\", fontsize=200, regular=1, width=" << nodeWidth << "]" << endl;
      else if (teamPath.size() > 0)
         ofs << " p_" << (*leiter)->id() << " [shape=box, style=filled, color=grey90, label=\"\", fontsize=200, regular=1, width=" << nodeWidth << "]" << endl;
      else
         ofs << " p_" << (*leiter)->id() << " [shape=box, style=filled, color=grey70, label=\"\", fontsize=200, regular=1, width=" << nodeWidth << "]" << endl;
   }

   //teams
   //int label = 0;
   for(auto teiter = teams.begin(); teiter != teams.end(); teiter++)
      if ((*teiter)->id() == rootTeam->id() || (step > 0 && find(teamPath.begin(), teamPath.end(), *teiter) != teamPath.end()))
         //ofs << " t_" << (*teiter)->id() << " [shape=circle, style=filled, fillcolor=black, label=\"  "<< label ++ <<", fontsize=84, regular=1, width=" << nodeWidth << "]" << endl;
         //ofs << " t_" << (*teiter)->id() << " [shape=circle, style=filled, fillcolor=black, label=\"t" << label++ << "\", fontsize=84, regular=1, width=" << nodeWidth*2 << "]" << endl;
         ofs << " t_" << (*teiter)->id() << " [shape=circle, style=filled, fillcolor=black, label=\"\", fontsize=84, regular=1, width=" << nodeWidth*2 << "]" << endl;
      else
         //ofs << " t_" << (*teiter)->id() << " [shape=circle, style=filled, fillcolor=grey90, label=\"\", fontsize=84, regular=1, width=" << nodeWidth*2 << "]" << endl;
         ofs << " t_" << (*teiter)->id() << " [shape=circle, style=filled, fillcolor=deepskyblue, label=\"\", fontsize=84, regular=1, width=" << nodeWidth*5 << "]" << endl;

   ////memory registers
   //for(auto meiter = memories.begin(); meiter != memories.end(); meiter++)
   //   ofs << " m_" << (*meiter)->id() << " [shape=invhouse, style=filled, fillcolor=grey, label=\"\", regular=1, width=" << nodeWidth << "]" << endl;

   //program -> team edges
   for(auto leiter = programs.begin(); leiter != programs.end(); leiter++){
      if ((*leiter)->action() >= 0){
         double w = find(winningProgramsDepth.begin(), winningProgramsDepth.end(), *leiter) == winningProgramsDepth.end() || !drawPath ? edgeWidth_1 : edgeWidth_2;
         if (teamPath.size() < 1)
            w = 5;
         double as = find(winningProgramsDepth.begin(), winningProgramsDepth.end(), *leiter) == winningProgramsDepth.end() || !drawPath ? arrowSize_1 : arrowSize_2;
         string col = find(winningProgramsDepth.begin(), winningProgramsDepth.end(), *leiter) == winningProgramsDepth.end() ? "black" : "green";
         ofs << " p_" << (*leiter)->id() << "->" << "t_"<< (*leiter)->action() << " [arrowsize=" << as << ", penwidth=" << w << " color=" << col.c_str() << "];" << endl;
      }
   }

   ////program -> memory edges
   //for(auto leiter = programs.begin(); leiter != programs.end(); leiter++){
   //   double w = find(winningProgramsDepth.begin(), winningProgramsDepth.end(), *leiter) == winningProgramsDepth.end() ? edgeWidth_1 : edgeWidth_2;
   //   double as = find(winningProgramsDepth.begin(), winningProgramsDepth.end(), *leiter) == winningProgramsDepth.end() ? arrowSize_1 : arrowSize_2;
   //   ofs << " p_" << (*leiter)->id() << "->" << "m_"<< (*leiter)->memGet()->id();
   //      ofs << " [dir=both, arrowsize=" << as << ", penwidth=" << w << "];" << endl;
   //}

   //team -> program edges
   for(auto teiter = teams.begin(); teiter != teams.end(); teiter++){
      list < program * > mem;
      (*teiter)->members(&mem);
      for(auto leiter = mem.begin(); leiter != mem.end(); leiter++){
         double w = find(teamPath.begin(), teamPath.end(), *teiter) == teamPath.end() || !drawPath ? edgeWidth_1 : edgeWidth_2;
         if (teamPath.size() < 1)
            w = 5;
         double as = find(teamPath.begin(), teamPath.end(), *teiter) == teamPath.end() || !drawPath ? arrowSize_1 : arrowSize_2;
         string col = find(winningProgramsDepth.begin(), winningProgramsDepth.end(), *leiter) == winningProgramsDepth.end() ? "black" : "green";
         ofs << " t_" << (*teiter)->id() << "->p_" << (*leiter)->id() << " [arrowsize=" << as  << ", penwidth=" << w << " color=" << col.c_str() << "];" << endl;
      }
   }
   ofs << "}" << endl;
   ofs.close();
}

/**********************************************************************************************************/
void TPG::printPhyloGraphDot(){

   double nodeWidth = 10.0;
   double edgeWidth_1 = 0.1;//5;
   //double edgeWidth_2 = 30;
   //double arrowSize_1 = 2;//0.1;
   //double arrowSize_2 = 1;//0.2;

   char outputFilename[80];
   ofstream ofs;

   sprintf(outputFilename, "phyloGraphs/phylo_t_%05d%s", (int)_tCurrent, ".dot");
   ofs.open(outputFilename, ios::out);
   if (!ofs)
      die(__FILE__, __FUNCTION__, __LINE__, "Can't open file.");

   ofs << "digraph G {" << endl;
   ofs << "ratio=1" << endl;

   for (auto it = _phyloGraph.begin(); it != _phyloGraph.end(); it++){
      //if ((*it).second.adj.size() > 0){
      ofs << "subgraph {"<< endl;

      string col = "pink";
      if (_phyloGraph[(*it).first].fitnessBin == 0)
         col = "blue";
      else if (_phyloGraph[(*it).first].fitnessBin == 1)
         col = "red";
      else if (_phyloGraph[(*it).first].fitnessBin == 2)
         col = "green";
      else if (_phyloGraph[(*it).first].fitnessBin == 3)
         col = "yellow";
      else if (_phyloGraph[(*it).first].fitnessBin == 4)
         col = "orange";
      else if (_phyloGraph[(*it).first].fitnessBin == 5)
         col = "magenta";

      ofs << " t_" << (*it).first << " [shape=circle, style=filled, color=" << col.c_str() << ", label=\"t" << (*it).first << "\", fontsize=84, regular=1, width=" << nodeWidth << "]" << endl;

      if ((*it).second.adj.size() > 0)
         for (size_t i = 0; i < (*it).second.adj.size(); i++)
            ofs << " t_" << (*it).first << "->" << "t_"<< (*it).second.adj[i] << " [penwidth=" << edgeWidth_1 << " color=" << "gray77" << "];" << endl;
      ofs << "}" << endl;
   }
   //}
   ofs << "}" << endl;
   ofs.close();
}

/**********************************************************************************************************/
void TPG::printProgramInfo(){
   oss << "printProgramInfo start" << endl;
   for(auto leiter = _L.begin(), leiterEnd = _L.end(); leiter != leiterEnd; leiter++)
      oss << (*leiter)->checkpoint(false) << endl;
   oss << "printProgramInfo end" << endl;
}

/**********************************************************************************************************/
void TPG::printTeamInfo(long t, int phase, bool singleBest, long teamId){
   double d;
   team * bestTeam = *(_Mroot.begin());
   if (singleBest && teamId == -1)
      bestTeam = getBestTeam();
   ostringstream tmposs;
   map < point *, double, pointLexicalLessThan > allOutcomes;
   //map < point *, double > :: iterator myoiter;
   vector <int> behaviourSequence;
   set <team*, teamIdComp> visitedTeams;
   for(auto teiter = _M.begin(); teiter != _M.end(); teiter++){
      if ((!singleBest && (*teiter)->root() && teamId == -1) || //all root teams
            (!singleBest && (*teiter)->id() == teamId) || //specific team 
            (singleBest && (*teiter)->id() == bestTeam->id())){ //singleBest root team
         oss << "tminfo t " << t << " id " << (*teiter)->id() << " gtm " << (*teiter)->gtime() << " phs " << phase;
         oss << " root " << ((*teiter)->root() ? 1 : 0);
         oss << " sz " << (*teiter)->size();
         oss << " asz " << (*teiter)->asize();
         oss << " age " << t - (*teiter)->gtime();
         //oss << " compl";
         //oss << " " << (*teiter)->numActiveTeams();
         //oss << " " << (*teiter)->numActivePrograms();
         //oss << " " << (*teiter)->numEffectiveInstructions();
         //oss << " " << (*teiter)->numActiveFeatures();
         oss << " nOut " << (*teiter)->numOutcomes(_TRAIN_PHASE, -1, -1) << " " << (*teiter)->numOutcomes(_TEST_PHASE, -1, -1);

         oss << setprecision(5) << fixed;

         for (int phs = 0; phs <= 2; phs++){
            if (phs == 0) oss << " mnOutTrn";
            else if (phs == 1) oss << " mnOutVal";
            else oss << " mnOutTst";

            //statistics stored in aux doubles
            for (size_t task = 0; task < _numTask; task++){
               oss << " tsk_" << task+1 << "_a";
               for (size_t i = 0; i < _numPointAuxDouble; i++){
                  if ((*teiter)->numOutcomes(phs, task, -1) > 0)
                     oss << " " << (*teiter)->getMeanOutcome(phs,task,-1,i,false,d);//specific task, all fitMode, specific auxDouble, don't skip zero values
                  else 
                     oss << " x";
               }
            }
         }

         oss << " fit " << (*teiter)->fit();

         oss << setprecision(3) << fixed;

         visitedTeams.clear();
         vector < int > programInstructionCounts, effectiveProgramInstructionCounts; 
         (*teiter)->policyInstructions(_teamMap, visitedTeams, programInstructionCounts, effectiveProgramInstructionCounts);

         oss << " pIns " << accumulate(programInstructionCounts.begin(), programInstructionCounts.end(), 0);
         oss << " mnProgIns " << vecMean(programInstructionCounts);

         oss << " ePIns " << accumulate(effectiveProgramInstructionCounts.begin(), effectiveProgramInstructionCounts.end(), 0);
         oss << " mnEProgIns " << vecMean(effectiveProgramInstructionCounts); 
         set <program*, programIdComp> programs;
         set <memory*, memoryIdComp> memories;
         set <team*, teamIdComp> visitedTeams2;
         (*teiter)->getAllNodes(_teamMap, visitedTeams2, programs, memories, false);
         oss << " nP " << programs.size();
         oss << " nT " << visitedTeams2.size();
         oss << " nM " << memories.size();

         visitedTeams.clear();
         set<long> pF;
         (*teiter)->policyFeatures(_teamMap, visitedTeams, pF, true);
         oss << " pF " << (double)pF.size()/_dim;

         vector < int > opCountsSingle;
         vector < int > opCountsTally;
         opCountsTally.reserve(MAX_OP);
         opCountsTally.resize(MAX_OP);

         fill(opCountsTally.begin(), opCountsTally.end(), 0);
         for (auto it = programs.begin(); it != programs.end(); it++){
            (*it)->opCounts(opCountsSingle);
            for (size_t i = 0; i < opCountsSingle.size(); i++)
               opCountsTally[i] += opCountsSingle[i];
         }
         oss << " nOp " << vecToStr(opCountsTally);

         vector <int> tmSizesRoot, tmSizesSub;
         tmSizesRoot.push_back((*teiter)->size());
         for (auto teiter2 = visitedTeams2.begin(); teiter2 != visitedTeams2.end(); teiter2++)
            if ((*teiter2)->id() != (*teiter)->id()) //not the root of this policy
               tmSizesSub.push_back((*teiter2)->size());
         oss << " mnTmSzR " << vecMean(tmSizesRoot) << " mnTmSzS " << (tmSizesSub.size() > 0 ? vecMean(tmSizesSub) : 0);

         //map<point*, double, pointLexicalLessThan> outs;
         //(*teiter)->outcomes(outs, phase);
         //oss << " envSeeds";
         //for (auto iter = outs.begin(); iter != outs.end(); iter++)
         //   oss << " " << (*iter).first->envSeed();

         //oss << " fBins";
         //map <long, int> bins = (*teiter)->fitnessBins();
         //for (auto iter = bins.begin(); iter != bins.end(); iter++)
         //   oss << ":" << (*iter).first << "-" << (*iter).second;
         oss << endl;
      }
   }

}

/***********************************************************************************************************
 * Read in populations from a checkpoint file. (This whole process needs a rewrite sometime.)
 **********************************************************************************************************/
void TPG::readCheckpoint(long t, int phase, int chkpID, bool fromString, const string &inString){

   finalize(); //clear populations

   string str;

   if (fromString)
      str = inString;
   else{
      char filename[80];
      sprintf(filename, "%s/%s.%ld.%d.%d.%d.rslt","checkpoints","cp",t,chkpID,_seed,phase);
      ifstream t(filename);
      t.seekg(0, ios::end);
      str.reserve(t.tellg());
      t.seekg(0, ios::beg);
      str.assign((istreambuf_iterator<char>(t)),
            istreambuf_iterator<char>());
   }

   istringstream iss(str);

   string oneline;
   char delim=':';
   char *token;
   long memberId = 0;
   long max_teamCount = -1;
   long max_programCount = -1;
   int f;

   vector < string > outcomeFields;

   while (getline(iss, oneline)){
      outcomeFields.clear();

      split(oneline,delim,outcomeFields);

      if (outcomeFields[0].compare("seed") == 0)
         _seed = atol(outcomeFields[1].c_str());
      else if (outcomeFields[0].compare("t") == 0)
         _tCurrent = atol(outcomeFields[1].c_str());
      else if (outcomeFields[0].compare("activeTask") == 0)
         _activeTask = atoi(outcomeFields[1].c_str());
      else if (outcomeFields[0].compare("fitMode") == 0)
         _fitMode = atoi(outcomeFields[1].c_str());
      else if (outcomeFields[0].compare("phase") == 0)
         _phase = atoi(outcomeFields[1].c_str());

      else if (outcomeFields[0].compare("memory") == 0){
         long id = atol(outcomeFields[1].c_str());
         int nrefs = atoi(outcomeFields[2].c_str());
         memory *mem = new memory(id, nrefs);
         _Memory.insert(mem);
      }

      else if (outcomeFields[0].compare("linearM") == 0){
         program *l;
         f = 1;
         long id = atol(outcomeFields[f++].c_str()); 
         if (id > max_programCount) max_programCount = id;
         long gtime = atol(outcomeFields[f++].c_str());
         long action = atol(outcomeFields[f++].c_str());
         int stateful = atoi(outcomeFields[f++].c_str());
         long dim = atol(outcomeFields[f++].c_str());
         int nrefs = atol(outcomeFields[f++].c_str());
         long memId = atol(outcomeFields[f++].c_str());

         vector < linearM::instruction * > bid;
         for (size_t ii = f; ii < outcomeFields.size(); ii++){
            token = (char*)outcomeFields[ii].c_str();
            linearM::instruction *in = new linearM::instruction();
            in->reset();
            for (int j = in->size()-1, k = 0; j >= 0; j--, k++){
               if (token[j] == '1'){
                  in->flip(k);
               }
            }
            bid.push_back(in);
         }
         l = new linearM(gtime, action, stateful, dim, id, nrefs, bid);

         for(auto meiter = _Memory.begin(); meiter != _Memory.end(); meiter++)
            if ((*meiter)->id() == memId){
               l->memSet(*meiter);
               l->memGet()->refInc();
               break;
            }

         _L.insert(l);
      }
      else if (outcomeFields[0].compare("team") == 0){
         //set < program *> members;
         team *m;
         f = 1;
         long id = atol(outcomeFields[f++].c_str());
         if (id > max_teamCount) max_teamCount = id;
         long gtime = atol(outcomeFields[f++].c_str());
         m = new team(gtime, id);
         m->inDeg(atoi(outcomeFields[f++].c_str()));
         m->numEval(atoi(outcomeFields[f++].c_str()));
         //add programs in order
         for (size_t ii = f; ii < outcomeFields.size(); ii++){
            memberId = atol(outcomeFields[ii].c_str());
            for(auto leiter = _L.begin(), leiterEnd = _L.end(); leiter != leiterEnd; leiter++){ //inefficient!
               if ((*leiter)->id() == memberId){
                  if (m->addProgram(*leiter) == false)
                     m->addProgramActive(*leiter);
                  break;
               }
            }
         }
         _M.insert(m);
         _teamMap[m->id()] = m;
         if (m->inDeg() == 0)
            _Mroot.insert(m);
         else
            m->root(false);
      }
      else if (outcomeFields[0].compare("fBin") == 0){
         long id = atol(outcomeFields[1].c_str());
         team *tm = _teamMap[id];
         for (size_t ii = 2; ii < outcomeFields.size(); ii++){
            vector <string> fb;
            split(outcomeFields[ii].c_str(), '-', fb);
            tm->fitnessBin(atol(fb[0].c_str()), atoi(fb[1].c_str()));
         }
      }
      else if (!fromString && outcomeFields[0].compare("phyloNode") == 0 && find(outcomeFields.begin(), outcomeFields.end(), "gtime") == outcomeFields.end()){
         f = 1;
         long id = atol(outcomeFields[f++].c_str());
         _phyloGraph.insert(pair<long, phyloRecord>(id, phyloRecord()));
         _phyloGraph[id].gtime = atol(outcomeFields[f++].c_str());
         _phyloGraph[id].dtime = atol(outcomeFields[f++].c_str());
         _phyloGraph[id].fitnessBin = atoi(outcomeFields[f++].c_str());
         _phyloGraph[id].fitness = atof(outcomeFields[f++].c_str());
         _phyloGraph[id].root = atoi(outcomeFields[f++].c_str()) > 0 ? true : false;
      }
      else if (!fromString && outcomeFields[0].compare("phyloLink") == 0 && find(outcomeFields.begin(), outcomeFields.end(), "from") == outcomeFields.end())
         _phyloGraph[atol(outcomeFields[1].c_str())].adj.push_back(atol(outcomeFields[2].c_str()));
      else if (!fromString && outcomeFields[0].compare("genTimes") == 0)
         for (size_t gt = 1; gt < outcomeFields.size(); gt++)
            _genTimes.push_back(atoi(outcomeFields[gt].c_str()));
      else if (outcomeFields[0].compare("miniBatchIndexes") == 0)
         for (size_t mb = 1; mb < outcomeFields.size(); mb++)
            _miniBatchIndexes.push_back(atoi(outcomeFields[mb].c_str()));
   }

   _programCount = max_programCount+1;
   _teamCount = max_teamCount+1;

   //put this in a "sanity check" function
   int sumTeamSizes=0;
   int nrefs = 0;
   int sumNumOutcomes = 0;

   if (!fromString) oss << "TPG::readCheckpoint " << " Msize " << _M.size() << " Lsize " << _L.size() << " MrooSize " << _Mroot.size();

   for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
   {
      sumTeamSizes += (*teiter)->size();
      sumNumOutcomes += (*teiter)->numOutcomes(_TRAIN_PHASE, -1, -1);
   }

   if (fromString) recalculateProgramRefs();

   for(auto leiter = _L.begin(); leiter != _L.end(); leiter++)
      nrefs += (*leiter)->refs();

   if (!fromString) oss << " sumTeamSizes " << sumTeamSizes << " nrefs " << nrefs << " sumNumOutcomes " << sumNumOutcomes << endl;

   //if(sumTeamSizes != nrefs)
   //   die(__FILE__, __FUNCTION__, __LINE__, "something messed up during readCheckpoint");
}

/*********************************************************************************************************/
void TPG::recalculateProgramRefs(){

   for(auto leiter = _L.begin(); leiter != _L.end(); leiter++)
      (*leiter)->setNrefs(0);

   set < program *, programIdComp> mem;
   for(auto teiter = _M.begin(); teiter != _M.end(); teiter++){
      (*teiter)->members(&mem);
      for(auto leiter = mem.begin(); leiter != mem.end(); leiter++)
         (*leiter)->refInc(); 
      mem.clear();
   }
}

/********************************************************************************************/
void TPG::selTeams(long t, bool verbose, int genTime, bool checkTestElite)
{
   set < team *, teamFitnessLexicalCompare > teams;
   int numOldDeleted = 0;
   int numDeleted = 0;

   setSingleTaskFitness(_phase);
   setEliteTeams(t, _phase, verbose);
   //setSingleTaskFitness(_phase);

   long sumRunTimeComplexityIns = 0;
   long sumRunTimeComplexityTms = 0;
   long sumEffectiveInstructions = 0;
   for (auto teiter = _Mroot.begin(); teiter != _Mroot.end(); teiter++){
      sumRunTimeComplexityIns += (*teiter)->runTimeComplexityIns();
      sumRunTimeComplexityTms += (*teiter)->runTimeComplexityTms();

      bool testElite = false;
      if (checkTestElite)
         testElite = isElite(*teiter, _TEST_PHASE);

      if(!isElite(*teiter, _TRAIN_PHASE) && !testElite){
         auto ret =  teams.insert(*teiter);
         if (!ret.second)
            die(__FILE__, __FUNCTION__, __LINE__, "wt.f check team comparator?");
      }
   }

   int numToDelete = floor(teams.size() * _Rgap);
   for (auto teiter = teams.rbegin(); teiter != teams.rend() && numDeleted < numToDelete; teiter++){   
      if((*teiter)->gtime() < t)
         numOldDeleted++;
      _phyloGraph[(*teiter)->id()].dtime = t;
      _Mroot.erase(*teiter);
      _teamMap.erase((*teiter)->id());
      _M.erase(*teiter);
      (*teiter)->cleanup();
      delete *teiter;
      numDeleted++;
   }

   //delete programs with zero refs
   for(auto leiter = _L.begin(); leiter != _L.end();){
      if ((*leiter)->refs() < 0 )
         die(__FILE__, __FUNCTION__, __LINE__, "wtf. less than zero refs?");
      if ((*leiter)->refs() == 0) {
         if ((*leiter)->action() >= 0){
            if (_teamMap[(*leiter)->action()]->inDeg() == 1){
               _teamMap[(*leiter)->action()]->root(true);
               _Mroot.insert(_teamMap[(*leiter)->action()]);
               _phyloGraph[_teamMap[(*leiter)->action()]->id()].root = true;
            }
            _teamMap[(*leiter)->action()]->inDeg(_teamMap[(*leiter)->action()]->inDeg() - 1);
            //if team was a subsumed root clone that has now become a root itself, just delete it
            //programs will be cleaned up on the next round
            if (_teamMap[(*leiter)->action()]->root() && _teamMap[(*leiter)->action()]->numOutcomes(_TRAIN_PHASE, -1, -1) == 0){
               team * tm = _teamMap[(*leiter)->action()];
               _phyloGraph[tm->id()].dtime = t;
               _Mroot.erase(tm);
               _teamMap.erase(tm->id());
               _M.erase(tm);
               tm->cleanup();
               delete tm;
            }
         }
         (*leiter)->memGet()->refDec();
         if ((*leiter)->memGet()->refs() == 0){
            _Memory.erase((*leiter)->memGet());
            delete (*leiter)->memGet();
         }
         delete *leiter;
         _L.erase(leiter++);
      }
      else {
         sumEffectiveInstructions += (*leiter)->esize();
         leiter++;
      }
   }

   if (_reproduce)
      genTime = _genTimes[t-1];
   else
      _genTimes.push_back(genTime);

   if (_maxGenMillis > 0 && genTime <= _maxGenMillis){
      oss << "t " << t << " _Rsz " << _Rsize << " += " <<  floor(max(1.0, 1/(genTime/_maxGenMillis))) << " gTm " << genTime << " q " << 1/(_maxGenMillis/genTime) << setprecision(5) << " _pA " << _pAtomic <<  endl;
      //_Rsize += floor(max(1.0,1/(_maxGenMillis/genTime)));
      if (_pAtomic > 0.5) _pAtomic -= 0.0001;
   }
   else if (_maxGenMillis > 0 && genTime > _maxGenMillis){
      oss << "t " << t << " _Rsz " << _Rsize << " -= " <<  floor(max(1.0, 1/(_maxGenMillis/genTime))) << " gTm " << genTime << " q " << 1/(_maxGenMillis/genTime) << setprecision(5) << " _pA " << _pAtomic << endl;
      //_Rsize -= floor(max(1.0, 1/(_maxGenMillis/genTime)));
      if (_pAtomic < 0.99) _pAtomic += 0.0001;
   }
   else
      oss << "t " << t << " _Rsz " << _Rsize << endl;

   memory *m;
   while (_Memory.size() < _M.size()){ 
      m = new memory(_memoryCount++);
      _Memory.insert(m);
   }

   oss << "selTms t " << t << " Msz " << _M.size() << " Lsz " << _L.size() << " rSz " << _Rsize << " mrSz " << _Mroot.size() << " mSz " << _Memory.size();
   oss << " eLSz " << _eliteTeams.size() << " nToDel " << numToDelete << " nDel " << numDeleted << " nOldDel " << numOldDeleted << " nOldDelPr " << (double)numOldDeleted/numDeleted;
   oss << " sGsz " << sumRunTimeComplexityTms << " sRTC " << sumRunTimeComplexityIns << " sEsz " << sumEffectiveInstructions;
   oss << " RTC/sEsz " << (double)sumRunTimeComplexityIns/sumEffectiveInstructions << endl;
}

/********************************************************************************************/
void TPG::setParams(bool verbose){
   ostringstream osss;
   osss << "tpg." << _seed << ".arg";

   map < string, string > args;

   readMap(osss.str(), args);

   /* Get arguments. */

   map < string, string > :: iterator maiter;

   if((maiter = args.find("continuousOutput")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg continuousOutput");
   _continuousOutput = stringToInt(maiter->second) > 0 ? true : false;

   if((maiter = args.find("paretoEpsilonTeam")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg paretoEpsilonTeam");
   _paretoEpsilonTeam = stringToDouble(maiter->second);

   if((maiter = args.find("Rsize")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg Rsize");
   _Rsize = stringToInt(maiter->second);

   if((maiter = args.find("Rgap")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg Rgap");
   _Rgap = stringToDouble(maiter->second);

   if((maiter = args.find("numProfilePoints")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg numProfilePoints");
   _numProfilePoints = stringToInt(maiter->second);

   if((maiter = args.find("pAddProfilePoint")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pAddProfilePoint");
   _pAddProfilePoint = stringToDouble(maiter->second);

   if((maiter = args.find("pAtomic")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pAtomic");
   _pAtomic = stringToDouble(maiter->second);

   if((maiter = args.find("pmd")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pmd");
   _pmd = stringToDouble(maiter->second);

   if((maiter = args.find("pma")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pma");
   _pma = stringToDouble(maiter->second);

   if((maiter = args.find("pmm")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pmm");
   _pmm = stringToDouble(maiter->second);

   if((maiter = args.find("pmn")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pmn");
   _pmn = stringToDouble(maiter->second);

   if((maiter = args.find("pms")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pms");
   _pms = stringToDouble(maiter->second);

   if((maiter = args.find("pmw")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pmw");
   _pmw = stringToDouble(maiter->second);

   if((maiter = args.find("pmx")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pmx");
   _pmx = stringToDouble(maiter->second);

   if((maiter = args.find("initialTeamSize")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg initialTeamSize");
   _initialTeamSize = stringToInt(maiter->second);

   if((maiter = args.find("maxInitialTeamSize")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg maxInitialTeamSize");
   _maxInitialTeamSize = stringToInt(maiter->second);

   if((maiter = args.find("maxTeamSize")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg maxTeamSize");
   _maxTeamSize = stringToInt(maiter->second);

   if((maiter = args.find("maxTeamsPerGraph")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg maxTeamsPerGraph");
   _maxTeamsPerGraph = stringToInt(maiter->second);

   if((maiter = args.find("maxRunTimeComplexityIns")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg maxRunTimeComplexityIns");
   _maxRunTimeComplexityIns = stringToInt(maiter->second);

   if((maiter = args.find("maxRunTimeComplexityTms")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg maxRunTimeComplexityTms");
   _maxRunTimeComplexityTms = stringToInt(maiter->second);

   if((maiter = args.find("maxInitialProgSize")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg maxInitialProgSize");
   _maxInitialProgSize = stringToInt(maiter->second);

   if((maiter = args.find("maxProgSize")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg maxProgSize");
   _maxProgSize = stringToInt(maiter->second);

   if((maiter = args.find("pBidMutate")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pBidMutate");
   _pBidMutate = stringToDouble(maiter->second);

   if((maiter = args.find("pBidSwap")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pBidSwap");
   _pBidSwap = stringToDouble(maiter->second);

   if((maiter = args.find("pBidDelete")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pBidDelete");
   _pBidDelete = stringToDouble(maiter->second);

   if((maiter = args.find("pBidAdd")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg pBidAdd");
   _pBidAdd = stringToDouble(maiter->second);

   if((maiter = args.find("numElite")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg numElite");
   _numElite = stringToInt(maiter->second);

   if((maiter = args.find("numEvalPerGeneration")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg numEvalPerGeneration");
   _numEvalPerGeneration = size_t(stringToInt(maiter->second));

   if((maiter = args.find("numStoredOutcomesPerHost_TRAIN")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg numStoredOutcomesPerHost");
   _numStoredOutcomesPerHost[_TRAIN_PHASE] = stringToLong(maiter->second);

   if((maiter = args.find("numStoredOutcomesPerHost_VALIDATION")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg numStoredOutcomesPerHost_VALIDATION");
   _numStoredOutcomesPerHost[_VALIDATION_PHASE] = stringToLong(maiter->second);

   if((maiter = args.find("numStoredOutcomesPerHost_TEST")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg numStoredOutcomesPerHost_TEST");
   _numStoredOutcomesPerHost[_TEST_PHASE] = stringToLong(maiter->second);

   if((maiter = args.find("diversityMode")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg diversityMode");
   _diversityMode = stringToInt(maiter->second);

   if((maiter = args.find("maxGenMillis")) == args.end()) die(__FILE__, __FUNCTION__, __LINE__, "cannot find arg maxGenMillis");
   _maxGenMillis = stringToInt(maiter->second);

   if (verbose){
      oss << "TPG_par  seed " << _seed << endl;

      oss << "TPG_par maxRunTimeComplexityIns " << _maxRunTimeComplexityIns << endl;
      oss << "TPG_par maxRunTimeComplexityTms " << _maxRunTimeComplexityTms << endl;
      oss << "TPG_par initialTeamSize " << _initialTeamSize << endl;
      oss << "TPG_par maxInitialTeamSize " << _maxInitialTeamSize << endl;
      oss << "TPG_par maxTeamSize " << _maxTeamSize << endl;
      oss << "TPG_par pAtomic " << _pAtomic << endl;
      oss << "TPG_par pmd " << _pmd << endl;
      oss << "TPG_par pma " << _pma << endl;
      oss << "TPG_par pmm " << _pmm << endl;
      oss << "TPG_par pmn " << _pmn << endl;
      oss << "TPG_par pms " << _pms << endl;
      oss << "TPG_par pmw " << _pms << endl;
      oss << "TPG_par pmx " << _pmx << endl;

      oss << "TPG_par maxInitialProgSize " << _maxInitialProgSize << endl;
      oss << "TPG_par maxProgSize " << _maxProgSize << endl;
      oss << "TPG_par pBidMutate " << _pBidMutate << endl;
      oss << "TPG_par pBidSwap " << _pBidSwap << endl;
      oss << "TPG_par pBidDelete " << _pBidDelete << endl;
      oss << "TPG_par pBidAdd " << _pBidAdd << endl;
      oss << "TPG_par MEMORY_REGISTERS " << MEMORY_REGISTERS << endl;

      oss << "TPG_par Rsize " << _Rsize << endl;
      oss << "TPG_par Rgap " << _Rgap << endl;
      oss << "TPG_par numElite " << _numElite << endl;
      oss << "TPG_par numStoredOutcomesPerHost_TRAIN_PHASE " << _numStoredOutcomesPerHost[_TRAIN_PHASE] << endl;
      oss << "TPG_par numStoredOutcomesPerHost_VALIDATION_PHASE " << _numStoredOutcomesPerHost[_VALIDATION_PHASE] << endl;
      oss << "TPG_par numStoredOutcomesPerHost_TEST_PHASE " << _numStoredOutcomesPerHost[_TEST_PHASE] << endl;
      oss << "TPG_par numProfilePoints " << _numProfilePoints << endl;
      oss << "TPG_par pAddProfilePoint " << _pAddProfilePoint << endl;
      oss << "TPG_par paretoEpsilonTeam " << _paretoEpsilonTeam << endl;

      oss << "TPG_par diversityMode " << _diversityMode << endl;

      oss << "TPG_par dim " << _dim << endl;

      oss << "TPG_par chechkpoint " << _checkpoint << endl;
      oss << "TPG_par checkPointInMode " << _checkpointInPhase << endl;
      oss << "TPG_par taskSwitchMod " << _taskSwitchMod << endl;
      oss << "TPG_par activeTask " << _activeTask << endl;
      oss << "TPG_par hostToReplay " << _hostToReplay << endl;
      oss << "TPG_par id " << _id << endl;
      oss << "TPG_par replay " << _replay << endl;
      oss << "TPG_par seed " << _seed << endl;
      oss << "TPG_par stateful " << _stateful << endl;
      oss << "TPG_par tMain " << _tMain << endl;
      oss << "TPG_par tPickup " << _tPickup << endl;
      oss << "TPG_par verbose " << _verbose << endl;
   }

   if(_Rsize < 2) die(__FILE__, __FUNCTION__, __LINE__, "bad arg Rsize < 2");

   if(_pmd < 0 || _pmd > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pmd < 0 || pmd > 1");

   if(_pma < 0 || _pma > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pma < 0 || pma > 1");

   if(_pmm < 0 || _pmm > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pmm < 0 || pmm > 1");

   if(_pmn < 0 || _pmn > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pmn < 0 || pmn > 1");

   if(_pms < 0 || _pms > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pms < 0 || pms > 1");

   if(_pmw < 0 || _pmw > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pmw < 0 || pmw > 1");

   if(_maxInitialTeamSize < 2) die(__FILE__, __FUNCTION__, __LINE__, "bad arg _maxInitialTeamSize < 2");

   if(_maxTeamSize < 2) die(__FILE__, __FUNCTION__, __LINE__, "bad arg _maxTeamSize < 2");

   if(_Rgap < 0 || _Rgap > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg Rgap < 0 || Rgap > 1");

   if(_maxInitialProgSize < 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg _maxInitialProgSize < 1");

   if(_maxProgSize < 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg _maxProgSize < 1");

   if(_pBidDelete < 0 || _pBidDelete > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pBidDelete < 0 || pBidDelete > 1");

   if(_pBidAdd < 0 || _pBidAdd > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pBidAdd < 0 || pBidAdd > 1");

   if(_pBidSwap < 0 || _pBidSwap > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pBidSwap < 0 || pBidSwap > 1");

   if(_pBidMutate < 0 || _pBidMutate > 1) die(__FILE__, __FUNCTION__, __LINE__, "bad arg pBidMutate < 0 || pBidMutate > 1");

   if(_paretoEpsilonTeam < 0) die(__FILE__, __FUNCTION__, __LINE__, "bad arg _paretoEpsilonTeam < 0");
}

/********************************************************************************************/
void TPG::setSingleTaskFitness(int phase){
   double d;
   for(auto teiter = _Mroot.begin(); teiter != _Mroot.end(); teiter++){
      markEffectiveCode(*teiter, true);
      (*teiter)->domBy(0); //for multi-objective ranking
      (*teiter)->domOf(0); //for multi-objective ranking
      (*teiter)->updateComplexityRecord(_teamMap, _numPointAuxDouble - 1);

      if ((*teiter)->runTimeComplexityIns() > _maxRunTimeComplexityIns || (*teiter)->runTimeComplexityTms() > _maxRunTimeComplexityTms)
         (*teiter)->fit(numeric_limits<double>::lowest());
      else
         (*teiter)->fit((*teiter)->getMeanOutcome(phase, _activeTask, _fitMode, _fitMode,false,d));

      _phyloGraph[(*teiter)->id()].fitness = (*teiter)->fit();
      _phyloGraph[(*teiter)->id()].numActiveFeatures = (*teiter)->numActiveFeatures();
      _phyloGraph[(*teiter)->id()].numActivePrograms = (*teiter)->numActivePrograms();
      _phyloGraph[(*teiter)->id()].numActiveTeams = (*teiter)->numActiveTeams();
      _phyloGraph[(*teiter)->id()].numEffectiveInstructions = (*teiter)->numEffectiveInstructions();
   }
}

/********************************************************************************************/
void TPG::teamTaskRank(int phase, const vector <int> &objectives){
   double d;
   oss << "TPG::teamTaskRank <team:avgRank>";
   for(auto teiterA = _M.begin(); teiterA != _M.end(); teiterA++){
      if (!(*teiterA)->root()) continue;
      vector <int> ranks; for (size_t i = 0; i < objectives.size(); i++) ranks.push_back(1);
      for (size_t o = 0; o < objectives.size(); o++){
         for(auto teiterB = _M.begin(); teiterB != _M.end(); teiterB++){
            if (!(*teiterB)->root()) continue;
            if ((*teiterB)->getMeanOutcome(phase, objectives[o], _fitMode, _fitMode ,false,d) > (*teiterA)->getMeanOutcome(phase, objectives[o], _fitMode, _fitMode,false,d))
               ranks[o]++;
         }
      }

      double rankSum = 0;
      for (size_t i = 0; i < ranks.size(); i++) rankSum += ranks[i];
      (*teiterA)->fit(1 / (rankSum/ranks.size()));
      oss << " " << (*teiterA)->id() << ":" << (*teiterA)->fit();
   }
   oss << endl;
}

/**********************************************************************************************************/
void TPG::updateMODESFilters(bool roots){

   if (_tCurrent == _tStart){
      _persistenceFilterA.clear();
      set <long> ancestorIds;
      for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
         if (!roots || (roots && (*teiter)->root())){
            (*teiter)->getAncestorIds(ancestorIds);
            for (auto pr = _allComponentsA.begin(); pr != _allComponentsA.end(); pr++)
               if (find(ancestorIds.begin(), ancestorIds.end(), (*pr).first) != ancestorIds.end()){
                  //found an ancestor of *teiter in _allComponentsA, add *teiter to _persistenceFilterA
                  _persistenceFilterA.insert(pair<long, modesRecord>((*teiter)->id(), modesRecord()));
                  //store active programs for novelty metric
                  set < team *, teamIdComp > teams;
                  set < program *, programIdComp > programs;
                  set < memory *, memoryIdComp > memories;
                  (*teiter)->getAllNodes(_teamMap, teams, programs, memories, true);
                  for (auto leiter = programs.begin(); leiter != programs.end(); leiter++){
                     _persistenceFilterA[(*teiter)->id()].activeProgramIds.insert((*leiter)->id());
                     _persistenceFilterA[(*teiter)->id()].effectiveInstructionsTotal += (*leiter)->esize();
                  }
                  for (auto teiter2 = teams.begin(); teiter2 != teams.end(); teiter2++)
                     _persistenceFilterA[(*teiter)->id()].activeTeamIds.insert((*teiter2)->id());
                  _persistenceFilterS.insert(pair<long, modesRecord>((*teiter)->id(), _persistenceFilterA[(*teiter)->id()]));
                  break;
               }
         }

      //do analysis here
      int change = 0;
      int novelty = 0;
      int complexityA = 0;
      int complexityB = 0;
      double ecology = 0;
      double MODES_NOVELTY_THRESHOLD = 0.25;
      for (auto prA = _persistenceFilterA.begin(); prA != _persistenceFilterA.end(); prA++){
         //change
         if (_persistenceFilterA_t.find((*prA).first) == _persistenceFilterA_t.end())
            change++;
         //novelty
         double minNoveltyScore = 1.0;
         for (auto prS = _persistenceFilterS.begin(); prS != _persistenceFilterS.end(); prS++){
            if (prS != prA){ //if this gets expensive could fix...
               vector<long> symbiontIntersection;
               vector<long> symbiontUnion;
               int symIntersection;
               int symUnion;
               set_intersection ((*prA).second.activeProgramIds.begin(), (*prA).second.activeProgramIds.end(), 
                     (*prS).second.activeProgramIds.begin(), (*prS).second.activeProgramIds.end(), 
                     back_inserter(symbiontIntersection));
               symIntersection = symbiontIntersection.size();
               set_union ((*prA).second.activeProgramIds.begin(), (*prA).second.activeProgramIds.end(),
                     (*prS).second.activeProgramIds.begin(), (*prS).second.activeProgramIds.end(),
                     back_inserter(symbiontUnion));
               symUnion = symbiontUnion.size();
               double noveltyScore = 1.0 - ((double)symIntersection / symUnion);
               minNoveltyScore = min(minNoveltyScore, noveltyScore);
            }
         }
         if (minNoveltyScore >= MODES_NOVELTY_THRESHOLD)
            novelty++;

         //complexity
         complexityA = max(complexityA, (int)(*prA).second.effectiveInstructionsTotal);
         complexityB = max(complexityB, (int)(*prA).second.activeTeamIds.size()); //transitions ?

         //ecology
         double Pc = 0;
         for (auto prA2 = _persistenceFilterA.begin(); prA2 != _persistenceFilterA.end(); prA2++)
            if ((*prA2).second.activeTeamIds.find((*prA).first) != (*prA2).second.activeTeamIds.end())
               Pc++;
         Pc = Pc/_persistenceFilterA.size();
         ecology += Pc * log2(Pc);
      }
      ecology *= -1;

      oss << "TPG::MODES t " << _tCurrent << " pfASize " << _persistenceFilterA.size() << " pfA_tSize " << _persistenceFilterA_t.size();
      oss << " change " << change << " novelty " << novelty << " complexityA " << complexityA << " complexityB " << complexityB << " ecology " << ecology << endl;

      _persistenceFilterA_t.clear();
      _persistenceFilterA_t.insert(_persistenceFilterA.begin(), _persistenceFilterA.end());
   }

   //for next t
   _allComponentsA.clear();
   for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
      if (!roots || (roots && (*teiter)->root()))
         _allComponentsA.insert(pair<long, modesRecord>((*teiter)->id(), modesRecord()));
}

/**********************************************************************************************************/
void TPG::writeCheckpoint(long t, bool elite) {

   ofstream ofs;
   char filename[80]; sprintf(filename, "%s/%s.%ld.%d.%d.%d.rslt","checkpoints","cp",t,_id,_seed,_phase);

   if (fileExists(filename)){
      if( remove(filename) != 0 )
         cerr << "error deleting " << filename << endl;
   }
   ofs.open(filename, ios::out);
   if (!ofs.is_open() || ofs.fail()){
      cerr << "open failed for file: " << filename << " error:" << strerror(errno) << '\n';
      die(__FILE__, __FUNCTION__, __LINE__, "Can't open file.");
   }


   ofs << "seed:" << _seed << endl;
   ofs << "t:" << t << endl;

   if (elite){
      set < program *, programIdComp > programs;
      set < team *, teamIdComp > teams;
      set < memory *, memoryIdComp > memories;


      _eliteTeamMT[_phase]->getAllNodes(_teamMap, teams, programs, memories, false);
      for (size_t i = 0; i < _numTask; i++)
         _eliteTeamsEachTask[_phase][i]->getAllNodes(_teamMap, teams, programs, memories, false);

      for(auto meiter = memories.begin(); meiter != memories.end(); meiter++)
         ofs << (*meiter)->checkpoint();
      for(auto leiter = programs.begin(); leiter != programs.end(); leiter++)
         ofs << (*leiter)->checkpoint(true); //all instructions
      for(auto teiter = teams.begin(); teiter != teams.end(); teiter++)
         ofs << (*teiter)->checkpoint(false);
      for(auto teiter = teams.begin(); teiter != teams.end(); teiter++)
         ofs << (*teiter)->checkpoint(true);
   }
   else {
      for(auto meiter = _Memory.begin(); meiter != _Memory.end(); meiter++)
         ofs << (*meiter)->checkpoint();
      for(auto leiter = _L.begin(); leiter != _L.end(); leiter++)
         ofs << (*leiter)->checkpoint(true); //all instructions
      for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
         ofs << (*teiter)->checkpoint(false);
      for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
         ofs << (*teiter)->checkpoint(true);

      ofs <<"phyloNode:id:gtime:dtime:fitness:root" << endl;
      ofs <<"phyloLink:from,to" << endl;
      for (auto it = _phyloGraph.begin(); it != _phyloGraph.end(); it++){
         ofs << "phyloNode:" << (*it).first << ":" << (*it).second.gtime << ":" << (*it).second.dtime << ":" << (*it).second.fitnessBin << ":" << (*it).second.fitness << ":" << (*it).second.root << endl;
         if ((*it).second.adj.size() > 0)
            for (size_t i = 0; i < (*it).second.adj.size(); i++)
               ofs << "phyloLink:" << (*it).first << ":" << (*it).second.adj[i] << endl;
      }  

      ofs << "genTimes";
      for (size_t i = 0; i < _genTimes.size(); i++)
         ofs << ":" << _genTimes[i];
      ofs << endl;
   }

   ofs << "end" << endl;
   ofs.close();

   //if (compress){
   //   //compress this checkpoint
   //   oss.str("");
   //   oss << "tar czpf " << filename << ".tgz -C  checkpoints cp." << t << "." << _id << "." << _seed << "." << _phase << ".rslt";
   //   if (system (oss.str().c_str()) != 0)
   //      cerr << "error with system call" << endl;
   //   if( remove(filename) != 0 )
   //      cerr << "error deleting " << filename << endl;
   //   oss.str("");
   //}
   ////delete previous (compressed) checkpoint
   //if (previousT >= 0){
   //   sprintf(filename, "%s/%s.%ld.%d.%d.%d.rslt%s", "checkpoints", "cp", previousT, _id,_seed, _phase, compress ? ".tgz": "");
   //   if (ifstream(filename))
   //      if( remove(filename) != 0 )
   //         cerr << "error deleting " << filename << endl;
   //}

   oss << "TPG::writeCheckpoint " << " Msize " << _M.size() << " Lsize " << _L.size() << " MrooSize " << _Mroot.size() << endl;
}

/***********************************************************************************************************/
void TPG::writeCheckpoint(string &s, vector < team* > &rootTeams) {

   set < program *, programIdComp > programs;
   set < team *, teamIdComp > teams;
   set < memory *, memoryIdComp > memories;

   for (auto teiter = rootTeams.begin(); teiter != rootTeams.end(); teiter++)
      (*teiter)->getAllNodes(_teamMap, teams, programs, memories, false);

   stringstream ss;

   ss << "seed:" << _seed << endl;
   ss << "t:" << _tCurrent << endl;
   ss << "activeTask:" << _activeTask << endl;
   ss << "fitMode:" << _fitMode << endl;
   ss << "phase:" << _phase << endl;
   for(auto meiter = memories.begin(); meiter != memories.end(); meiter++)
      ss << (*meiter)->checkpoint();
   for(auto leiter = programs.begin(); leiter != programs.end(); leiter++)
      ss << (*leiter)->checkpoint(true);// all instructions
   for(auto teiter = teams.begin(); teiter != teams.end(); teiter++)
      ss << (*teiter)->checkpoint(false);
   ss << "miniBatchIndexes";
   for (size_t i = 0; i < _miniBatchIndexes.size(); i++)
      ss << ":" << _miniBatchIndexes[i];
   ss << endl; 

   s = ss.str();
}
