#ifndef misc_h
#define misc_h
#include <cstring>
#include <sstream>
#include <fstream>
#include <iostream>
#include <cmath>
#include <set>
#include <vector>
#include <map>
#include <algorithm>
#include <numeric>
#include <bzlib.h>
#include <deque>
#include <chrono>
#include <sstream>
//#include <boost/iostreams/filtering_streambuf.hpp>
//#include <boost/iostreams/copy.hpp>
//#include <boost/iostreams/filter/gzip.hpp>

using std::numeric_limits;
using namespace std;
typedef double behaviourType;

#define NEARZERO 10e-12
#define EPSILON_SBB 1e-5
#define MAX_NCD 1.2
#define _MEAN_OUT_PROP 1.0
#define MIN_NIBBLE_VAL 0
#define MAX_NIBBLE_VAL 15
#define _TRAIN_PHASE 0
#define _VALIDATION_PHASE 1
#define _TEST_PHASE 2
#define _PLAY_PHASE 3
#define _NUM_PHASE 4

int compressedLength(char *);

inline double discretize(double f,double min, double max, int steps){
   double d = round(((f - min)/(max - min))*(steps-1));
   return d>steps?steps-1:d;
}
void die(const char*, const char *, const int, const char *);
double EuclideanDistSqrd(double *, double *, int);
double EuclideanDistSqrd(vector < double > &, vector < double > &);
double EuclideanDistSqrdNorm(vector < double > &, vector < double > &);
double EuclideanDist(vector < double > &, vector < double > &);

inline bool fileExists(const char *fileName)
{
   ifstream infile(fileName);
   return infile.good();
}

int hammingDist(vector < int > &, vector < int > &);

inline bool isEqual(double x, double y)
{ return fabs(x - y) < NEARZERO; }

inline bool isEqual(double x, double y, double e)
{ return fabs(x - y) < e; }

inline bool isEqual(unsigned char x, unsigned char y)
{ return fabs(x - y) < 0; }

bool isEqual(vector < int > &, vector < int > &);
bool isEqual(vector < double > &, vector < double > &, double);
template<class T>
bool isEqual(vector < T > &, vector < T > &, double);
inline bool isGreater(double x, double y, double e, bool orEqual = false) { return orEqual ? x > y || fabs(x - y) < e : x > y && fabs(x - y) > e; }
inline bool isLess(double x, double y, double e, bool orEqual = false) { return orEqual ? x < y || fabs(x - y) < e : x < y && fabs(x - y) > e; }

struct modesRecord{
   set < long > activeProgramIds;
   set < long > activeTeamIds;
   long effectiveInstructionsTotal;
   modesRecord(){ effectiveInstructionsTotal = 0; }
};
double normalizedCompressionDistance(vector<int>&v1,vector<int>&v2);

struct noveltyDescriptor {
   double novelty;
   vector < int > profile;
   vector < long > profileLong;
} ;

struct phyloRecord{
   vector < long > adj;
   long gtime;
   long dtime;
   int fitnessBin;
   double fitness;
   bool root;
   long numActiveFeatures;
   long numActivePrograms;
   long numActiveTeams;
   long numEffectiveInstructions;
   phyloRecord(){ gtime = -1; dtime = -1; fitnessBin = -1; fitness = 0; root = true;}
};

int readMap(string, map < string, string > &);
inline double sigmoid(double x) { return 1 / (1 + exp(-x)); }
double stdDev(vector<double>);
int stringToInt(string);
long stringToLong(string);
double stringToDouble(string);

template < class vtype > string setToStr(set < vtype > &v)
{ ostringstream oss; //oss.precision(numeric_limits<double>::digits10+1);
   for(int i = 0; i < v.size(); i++) {
      oss << v[i];
      if (i < v.size() - 1)
         oss << " ";
   }
   return oss.str();
}
inline double sas(double s1,double s2, double a){
   return sqrt(pow(s1,2) + pow(s2,2) - (2*s1*s2*cos(a*(3.14159265/180.0))));
}
vector<string> &split(const string &s, char delim, vector<string> &elems);
vector<string> split(const string &s, char delim);

template < class ptype > struct lessThan : public binary_function < ptype *, ptype *, bool >
{
   bool operator() (ptype *lhs, ptype *rhs) { return lhs->key() < rhs->key(); }
};
template < class ptype > struct greaterThan : public binary_function < ptype *, ptype *, bool >
{
   bool operator() (ptype *lhs, ptype *rhs) { return lhs->key() > rhs->key(); }
};
template < class vtype > string vecToStr(vector < vtype > &v)
{ ostringstream oss; //oss.precision(numeric_limits<double>::digits10+1);
   for(size_t i = 0; i < v.size(); i++) {
      oss << v[i];
      if (i < v.size() - 1)
         oss << " ";
   }
   return oss.str();
}
template < class vtype > string vecToStrNoSpace(vector < vtype > &v)
{ ostringstream oss; for(size_t i = 0; i < v.size(); i++) { oss << v[i]; } return oss.str(); }

double vecMedian(vector<double>);
int vecMedian(vector<int>);
double vecMean(vector<double>);
double vecMean(vector<int>);

//string compressString(std::string& data);
//string decompressString(std::string& data);

class CSVReader
{
   string fileName;
   string delimiter;
   int dim;
   public:
   CSVReader(string fname, int d, string delm = " "){
      fileName = fname;
      delimiter = delm;
      dim = d;
   }
   vector < vector <double> > getData()
   {
      ifstream file(fileName);
      vector < vector <double> > dataVec;
      string line = "";
      while (getline(file, line))
      {
         vector<double> doubleValues(dim);//features
         doubleValues[0] = stod(line.c_str());
         dataVec.push_back(doubleValues);
      }
      file.close();
      return dataVec;
   }
};

#endif
