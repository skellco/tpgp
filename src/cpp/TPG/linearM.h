#ifndef linearM_h
#define linearM_h
#include <random>
#include <bitset>
#include "memory.h"
#include "program.h"

using namespace std;

class linearM: public program
{
   public:
      typedef bitset < 16 + 3 + 4 + 4 > instruction; //16 ops

      static const int _registers;
      static const int _registersRW;
      static const int _registersRO;
      /* Mode bits */
      static const int _dest_bit;
      static const int _src1_bit;
      static const int _feature_bit;
      static const int  _src2_bit;
      /* Used to define unique bidding behaviour. */
      static const int  _bid_epsilon;
      vector < instruction * > _bid; // Bid program
      vector < instruction * > _bidEffective;
      vector < instruction * > *_program;

      /* Instruction definitions. */

      /* Masks. Apply before shifting. */
      static const instruction _modeMask;
      static const instruction _opMask;
      static const instruction _dstMask;
      static const instruction _srcMask;

      /* Modes. */
      static const instruction _mode0;
      static const instruction _mode1;

      /* Operations. */
      static const instruction _opSum;
      static const instruction _opDiff;
      static const instruction _opProd;
      static const instruction _opDiv;
      static const instruction _opCos;
      static const instruction _opLog;
      static const instruction _opExp;
      static const instruction _opCondA;

      static const instruction _opSqrt;
      static const instruction _opSin;
      static const instruction _opTanh;
      static const instruction _opCondB;
      static const instruction _opSqr;
      static const instruction _opPow;
      static const instruction _opAbs;
      static const instruction _opCube;

      /* Shift amounts. */
      static const short _modeShift;
      static const short _opShift;
      static const short _dstShift;
      static const short _srcShift;

      void markIntrons(bool);
      double run(state *, int, int);
      string checkpoint(bool);
      inline void getBid(vector <instruction *> &b) { b = _bid;}
      linearM(long, long, int, long, long, mt19937 &); /* Create arbitrary linearM. */
      linearM(long, linearM &, long); /* Create linearM from another linearM. */
      linearM(long, long, int, long,long, long, vector < instruction * >); /* Create linearM from checkpoint file. */
      ~linearM();
      bool muBid(double, double, double, double, size_t, mt19937 &, uniform_real_distribution<> &); /* Mutate bid, return true if any changes occured. */
      inline int numInstructionBits() { return _bid[0]->size(); }
      inline int size() { return _bid.size(); }
      inline int esize() { return _bidEffective.size(); }
};
#endif
