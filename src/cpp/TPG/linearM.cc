#include "linearM.h"

//16 ops
/* 000 0000 0000 0000 0000 0000 1111 */
const linearM::instruction linearM::_modeMask(0xF);   
/* 000 0000 0000 0000 0000 1111 0000 */
const linearM::instruction linearM::_opMask(0xF0);     
/* 000 0000 0000 0000 0111 0000 0000 */
const linearM::instruction linearM::_dstMask(0x700);   
/* 111 1111 1111 1111 1000 0000 0000 */
const linearM::instruction linearM::_srcMask(0x7FFF800); 

//markIntrons() depends on these specific instruction
const linearM::instruction linearM::_opSum(0x0);
const linearM::instruction linearM::_opDiff(0x1);
const linearM::instruction linearM::_opProd(0x2);
const linearM::instruction linearM::_opDiv(0x3);
const linearM::instruction linearM::_opCos(0x4);
const linearM::instruction linearM::_opLog(0x5);
const linearM::instruction linearM::_opExp(0x6);
const linearM::instruction linearM::_opCondA(0x7);

const linearM::instruction linearM::_opSqrt(0x8);
const linearM::instruction linearM::_opSin(0x9);
const linearM::instruction linearM::_opTanh(0xA);
const linearM::instruction linearM::_opCondB(0xB);
const linearM::instruction linearM::_opSqr(0xC);
const linearM::instruction linearM::_opPow(0xD);
const linearM::instruction linearM::_opAbs(0xE);
const linearM::instruction linearM::_opCube(0xF);

const short linearM::_modeShift = 0;
const short linearM::_opShift = linearM::_modeMask.count();
const short linearM::_dstShift = linearM::_modeMask.count() + linearM::_opMask.count();
const short linearM::_srcShift = linearM::_modeMask.count() + linearM::_opMask.count() + linearM::_dstMask.count();

const int linearM::_registersRW = MEMORY_REGISTERS;
const int linearM::_registersRO = 18; 
const int linearM::_registers = _registersRW + _registersRO;

/* Mode bits */
const int linearM::_dest_bit = 3;
const int linearM::_src1_bit = 2;
const int linearM::_feature_bit = 1;
const int  linearM::_src2_bit = 0;

/* Used to define unique bidding behaviour. */
const int  linearM::_bid_epsilon = 1e-5; /* Using this equality threshold. */

/********************************************************************************************/
string linearM::checkpoint(bool all){
   ostringstream oss;

   oss << "linearM:" << _id << ":" << _gtime << ":" << _action << ":" << _stateful << ":" << _dim << ":" <<  _nrefs << ":" << _memory->id();

   if (all)
      for(size_t i = 0; i < _bid.size(); i++)
         oss << ":" << *_bid[i];
   else
      for(size_t i = 0; i < _bidEffective.size(); i++)
         oss << ":" << *_bidEffective[i];
   oss << endl;

   return oss.str();
}

/********************************************************************************************/
linearM::linearM(long gtime, long action, int maxInitialProgSize, long dim, long id, mt19937 &rng){

   _action = action;
   _dim = dim;
   _gtime = gtime;
   _id = id;
   _key = 0;
   _nrefs = 0;
   _REG = new double[_registers]; 

   //constants
   double cMin;
   for (size_t i = 1; i <= _registersRO; i++){
      cMin = i <= _registersRO/2 ? -1.0 : -0.9;//skip zero
      _REG[(_registersRW - 1) + i] = cMin + (i * 0.1);
   }

   _skipIntrons = false;

   instruction *in;

   uniform_real_distribution<double> disR(0.0, 1.0);
   uniform_int_distribution<int> disP(1, maxInitialProgSize);
   int progSize = disP(rng);

   for(int i = 0; i < progSize; i++)
   {
      in = new instruction();

      for(size_t j = 0; j < in->size(); j++)
         if (disR(rng) < 0.5) in->flip(j);

      _bid.push_back(in);
   }
   _opCounts.reserve((_opMask >> _opShift).to_ulong() + 1);
   _opCounts.resize((_opMask >> _opShift).to_ulong() + 1);
}

/********************************************************************************************/
linearM::linearM(long gtime, linearM &plr, long id){

   _action = plr.action();
   _dim = plr._dim;
   _gtime = gtime;
   _id = id;
   _key = plr.key();
   _bidVal = -(numeric_limits<double>::max());
   _nrefs = 0;
   _REG = new double [_registers];

   //constants 
   double cMin;
   for (size_t i = 1; i <= _registersRO; i++){
      cMin = i <= _registersRO/2 ? -1.0 : -0.9; //skip zero
      _REG[(_registersRW - 1) + i] = cMin + (i * 0.1);
   }

   _skipIntrons = false;
   _stateful = plr.stateful();

   vector < instruction * > :: iterator initer, initerend;

   for(initer = plr._bid.begin(), initerend = plr._bid.end();
         initer != initerend; initer++)
      _bid.push_back(new instruction(**initer));

   _opCounts.reserve((_opMask >> _opShift).to_ulong() + 1);
   _opCounts.resize((_opMask >> _opShift).to_ulong() +1);
}

/********************************************************************************************/
/* Create linearM from checkpoint file */
linearM::linearM(long gtime, long action, int stateful, long dim, long id, long nrefs, vector <instruction *> bid){

   _action= action;
   _bid = bid;
   _dim= dim;
   _gtime = gtime;
   _id = id;
   _key = 0;
   _nrefs = nrefs;
   _REG = new double[_registers];

   //constants 
   double cMin;
   for (size_t i = 1; i <= _registersRO; i++){
      cMin = i <= _registersRO/2 ? -1.0 : -0.9; //skip zero
      _REG[(_registersRW - 1) + i] = cMin + (i * 0.1);
   }

   _stateful = stateful > 0 ? true : false;
   _skipIntrons = false;

   _opCounts.reserve((_opMask >> _opShift).to_ulong() + 1);
   _opCounts.resize((_opMask >> _opShift).to_ulong() + 1);
}

/********************************************************************************************/
linearM::~linearM()
{
   for(size_t i = 0; i < _bid.size(); i++)
      delete _bid[i];
   delete[] _REG;
}

/********************************************************************************************/
void linearM::markIntrons(bool continuousOutput)
{  
   vector < instruction * > :: reverse_iterator riter;

   instruction mode;
   instruction op;

   int reg;
   int dstIndex;
   int src1Index;
   long feat;

   fill(_opCounts.begin(), _opCounts.end(), 0);

   bitset < _registers > targetREG; //nothing above _registersRW -1 will be target

   _features.clear();
   _bidEffective.clear();

   targetREG.reset();
   targetREG.set(0, 1); //mark the bid output register
   if (continuousOutput) _memActive[0] = true; //return value for continuous output environments
   
   //From last to first instruction.
   for(riter = _bid.rbegin(); riter != _bid.rend(); riter++){ 
      mode = (**riter & _modeMask) >> _modeShift;

      //Get destination Index
      src1Index = dstIndex = (((**riter & _dstMask) >> _dstShift).to_ulong()) % _registersRW; 

      if (!mode[_dest_bit])
         dstIndex = dstIndex % MEMORY_REGISTERS;

      //Intruction is effective if its destination is a private stateless register index used later in this program OR
      //any index to a shared stateful register bank
      if ((mode[_dest_bit] && targetREG.test(dstIndex)) || !mode[_dest_bit]) {
         _bidEffective.insert(_bidEffective.begin(), *riter);

         op = (**riter & _opMask) >> _opShift;
         _opCounts[op.to_ulong()]++;

         //the destination will be overwritten, so remove it from targets
         if (mode[_dest_bit] && (op != _opCondA && op != _opCondB))
            targetREG.set(dstIndex, 0);
         //else if ((op != _opCondA && op != _opCondB))
         //   _memActive[dstIndex] = false;

         //If the operation is not unary, need to target src1
         if (op == _opSum || op == _opDiff || op == _opProd || op == _opDiv || op == _opPow || op == _opCondA || op == _opCondB) {
            if (mode[_src1_bit])
               targetREG.set(src1Index, 1);
            else{
               src1Index = src1Index % MEMORY_REGISTERS;
               _memActive[src1Index] = true;
            }
         }

         //SRC2 is input, get feature index y. 
         if (mode[_feature_bit]){ 
            feat = ((**riter & _srcMask) >> _srcShift).to_ulong() % _dim;
            _features.insert(feat);
         }
         //Neeed to target SRC2 
         else { 
            if (mode[_src2_bit]){
               reg = ((**riter & _srcMask) >> _srcShift).to_ulong() % _registers; //RW RO
               targetREG.set(reg, 1);
            }
            else{
               reg = ((**riter & _srcMask) >> _srcShift).to_ulong() % MEMORY_REGISTERS;
               _memActive[reg] = true;
            }
         }
      }
   }
}

/********************************************************************************************/
bool linearM::muBid(double pBidDelete,
      double pBidAdd,
      double pBidSwap,
      double pBidMutate,
      size_t maxProgSize,
      mt19937 &rng,
      uniform_real_distribution<> &disR)
{
   bool changed = false;

   /* Remove random instruction. */
   if(_bid.size() > 1 && disR(rng) < pBidDelete)
   {
      uniform_int_distribution<int> disBid(0,_bid.size()-1);
      int i = disBid(rng);

      delete *(_bid.begin() + i);
      _bid.erase(_bid.begin() + i);

      changed = true;
   }

   /* Insert random instruction. */
   if(_bid.size() < maxProgSize && disR(rng) < pBidAdd)
   {
      instruction *instr = new instruction();

      for(size_t j = 0; j < instr->size(); j++)
         if (disR(rng) < 0.5) instr->flip(j);

      uniform_int_distribution<int> disBid(0,_bid.size());
      int i = disBid(rng);

      _bid.insert(_bid.begin() + i, instr);

      changed = true;
   }

   /* Flip single bit of random instruction. */
   if (disR(rng) < pBidMutate)
   {
      uniform_int_distribution<int> disBid(0,_bid.size() - 1);
      int i = disBid(rng);
      uniform_int_distribution<int> disIns(0,_bid[0]->size() - 1);
      int j = disIns(rng);

      _bid[i]->flip(j);

      changed = true;
   }

   /* Swap positions of two instructions. */
   if (_bid.size() > 1 && disR(rng) < pBidSwap)
   {
      uniform_int_distribution<int> disBid(0,_bid.size()-1);
      int i = disBid(rng);

      int j;

      do
      {
         j = disBid(rng);
      } while(i == j);

      instruction *tmp;

      tmp = _bid[i];
      _bid[i] = _bid[j];
      _bid[j] = tmp;

      changed = true;
   }
   return changed;
}

/********************************************************************************************/
double linearM::run(state *s, int timeStep, int graphDepth)
{
   feature = s->getStatePointerDouble();

   memset(_REG, 0, _registersRW * sizeof(double));
   if (!_stateful) //if (_memory->refs() > 1)
      memset(_mem, 0, MEMORY_REGISTERS * sizeof(double));

   vector < instruction * > :: iterator initer, initerend;

   instruction mode;
   instruction op;

   int dstIndex;
   int src1Index;
   double src1Val;
   double src2Val;

   double *dest;

   int k;

   _program = _skipIntrons ? &_bidEffective : &_bid;
   for(k = 0, initer = _program->begin(), initerend = _program->end(); initer != initerend; initer++, k++){

      mode = (**initer & _modeMask) >> _modeShift;
      op = (**initer & _opMask) >> _opShift;

      /* Should be between 0 and _registers - 1. */
      src1Index = dstIndex = (((**initer & _dstMask) >> _dstShift).to_ulong()) % _registersRW;

      if (mode[_dest_bit])
         dest = _REG;
      else{
         dest = _mem;
         dstIndex = dstIndex % MEMORY_REGISTERS;
         _memWriteTime[dstIndex] = timeStep + (graphDepth/MAX_GRAPH_DEPTH);
      }

      if (mode[_src1_bit])
         src1Val = _REG[src1Index];
      else{
         src1Index = src1Index % MEMORY_REGISTERS;;
         src1Val = _mem[src1Index];
         /* If the operation is not unary*/
         if(op != _opCos && op != _opLog && op != _opExp && op != _opSqrt && op != _opSin && 
               op != _opTanh && op != _opSqr && op != _opAbs && op != _opCube && op != _opCondA && op != _opCondB){
            //if(op != _opCos && op != _opLog && op != _opExp) 
            _memReadTime[src1Index] = timeStep + (graphDepth/MAX_GRAPH_DEPTH);
         }
      }

      if (mode[_feature_bit])
         src2Val = double((feature[((**initer & _srcMask) >> _srcShift).to_ulong() % _dim]));
      else {
         if (mode[_src2_bit])
            src2Val = _REG[((**initer & _srcMask) >> _srcShift).to_ulong() % _registers]; //RW and RO
         else{
            src2Val = _mem[((**initer & _srcMask) >> _srcShift).to_ulong() % MEMORY_REGISTERS];
            _memReadTime[((**initer & _srcMask) >> _srcShift).to_ulong() % MEMORY_REGISTERS] = timeStep + (graphDepth/MAX_GRAPH_DEPTH);
         }
      }

      if (op == _opSum)
         dest[dstIndex] = src1Val + src2Val;
      else if (op == _opDiff)
         dest[dstIndex] = src1Val - src2Val;
      else if (op == _opProd)
         dest[dstIndex] = src1Val * src2Val;
      else if (op == _opDiv)
         dest[dstIndex] = src1Val / src2Val;
      else if (op == _opCos)
         dest[dstIndex] = cos(src2Val);
      else if (op == _opLog)
         dest[dstIndex] = log(fabs(src2Val));
      else if (op == _opExp)
         dest[dstIndex] = exp(src2Val);
      else if (op == _opCondA){
         if (src1Val < src2Val)
            dest[dstIndex] = - dest[dstIndex];
      }
      else if (op == _opSqrt)
         dest[dstIndex] = sqrt(src2Val);
      else if (op == _opSin)
         dest[dstIndex] = sin(src2Val);
      else if (op == _opTanh)
         dest[dstIndex] = tanh(src2Val);
      else if (op == _opCondB){
         if (src1Val >= src2Val)
            dest[dstIndex] = - dest[dstIndex];
      }
      else if (op == _opSqr)
         dest[dstIndex] = pow(src2Val, 2);
      else if (op == _opPow)
         dest[dstIndex] = pow(src1Val, src2Val);
      else if (op == _opAbs)
         dest[dstIndex] = abs(src2Val);
      else if (op == _opCube)
         dest[dstIndex] = pow(src2Val, 3);
      else
         die(__FILE__, __FUNCTION__, __LINE__, "bad operation");

      if (isfinite(dest[dstIndex]) == 0)
         dest[dstIndex] = 0;
   }
   //return numeric_limits<double>::lowest(); //_REG[0];
   return _REG[0];
}
