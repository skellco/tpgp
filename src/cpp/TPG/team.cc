#include <algorithm>
#include <limits>
#include "team.h"

/********************************************************************************************/
bool team::addProgram(program *lr, int i) {
   if (find(_members.begin(), _members.end(), lr) == _members.end()){//could have duplicates since ordermaters
      if (i < 0)
         _members.push_back(lr);
      else {
         auto it = _members.begin();
         advance(it, i);
         _members.insert(it, lr);
      }
      if (lr->action() < 0)
         _numAtomic++;
      _membersRun.resize(_members.size());
      _membersRunTally[lr->id()] = 0;
      return true;
   }
   return false;
}

/********************************************************************************************/
bool team::addProgramActive(program *lr) {
   if (find(_active.begin(), _active.end(), lr) == _active.end()){
      _active.insert(lr);
      return true;
   }
   return false;
}

/********************************************************************************************/
string team::checkpoint(bool fitnessBins) const {
   ostringstream oss;
   if (fitnessBins){
      oss << "fBin:" << _id;
      for (auto iter = _fitnessBins.begin(); iter != _fitnessBins.end(); iter++)
         oss << ":" << (*iter).first << "-" << (*iter).second;
   }
   else{
      oss << "team:" << _id << ":" << _gtime << ":" << _inDeg << ":" << _numEval;
      for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
         oss << ":" << (*leiter)->id();
      for(auto leiter = _active.begin(); leiter != _active.end(); leiter++)
         oss << ":" << (*leiter)->id();
   }

   oss << endl;
   return oss.str();
}

/********************************************************************************************/
void team::clone(team **tm) {
   //team* tm = new team(t, id);
   (*tm)->addAncestorId(_id);
   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++){
      (*tm)->addProgram(*leiter);
      (*leiter)->refInc();
   }
   for(auto leiter = _active.begin(); leiter != _active.end(); leiter++)
      (*tm)->addProgramActive(*leiter);
   (*tm)->fitnessBins(_fitnessBins);
   (*tm)->clone(true);
}

///********************************************************************************************/
////this version creates all new teams anprograms with the same structure
//void team::cloneNew(team **tm, vector <team*> &teamClones, vector<program*> &programClones) {
//   //team* tm = new team(t, id);
//   (*tm)->addAncestorId(_id);
//   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++){
//      (*tm)->addProgram(*leiter);
//      (*leiter)->refInc();
//   }
//   for(auto leiter = _active.begin(); leiter != _active.end(); leiter++)
//      (*tm)->addProgramActive(*leiter);
//   (*tm)->fitnessBins(_fitnessBins);
//   (*tm)->clone(true);
//}

/********************************************************************************************/
void team::deleteOutcome(point *pt) {
   map < point *, double, pointLexicalLessThan > :: iterator ouiter;

   if((ouiter = _outcomes.find(pt)) == _outcomes.end())
      die(__FILE__, __FUNCTION__, __LINE__, "should not delete outcome that is not set");
   delete ouiter->first;
   _outcomes.erase(ouiter);
}

/********************************************************************************************/
void team::features(set < long > &F) const {

   if(F.empty() == false)
      die(__FILE__, __FUNCTION__, __LINE__, "feature set not empty");

   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
      (*leiter)->features(F);
}

/********************************************************************************************/
double team::novelty(int type, int kNN) const {       
   multiset<double>::iterator it;
   double nov = 0;
   int i = 0;
   if (type==0){
      for (it=_distances_0.begin(); it!=_distances_0.end() && i<=kNN; ++it, i++)
         nov += *it;
      return nov/i;
   }
   else if (type==1){
      for (it=_distances_1.begin(); it!=_distances_1.end() && i<=kNN; ++it, i++)
         nov += *it;
      return nov/i;
   }
   else if (type==2){
      for (it=_distances_2.begin(); it!=_distances_2.end() && i<=kNN; ++it, i++)
         nov += *it;
      return nov/i;
   }
   else return -1;
}

/********************************************************************************************/
double team::symbiontUtilityDistance(team * t) const {
   vector<int> symbiontIntersection;
   vector<int> symbiontUnion;
   vector<int>::iterator it;
   int symIntersection;
   int symUnion;
   vector < long > team1Ids;
   vector < long > team2Ids;
   set < program *, programIdComp > activeMembers;
   t->activeMembers(&activeMembers);
   /* if either team has no active members then return 0 */
   if (_active.size() < 1 || activeMembers.size() < 1)
      return 0.0;
   for(auto leiter = _active.begin(); leiter != _active.end(); leiter++)
      team1Ids.push_back((*leiter)->id());
   for(auto leiter = activeMembers.begin(); leiter != activeMembers.end(); leiter++)
      team2Ids.push_back((*leiter)->id());
   sort(team1Ids.begin(), team1Ids.end());
   sort(team2Ids.begin(), team2Ids.end());
   set_intersection (team1Ids.begin(), team1Ids.end(), team2Ids.begin(), team2Ids.end(), back_inserter(symbiontIntersection));
   symIntersection = symbiontIntersection.size();
   set_union (team1Ids.begin(), team1Ids.end(), team2Ids.begin(), team2Ids.end(), back_inserter(symbiontUnion));
   symUnion = symbiontUnion.size();
#ifdef MYDEBUG
   cout << "genoDiffa t1Size " << _active.size() << " t1Ids " << vecToStr(team1Ids);
   cout << " allMembersSize " << _members.size();
   cout << " t2Size " << t->asize() << " t2Ids ";
   cout << vecToStr(team2Ids) << " symIntersection " << vecToStr(symbiontIntersection) << " symIntersectionSize " << symIntersection;
   cout << " symUnion " << vecToStr(symbiontUnion) << " symUnionSize " << symUnion << " diff " << 1.0 - ((double)symIntersection / (double)symUnion) << endl;
#endif
   return 1.0 - ((double)symIntersection / (double)symUnion);
}

/********************************************************************************************/
//this version compares with a vector of Ids *assumed sorted*
double team::symbiontUtilityDistance(vector < long > & compareWithThese) const {
   vector<int> symbiontIntersection;
   vector<int> symbiontUnion;
   vector<int>::iterator it;
   int symIntersection;
   int symUnion;
   vector < long > team1Ids;
   /* if either team has no active members then return 0 */
   if (_active.size() < 1 || compareWithThese.size() < 1)
      return 0.0;
   for(auto leiter = _active.begin(); leiter != _active.end(); leiter++)
      team1Ids.push_back((*leiter)->id());
   sort(team1Ids.begin(), team1Ids.end());
   set_intersection (team1Ids.begin(), team1Ids.end(), compareWithThese.begin(), compareWithThese.end(), back_inserter(symbiontIntersection));
   symIntersection = symbiontIntersection.size();
   set_union (team1Ids.begin(), team1Ids.end(), compareWithThese.begin(), compareWithThese.end(), back_inserter(symbiontUnion));
   symUnion = symbiontUnion.size();
#ifdef MYDEBUG
   cout << "genoDiffb t1Size " << _active.size() << " t1Ids " << vecToStr(team1Ids);
   cout << " allMembersSize " << _members.size();
   cout << " t2Size " << compareWithThese.size() << " t2Ids ";
   cout << vecToStr(compareWithThese) << " symIntersection " << vecToStr(symbiontIntersection) << " symIntersectionSize " << symIntersection;
   cout << " symUnion " << vecToStr(symbiontUnion) << " symUnionSize " << symUnion << " diff " << 1.0 - ((double)symIntersection / (double)symUnion) << endl;
#endif
   return 1.0 - ((double)symIntersection / (double)symUnion);
}

/********************************************************************************************/
void team::updateComplexityRecord(map <long, team*> &teamMap, size_t rtcIndex){
   set <team *, teamIdComp> teams;
   set <program *, programIdComp> programs;
   set <memory *, memoryIdComp> memories;
   getAllNodes(teamMap, teams, programs, memories, false);//not just active programs
   _numActiveTeams = teams.size();
   _numActivePrograms = programs.size();
   _numEffectiveInstructions = 0;
   _numActiveFeatures = 0;
   for (auto leiter = programs.begin(); leiter != programs.end(); leiter++){
      _numEffectiveInstructions += (*leiter)->esize();
      _numActiveFeatures += (*leiter)->numFeatures();
   }
   getMeanOutcome(_TRAIN_PHASE,-1, -1, rtcIndex, false, _runTimeComplexityIns);
   getMeanOutcome(_TRAIN_PHASE,-1, -1, rtcIndex - 1, false, _runTimeComplexityTms);
}


/********************************************************************************************/
//this version returns partial graph up to team tm
void team::getAllNodes(map <long, team*> &teamMap, set <team*, teamIdComp> &visitedTeams, long stopId, bool skipRoot) const {
   if (!skipRoot || !_root)
      visitedTeams.insert(teamMap[_id]);

   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
      if ((*leiter)->action() >= 0 && find(visitedTeams.begin(), visitedTeams.end(), teamMap[(*leiter)->action()]) == visitedTeams.end() && (*leiter)->action() != stopId)
         teamMap[(*leiter)->action()]->getAllNodes(teamMap, visitedTeams, stopId, skipRoot);
}

/********************************************************************************************/
void team::getAllNodes(map <long, team*> &teamMap, set <team*, teamIdComp> &visitedTeams, set <program*, programIdComp> &programs) const {
   visitedTeams.insert(teamMap[_id]);

   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
   {
      programs.insert(*leiter);
      if ((*leiter)->action() >= 0 && find(visitedTeams.begin(), visitedTeams.end(), teamMap[(*leiter)->action()]) == visitedTeams.end())
         teamMap[(*leiter)->action()]->getAllNodes(teamMap, visitedTeams, programs);
   }
}

/********************************************************************************************/
void team::getAllNodes(map <long, team*> &teamMap, set <team*, teamIdComp> &visitedTeams, set <program*, programIdComp> &programs, set <memory *, memoryIdComp> &memories, bool activePrograms) const {
   visitedTeams.insert(teamMap[_id]);

   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
      if (!activePrograms || (activePrograms && _active.find(*leiter) != _active.end())){
         programs.insert(*leiter);
         memories.insert((*leiter)->memGet());
         if ((*leiter)->action() >= 0 && find(visitedTeams.begin(), visitedTeams.end(), teamMap[(*leiter)->action()]) == visitedTeams.end())
            teamMap[(*leiter)->action()]->getAllNodes(teamMap, visitedTeams, programs, memories, activePrograms);
      }
}

/********************************************************************************************/
// Fill F with every feature indexed by every program in this policy (tree).If we ever build 
// massive policy tress, this should be changed to a more efficient traversal. For now just 
// look at every node.
int team::policyFeatures(map <long, team*> &teamMap, set <team*, teamIdComp> &visitedTeams, set < long > &F, bool active) const {
   visitedTeams.insert(teamMap[_id]);

   set < long > featuresSingle;
   int numProgramsInPolicy = 0;
   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
   {
      numProgramsInPolicy++;
      (*leiter)->features(featuresSingle);
      F.insert(featuresSingle.begin(),featuresSingle.end());
      if ((*leiter)->action() >= 0 && find(visitedTeams.begin(), visitedTeams.end(), teamMap[(*leiter)->action()]) == visitedTeams.end())
         numProgramsInPolicy += teamMap[(*leiter)->action()]->policyFeatures(teamMap, visitedTeams, F, active);
   }
   return numProgramsInPolicy;
}

/********************************************************************************************/
void team::policyInstructions(map <long, team*> &teamMap, set <team*, teamIdComp> &visitedTeams, vector < int > &programInstructionCounts, vector < int > &effectiveProgramInstructionCounts) const {
   visitedTeams.insert(teamMap[_id]);

   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
   {
      programInstructionCounts.push_back((*leiter)->size());
      effectiveProgramInstructionCounts.push_back((*leiter)->esize());

      if ((*leiter)->action() >= 0 && find(visitedTeams.begin(), visitedTeams.end(), teamMap[(*leiter)->action()]) == visitedTeams.end())
         teamMap[(*leiter)->action()]->policyInstructions(teamMap, visitedTeams, programInstructionCounts, effectiveProgramInstructionCounts);
   }
}

/********************************************************************************************/
void team::getBehaviourSequence(vector<int>&s, int phase) {
   vector < behaviourType > singleEpisodeBehaviour;
   map < point *, double >::reverse_iterator rit;
   for (rit=_outcomes.rbegin(); rit!=_outcomes.rend(); rit++){
      if ((rit->first)->phase() == phase){
         (rit->first)->getBehaviour(singleEpisodeBehaviour);
         if (s.size() + singleEpisodeBehaviour.size() <= MAX_NCD_PROFILE_SIZE)
            s.insert(s.end(),singleEpisodeBehaviour.begin(),singleEpisodeBehaviour.end()); //will cast to int (only discrete values used)
         else
            break;
      }
   }
}

/********************************************************************************************/
double team::getMeanOutcome(int phase, int task, int fitMode, int auxDouble, bool skipZeros, double &rValue) {
   vector < double > outcomes;

   for(auto ouiter = _outcomes.begin(); ouiter != _outcomes.end(); ouiter++){
      if (((ouiter->first)->phase() == phase && (task == -1 || (ouiter->first)->task() == task) && (fitMode == -1 || (ouiter->first)->fitMode() == fitMode)) &&
            (!skipZeros || (skipZeros && !isEqual((ouiter->first)->auxDouble(auxDouble),0))))
         outcomes.push_back((ouiter->first)->auxDouble(auxDouble));

   }
   if (outcomes.size() == 0)
      die(__FILE__, __FUNCTION__, __LINE__, "trying to get meanOutcome with no outcomes");
   //sort (outcomes.begin(), outcomes.end(), greater<double>());
   rValue = accumulate(outcomes.begin(),outcomes.end(), 0.0) / outcomes.size(); //+(int)(topPortion*outcomes.size()),0.0)/(int)(topPortion*outcomes.size());
   return rValue;
}

/********************************************************************************************/
bool team::getOutcome(point *pt, double *out) {
   map < point *, double, pointLexicalLessThan > :: iterator ouiter;

   if((ouiter = _outcomes.find(pt)) == _outcomes.end())
      return false;

   *out = ouiter->second;

   return true;
}

/********************************************************************************************/
double team::getRMSOutcome(int phase, int auxDouble) {
   double rms = 0;
   for(auto ouiter = _outcomes.begin(); ouiter != _outcomes.end(); ouiter++)
      if ((ouiter->first)->phase() == phase)
         rms += (ouiter->first)->auxDouble(auxDouble);
   return isfinite(rms) ? -(sqrt(rms / _outcomes.size())) : -numeric_limits<double>::max();
}

/********************************************************************************************/
bool team::hasOutcome(point *pt) {
   map < point *, double, pointLexicalLessThan > :: iterator ouiter;

   if((ouiter = _outcomes.find(pt)) == _outcomes.end())
      return false;

   return true;
}

/********************************************************************************************/
/* Calculate normalized compression distance w.r.t another team. */
double team::ncdBehaviouralDistance(team * t, int phase) {
   ostringstream oss;
   vector <int> theirBehaviourSequence;
   t->getBehaviourSequence(theirBehaviourSequence, phase);
   vector <int> myBehaviourSequence;
   getBehaviourSequence(myBehaviourSequence, phase);
   if (myBehaviourSequence.size() == 0 || theirBehaviourSequence.size() == 0)
      return -1;
   return normalizedCompressionDistance(myBehaviourSequence,theirBehaviourSequence);
}

/********************************************************************************************/
int team::numOutcomes(int phase, int task, int fitMode) {
   int numOut = 0;
   for(auto ouiter = _outcomes.begin(); ouiter != _outcomes.end(); ouiter++)
      if (ouiter->first->phase() == phase && (task == -1 || ouiter->first->task() == task) && (fitMode == -1 || ouiter->first->fitMode() == fitMode))
         numOut++;
   return numOut;
}

/********************************************************************************************/
void team::outcomes(int i, int phase, vector < double > &outcomes) {
   map < point *, double, pointLexicalLessThan > :: iterator ouiter;
   for(ouiter = _outcomes.begin(); ouiter != _outcomes.end(); ouiter++)
      if ((ouiter->first)->phase() == phase)
         outcomes.push_back((ouiter->first)->auxDouble(i));
}

/********************************************************************************************/
void team::outcomes(map < point*, double, pointLexicalLessThan > &outcomes,int phase) {
   for(auto ouiter = _outcomes.begin(); ouiter != _outcomes.end(); ouiter++)
      if ((ouiter->first)->phase() == phase)
         outcomes.insert(*ouiter);
}

/********************************************************************************************/
//must run markEffectiveCode first
void team::prunePrograms(){
   if (_active.size() == 0)
      die(__FILE__, __FUNCTION__, __LINE__, "should not prune when no programs are marked active");
   for (auto leiter = _members.begin(); leiter != _members.end();){
      if (((*leiter)->action() < 0 && _numAtomic < 2) || // don't remove the only atomic
            _active.find(*leiter) != _active.end()) //don't remove active programs
         leiter++;
      else{
         (*leiter)->refDec();
         if ((*leiter)->action() < 0)
            _numAtomic--;
         _membersRunTally.erase((*leiter)->id());
         _members.erase(leiter++);
      }
   }
   _membersRun.resize(_members.size());
}

/********************************************************************************************/
team::~team() {
}

/********************************************************************************************/
void team::cleanup() {
   //decrement program refs
   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
      (*leiter)->refDec();
   for (auto ouiter = _outcomes.begin(); ouiter != _outcomes.end();){
      delete ouiter->first;
      _outcomes.erase(ouiter++);
   }
}

/********************************************************************************************/
void team::removeProgram(program *lr) {
   list <program*> :: iterator leiter;
   set <program*, programIdComp> :: iterator aiter;

   if((leiter = find(_members.begin(), _members.end(), lr)) == _members.end())
      die(__FILE__, __FUNCTION__, __LINE__, "should not remove program that is not there");

   _members.erase(leiter);
   _membersRun.resize(_members.size());
   _membersRunTally.erase((*leiter)->id());

   if((aiter = _active.find(lr)) != _active.end())
      _active.erase(aiter);

   if (lr->action() < 0)
      _numAtomic--;
}

/********************************************************************************************/
void team::resetOutcomes(int phase) {
   map < point *, double, pointLexicalLessThan > :: iterator ouiter;
   for (ouiter = _outcomes.begin(); ouiter != _outcomes.end();)
   {
      if ((ouiter->first)->phase() == phase || phase < 0){
         delete ouiter->first;
         _outcomes.erase(ouiter++);
      }
      else
         ouiter++;
   }
}

/********************************************************************************************/
void team::setOutcome(point *pt, double out) {
   if((_outcomes.insert(map < point *, double >::value_type(pt, out))).second == false)
      die(__FILE__, __FUNCTION__, __LINE__, "could not set outcome, duplicate point?");
}

/********************************************************************************************/
void team::updateActiveMembersFromIds( vector < long > &activeMemberIds){
   sort(activeMemberIds.begin(), activeMemberIds.end()); //for binary search
   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++)
      if (binary_search(activeMemberIds.begin(), activeMemberIds.end(), (*leiter)->id()))
         _active.insert(*leiter);
}

/********************************************************************************************
*/
program * team::getAction(state *s, 
      map <long,team*> &teamMap, 
      bool updateActive, 
      set <team*, teamIdComp> &visitedTeams,
      long &decisionInstructions,
      int timeStep,
      vector <team*> &teamPath){

   _depthSum += visitedTeams.size(); _visitedCount++;
   visitedTeams.insert(teamMap[_id]);
   teamPath.push_back(teamMap[_id]);

   int l = 0;
   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++){
      (*leiter)->bidVal((*leiter)->run(s, timeStep, visitedTeams.size()));
      _membersRun[l++] = *leiter;
      decisionInstructions += (*leiter)->esize();
   }

   sort(_membersRun.begin(), _membersRun.end(), ProgramBidLexicalCompare());
   long teamIdToFollow = 0;
   for (size_t i = 0; i < _membersRun.size(); i++){   
      if (_membersRun[i]->action() < 0){
         if(_root && updateActive) 
            _active.insert(_membersRun[i]);
         _membersRunTally[_membersRun[i]->id()]++;
         return _membersRun[i]; //->action();
      } 
      else if (find(visitedTeams.begin(), visitedTeams.end(), teamMap[_membersRun[i]->action()]) == visitedTeams.end()){
         teamIdToFollow = _membersRun[i]->action();
         if(_root && updateActive) 
            _active.insert(_membersRun[i]);
         _membersRunTally[_membersRun[i]->id()]++;
         break;
      }
   }
   return teamMap[teamIdToFollow]->getAction(s, teamMap, updateActive, visitedTeams, decisionInstructions, timeStep, teamPath);
}

/********************************************************************************************
*/
program * team::getAction(state *s, 
      map <long,team*> &teamMap,
      bool updateActive,
      set <team*, teamIdComp> &visitedTeams,
      long &decisionInstructions,
      int timeStep,
      vector< program *> &allPrograms,
      vector< program *> &winningPrograms,
      vector < set <long> > &decisionFeatures,
      vector < set <memory*, memoryIdComp> > &decisionMemories,
      vector <team*> &teamPath)
{
   _depthSum += visitedTeams.size(); _visitedCount++;
   visitedTeams.insert(teamMap[_id]);
   teamPath.push_back(teamMap[_id]);

   set <long> features;
   set <long> featuresSingle;
   set <memory*, memoryIdComp> memories;
   set <memory*, memoryIdComp> memoriesSingle;

   int l = 0;
   for(auto leiter = _members.begin(); leiter != _members.end(); leiter++){
      (*leiter)->bidVal((*leiter)->run(s, timeStep, visitedTeams.size()));
      allPrograms.push_back(*leiter);
      _membersRun[l++] = *leiter;
      decisionInstructions += (*leiter)->esize();

      (*leiter)->features(featuresSingle);
      features.insert(featuresSingle.begin(),featuresSingle.end());
      memories.insert((*leiter)->memGet());
   }
   decisionFeatures.push_back(features);
   decisionMemories.push_back(memories);

   sort(_membersRun.begin(), _membersRun.end(), ProgramBidLexicalCompare());
   long teamIdToFollow = 0; 
   for (size_t i = 0; i < _membersRun.size(); i++){
      if ( _membersRun[i]->action() < 0 ){//atomic
         if(_root && updateActive) _active.insert(_membersRun[i]);
         winningPrograms.push_back(_membersRun[i]);
         _membersRun[i]->featuresMem(featuresSingle);
         decisionFeatures.push_back(featuresSingle);
         _membersRunTally[_membersRun[i]->id()]++;
         return _membersRun[i];
      } 
      else if (find(visitedTeams.begin(), visitedTeams.end(), teamMap[_membersRun[i]->action()]) == visitedTeams.end()){
         teamIdToFollow = _membersRun[i]->action();
         if(_root && updateActive) _active.insert(_membersRun[i]);
         winningPrograms.push_back(_membersRun[i]);
         _membersRun[i]->featuresMem(featuresSingle);
         decisionFeatures.push_back(featuresSingle);
         _membersRunTally[_membersRun[i]->id()]++;
         break;
      }
   }
   return teamMap[teamIdToFollow]->getAction(s, teamMap, updateActive, visitedTeams, decisionInstructions, timeStep, allPrograms, winningPrograms, decisionFeatures, decisionMemories, teamPath);
}

