#ifndef point_h
#define point_h

#include <vector>
#include <map>
#include <set>
#include "misc.h"

#define POINT_AUX_INT_TASK 0
#define POINT_AUX_INT_FITMODE 1
#define POINT_AUX_INT_PHASE 2
#define POINT_AUX_INT_ENVSEED 3

using namespace std;

class point
{
   public:

      /********************************************************************************************
       * Methods that set or query the state of this point.
       ********************************************************************************************/
      inline double auxDouble(int i){ return _auxDoubles[i]; }
      inline void auxDouble(int i, double d) { _auxDoubles[i] = d; }
      inline double auxInt(int i){ return _auxInts[i]; }
      inline void auxInt(int i, int j) { _auxInts[i] = j; }
      inline void auxDoubles( vector<double> &v) const {v = _auxDoubles; }
      inline void auxInts( vector<int> &v) const {v = _auxInts; }
      inline int task() const { return _auxInts[POINT_AUX_INT_TASK]; }
      inline int fitMode() const { return _auxInts[POINT_AUX_INT_FITMODE]; }
      inline int phase() const { return _auxInts[POINT_AUX_INT_PHASE]; }
      inline int envSeed() const { return _auxInts[POINT_AUX_INT_ENVSEED]; }
      inline void getBehaviour(vector < behaviourType > &v) const {v = _behaviour; }
      inline long gtime(){ return _gtime; }
      inline long id(){ return _id; }
      inline void id(long i){ _id = i; }
      inline double key(){ return _key; }
      inline void key(double key){ _key = key; }
      void setBehaviour(vector < behaviourType > &v){ _behaviour = v; }

      /********************************************************************************************
       * Constructors
       ********************************************************************************************/
      point(long, long, vector< behaviourType > &, vector < double > &, vector < int > &);

      point(point &p);

      /********************************************************************************************
       * Other
       ********************************************************************************************/
      bool isPointUnique(set < point * > &); /* Return true if the point is unique w.r.t. another point. */
      friend ostream & operator<<(ostream &, const point &);

   protected:

      /********************************************************************************************
       *  Member variables and data structures.
       ********************************************************************************************/
      vector <double> _auxDoubles;
      vector <int> _auxInts; //task, fitMode, phase, envSeed
      vector < behaviourType > _behaviour;
      long _gtime;
      long _id;
      double _key; /* For sorting. */
};

struct pointLexicalLessThan {
   bool operator()( point* p1 , point* p2) const
   {
      //if (!isEqual(p1->key(), p2->key()))
      //   return p1->key() > p2->key(); 
      return  p1->id() < p2->id(); //correlated to age but technically arbitrary, id is guaranteed to be unique and thus ensures deterministic comparison
   }
};

#endif
