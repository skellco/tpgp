#ifndef team_h
#define team_h

#include <map>
#include "misc.h"
#include "program.h"
#include "point.h"
#include "state.h"
#include <set>
#include <list>
#include <queue>

#define MEMBERS_RUN_ENTROPY_INDEX 3
#define NUM_TEAM_DISTANCE_MEASURES 3
#define MAX_NCD_PROFILE_SIZE 20000 //arbitrarily large, greater than max ALE frames/episode of 18000

using namespace std;

struct teamIdComp;

class team
{
   public:
      /********************************************************************************************
       * Methods that set or query the state of this team.
       ********************************************************************************************/
      inline void activeMembers(set < program *, programIdComp > *m) const { m->clear(); m->insert(_active.begin(), _active.end()); }
      inline void addAncestorId(long id) { _ancestorIds.insert(id); }
      inline void getActiveMembersByRef(set < program *, programIdComp > &m) const { m = _active; }
      inline void getAncestorIds(set < long > &a) const { a = _ancestorIds; }
      inline void setAncestorIds(set < long > &a) { _ancestorIds.clear(); _ancestorIds.insert(a.begin(), a.end()); }
      inline int asize() const { return _active.size(); } /* The number of active programs in this team. */
      void cleanup();
      inline void clearDepthTally() { _depthSum = _visitedCount = 0; }
      inline void clearMembersRunTally() { for ( auto it = _membersRunTally.begin(); it != _membersRunTally.end(); it++ ) it->second = 0; }
      inline double depthTally() const { return _visitedCount == 0 ? 0 : (double)_depthSum/_visitedCount; }
      inline size_t depthSum() { return _depthSum; }
      inline void depthSum(int i) { _depthSum = (size_t)i; }
      inline size_t depthSumCount() { return _visitedCount; }
      inline void depthSumCount(int i) { _visitedCount = (size_t)i; }
      inline int domBy() const { return _domBy; }
      inline void domBy(int d) { _domBy = d; }
      inline int domOf() { return _domOf; }
      inline void domOf(int d){ _domOf = d; }
      void features(set < long > &) const;
      inline int fitnessBin() const { return (_fitnessBins.rbegin())->second; }
      inline int fitnessBin(long t) const { 
         auto search = _fitnessBins.find(t);
         if (search != _fitnessBins.end())
            return search->second;
         else return -2;//code for not found
      }
      inline void fitnessBin(long t, int task) { _fitnessBins[t] = task; }
      inline void fitnessBins(map <long, int> &fb) { _fitnessBins.clear(); _fitnessBins.insert(fb.begin(), fb.end()); }
      inline map <long, int> fitnessBins() { return _fitnessBins; }
      void getAllNodes(map <long, team*> &teamMap, set <team *, teamIdComp> &, long, bool) const;
      void getAllNodes(map <long, team*> &teamMap, set <team *, teamIdComp> &, set <program *, programIdComp> &) const;
      void getAllNodes(map <long, team*> &teamMap, set <team *, teamIdComp> &, set <program *, programIdComp> &, set<memory *, memoryIdComp> &, bool) const;
      void getBehaviourSequence(vector <int> &, int);
      inline double fit() const { return _fit; }
      inline void fit(double f) { _fit = f; }
      double getMeanOutcome(int, int task, int fitMode, int auxDouble, bool skipZeros, double &rValue);
      bool getOutcome(point *, double *); /* Get outcome, return true if found. */
      double getRMSOutcome(int phase, int auxDouble);
      inline long gtime() const { return _gtime; }
      bool hasOutcome(point * p); /* Return true if team has an outcome for this point. */
      inline long id() const { return _id; }
      inline void id(long id) { _id = id; }
      inline int inDeg() const { return _inDeg; }
      inline void inDeg(int i) { _inDeg = i; } 
      inline double key() const { return _key; }
      inline void key(double key) { _key = key; }
      inline int lastCompareFactor() const { return _lastCompareFactor; }
      inline void lastCompareFactor(int c) { _lastCompareFactor = c; }
      inline void members(set < program *, programIdComp > *m) const { 
         m->clear(); 
         //m->insert(_members.begin(), _members.end()); 
         copy(_members.begin(),_members.end(), inserter(*m, m->end()));
      }
      inline void members(list < program * > *m) const { m->assign(_members.begin(), _members.end()); }
      inline void members(set < program *, programIdComp > &m) const { 
         m.clear(); 
         //m.insert(_members.begin(), _members.end()); 
         copy(_members.begin(),_members.end(), inserter(m, m.end()));
      }
      double membersRunEntropy(){
         double e = 0;
         for ( auto it = _membersRunTally.begin(); it != _membersRunTally.end(); it++ ){
            if (it->second > 0){
               it->second /= _visitedCount;
               e += it->second * log2(it->second);
            }
         }
         return -e;
      }
      inline void getMembersRef(list < program * > *&m) { m = &_members; }
      inline size_t numActiveFeatures() const { return _numActiveFeatures; }
      inline size_t numActivePrograms() const { return _numActivePrograms; }
      inline size_t numActiveTeams() const { return _numActiveTeams; }
      inline void numActiveTeams(size_t i) { _numActiveTeams = i; }
      inline size_t numEffectiveInstructions () const { return _numEffectiveInstructions; }
      double novelty(int, int) const;
      inline size_t numAtomic() const { return _numAtomic; }
      inline size_t numEval() const { return _numEval; }
      inline void numEval(size_t ne) { _numEval = ne; }
      int numOutcomes(int phase, int task, int fitMode = -1); /* Number of outcomes. */
      void outcomes(map<point*, double, pointLexicalLessThan>&, int); /* Get all outcomes from a particular phase */
      void outcomes(int, int, vector < double >&); /* Get all outcome values of a particular type and from a particular phase.*/
      inline void policyFeaturesGet(set< long > &f, bool active) const { if (active) f = _policyFeaturesActive; else f = _policyFeatures; }
      inline void policyFeaturesSet(set< long > &f, bool active) { if (active) _policyFeaturesActive = f; else _policyFeatures = f; }
      int policyFeatures(map <long, team*> &teamMap, set <team*, teamIdComp> &, set < long > &, bool) const; //populates last set with features and returns number of nodes(programs) in policy
      void policyInstructions(map <long, team*> &, set <team*, teamIdComp> &, vector < int > &, vector < int > &) const;
      void prunePrograms();
      void removeProgram(program *);
      void resetOutcomes(int); /* Delete all outcomes from phase. */
      inline bool root() const { return _root; }
      void root(bool r){ _root = r; }
      inline double runTimeComplexityIns() const { return _runTimeComplexityIns; }
      inline void runTimeComplexityIns(double rtc) { _runTimeComplexityIns = rtc; }
      inline double runTimeComplexityTms() const { return _runTimeComplexityTms; }
      inline void runTimeComplexityTms(double rtc) { _runTimeComplexityTms = rtc; }
      inline void setActive(program *l) { _active.insert(l); }
      inline void unSetActive(program *l) { _active.erase(l); }
      void setOutcome(point *, double); /* Set outcome. */
      inline size_t size() const { return _members.size(); } /* This is the team size of the root node. */
      double symbiontUtilityDistance(team*) const;
      double symbiontUtilityDistance(vector < long > &) const;
      inline void swapProgramOrder(int i, int j){
         auto leiter_i = _members.begin();
         auto leiter_j = _members.begin();
         advance(leiter_i, i);
         advance(leiter_j, j);
         swap(*leiter_i, *leiter_j);
      }
      inline void muProgramOrder(int source, int destination){
         auto leiter_source = _members.begin();
         auto leiter_destination = _members.begin();
         advance(leiter_source, source);
         advance(leiter_destination, destination);
         _members.splice(leiter_destination, _members, leiter_source);
      }

      void updateComplexityRecord(map <long, team*> &, size_t);

      /********************************************************************************************
       * Constructors
       ********************************************************************************************/

      //this constructor used for initialization and checkpointing
      team(long gtime, long id){
         _clone = false;
         _depthSum = 0;
         _visitedCount = 0;
         _domBy = -1;
         _domOf = -1;
         _fit = 0.0;
         _gtime = gtime;
         _id = id;
         _inDeg = 0;
         _key = 0;
         _lastCompareFactor = -1;
         _numAtomic = 0;
         _numEval = 0;
         _numLinearM = 0;
         _root = true;
      };

      ~team(); /* Affects program refs, unlike addProgram() and removeProgram(). */

      /********************************************************************************************
       * Other
       ********************************************************************************************/

      inline void addDistance(int type, double d){ if (type == 0) _distances_0.insert(d); else if (type == 1) _distances_1.insert(d); else if (type == 2) _distances_2.insert(d);}
      bool addProgram(program *, int i = -1);
      bool addProgramActive(program *);
      string checkpoint(bool) const;
      void clone(team **);
      inline bool clone() const { return _clone; }
      inline void clone(bool c) { _clone = c; }
      inline void clearDistances(){ _distances_0.clear(); _distances_1.clear(); _distances_2.clear();}
      void deleteOutcome(point *); /* Delete outcome. */
      program * getAction(state *, map <long, team*> &, bool, set <team*, teamIdComp> &, long &, int, vector <team*> &);
      program * getAction(state *, map <long, team*> &, bool, set <team*, teamIdComp> &, long &, int, vector <program*> &, vector <program*> &,  vector < set <long> > &, vector < set <memory*, memoryIdComp> > &, vector<team*> &);
      double ncdBehaviouralDistance(team*, int);
      void shuff(mt19937 &rng){
         vector< program* > vec(_members.begin(), _members.end()) ;
         shuffle( vec.begin(), vec.end(), rng );
         list<program*> shuffled_list {vec.begin(), vec.end()};
         _members.swap(shuffled_list);
      }
      void updateActiveMembersFromIds( vector < long > &);

   protected:

      /********************************************************************************************
       *  Member variables and data structures.
       ********************************************************************************************/
      set < program *, programIdComp > _active; /* Active member programs, a subset of _members, activated in getAction(). */
      set < long > _ancestorIds;
      bool _clone;
      size_t _depthSum; //sum of graph depths up to this team (over all executions)
      size_t _visitedCount; //number of executions for this team (for depth sum average)
      multiset <double> _distances_0;
      multiset <double> _distances_1;
      multiset <double> _distances_2;
      int _domBy; /* Number of other teams dominating this team. */
      int _domOf; /* Number of other teams that this team dominates. */
      map<long, int> _fitnessBins; // map of t -> task# keeps track of which fitness bin this team was protected by in each gen (-1 for multi-task)
      double _fit; /* Fitness value used for selection */	
      long _gtime; /* Time step at which generated. */
      long _id; /* Unique id of team. */
      int _inDeg; /* in degree */
      double _key; /* For sorting. */
      int _lastCompareFactor;
      list < program * > _members;
      vector < program * > _membersRun;
      map <long, double > _membersRunTally;//keep count of wins for each program
      int _numAtomic;
      size_t _numEval;
      int _numLinearM;
      size_t _numActiveTeams;
      size_t _numActivePrograms;
      size_t _numEffectiveInstructions;
      size_t _numActiveFeatures;
      map < point *, double, pointLexicalLessThan > _outcomes; /* Maps point->outcome. */
      set < long >  _policyFeatures;
      set < long >  _policyFeaturesActive;
      bool _root;
      double _runTimeComplexityIns;
      double _runTimeComplexityTms;
};

struct teamIdComp
{
   bool operator() ( team* t1, team* t2) const
   {
      return t1->id() > t2->id();
   }
};

struct teamFitnessLexicalCompare {
   bool operator()(team* t1, team* t2) const {
      //double d1, d2;
      if (!isEqual(t1->fit(), t2->fit())){ 
         t1->lastCompareFactor(1); 
         t2->lastCompareFactor(1); 
         return t1->fit() > t2->fit();
      }
      //else if (!isEqual(t1->getMeanOutcome(_TRAIN_PHASE, -1, -1, MEMBERS_RUN_ENTROPY_INDEX, false, d1), t2->getMeanOutcome(_TRAIN_PHASE, -1, -1, MEMBERS_RUN_ENTROPY_INDEX, false, d2))){
      //   t1->lastCompareFactor(2);
      //   t2->lastCompareFactor(2);
      //   return d1 < d2;  
      //}
      //else if (!isnan(t1->runTimeComplexityIns()) && !isnan(t2->runTimeComplexityIns()) && !isEqual(t1->runTimeComplexityIns(), t2->runTimeComplexityIns())){
      //   t1->lastCompareFactor(2);
      //   t2->lastCompareFactor(2);
      //   //cout << "teamLexComp lcf 2 " << t1->runTimeComplexityIns() << " (<) " << t2->runTimeComplexityIns() << endl;
      //   return t1->runTimeComplexityIns() < t2->runTimeComplexityIns();
      //}
      //else if (t1->numEffectiveInstructions() != t2->numEffectiveInstructions()){
      //   t1->lastCompareFactor(3);
      //   t2->lastCompareFactor(3);
      //   //cout << "teamLexComp lcf 3 " << t1->numEffectiveInstructions() << " (<) " << t2->numEffectiveInstructions() << endl;
      //   return t1->numEffectiveInstructions() < t2->numEffectiveInstructions();
      //}
      //else if (t1->numActivePrograms() != t2->numActivePrograms()){
      //   t1->lastCompareFactor(4);
      //   t2->lastCompareFactor(4);
      //   //cout << "teamLexComp lcf 4 " << t1->numActivePrograms() << " (<) " << t2->numActivePrograms() << endl;
      //   return t1->numActivePrograms() < t2->numActivePrograms();
      //}
      //else if (t1->numActiveTeams() != t2->numActiveTeams()){
      //   t1->lastCompareFactor(5);
      //   t2->lastCompareFactor(5);
      //   //cout << "teamLexComp lcf 5 " << t1->numActiveTeams() << " (<) " << t2->numActiveTeams() << endl;
      //   return t1->numActiveTeams() < t2->numActiveTeams();
      //}
      //else if (t1->gtime() != t2->gtime()){
      //   t1->lastCompareFactor(6);
      //   t2->lastCompareFactor(6);
      //   //cout << "teamLexComp lcf 6 " << t1->gtime() << " (>) " << t2->gtime() << endl;
      //   return t1->gtime()  > t2->gtime();//younger is better
      //}
      else { 
         t1->lastCompareFactor(7); 
         t2->lastCompareFactor(7);
         //cout << "teamLexComp lcf 7 " << t1->id() << " (>) " << t2->id() << endl; 
         return t1->id() > t2->id();
      } 
   }
};

struct teamInDegCompare {
   bool operator()(team* t1, team* t2) {
      return t1->inDeg() < t2->inDeg();
   }
};

#endif
