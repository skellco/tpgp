#ifndef TPG_H
#define TPG_H
#include <random>
#include "point.h"
#include "team.h"
#include "linearM.h"
#include "state.h"
#include "memory.h"
#include <iomanip>

class TPG
{
   public:
      TPG(int seed = 0);
      ~TPG();

      /********************************************************************************************
       * Methods that set or query parameters and state.
       ********************************************************************************************/
      //inline void clearProfilePoints() { _profilePointsFIFO.clear(); }
      inline int activeTask() const { return _activeTask; }
      inline void activeTask(int i){ _activeTask = i; }
      inline bool animate() const { return _animate; }
      inline void animate(bool b){ _animate = b; }
      inline bool continuousOutput() const { return _continuousOutput; }
      inline void continuousOutput(bool co) { _continuousOutput = co; } 
      inline bool checkpoint() const { return _checkpoint; }
      inline void checkpoint(bool c) { _checkpoint = c; }
      inline int checkpointInPhase() const { return _checkpointInPhase; }
      inline void checkpointInPhase(int c) { _checkpointInPhase = c; }
      inline int dim() const { return _dim; }
      inline void dim(int d){ _dim = d; }
      inline int diversityMode() const { return _diversityMode; }
      inline void diversityMode(int i){ _diversityMode = i; }
      inline team * getTeamByID(long id){
         for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
            if ((*teiter)->id() == id)
               return *teiter;
         return*(_M.begin());
      }
      inline int genTime(size_t t) const { return _genTimes[t]; }
      inline int hostDistanceMode() const { return _distMode; }
      inline void hostDistanceMode(int i){ _distMode = i; }
      inline int hostToReplay() const { return _hostToReplay; }
      inline void hostToReplay(int i){ _hostToReplay = i; }
      inline int hostFitnessMode() const { return _fitMode; }
      inline void hostFitnessMode(int i){ _fitMode = i; }
      inline int function() const { return _function; }
      inline void function(int f) { _function = f; }
      inline team * getEliteTeam(int task, int phase) const { return _eliteTeamsEachTask[phase][task]; }
      inline team * getEliteTeamMT(int phase) const { return _eliteTeamMT[phase]; }
      inline void getEliteTeams(set <team *, teamIdComp> &v) const { v = _eliteTeams; }
      inline team * getMrootBegin() const { return *_Mroot.begin(); }
      inline program * getLBegin() const { return * _L.begin(); }
      inline int id() const { return _id; }
      inline void id(long id) { _id = id; }
      inline void programs(set < program *, programIdComp > &l) const { l = _L; }
      inline void programs(map <long, program * > &l) const { 
         l.clear();
         for(auto leiter = _L.begin(); leiter != _L.end(); leiter++)
            l[(*leiter)->id()] = *leiter; 
      }
      inline double maxGenMillis() const { return _maxGenMillis; }
      inline size_t maxInitialProgSize() const { return _maxInitialProgSize; }
      inline size_t maxProgSize() const { return _maxProgSize; }
      inline size_t maxInitialTeamSize() const { return _maxInitialTeamSize; }
      inline size_t maxTeamSize() const { return _maxTeamSize; }
      inline void memories(set < memory *, memoryIdComp > &m) const { m = _Memory; }
      inline size_t numEliteTeams() const { return _eliteTeams.size(); }
      inline size_t numEvalPerGeneration() const { return _numEvalPerGeneration; }
      inline double numPointAuxDouble() const { return _numPointAuxDouble; }
      inline void  numPointAuxDouble(size_t i) { _numPointAuxDouble = i;}
      inline double numPointAuxInt() const { return _numPointAuxInt; }
      inline void  numPointAuxInt(size_t i) { _numPointAuxInt = i;}
      inline size_t numRoot() const { return _Mroot.size(); }
      inline int numFitMode() const { return _numFitMode; }
      inline void numFitMode(int fm){ _numFitMode = fm; }
      inline size_t numTask() const { return _numTask; }
      inline void numTask(size_t nt){ 
         _numTask = nt; 
         _eliteTeamsEachTask.resize(3); //num phase
         _eliteTeamsEachTask[_TRAIN_PHASE].resize(nt); 
         _eliteTeamsEachTask[_VALIDATION_PHASE].resize(nt);
         _eliteTeamsEachTask[_TEST_PHASE].resize(nt);
         _eliteTeamMT.resize(3);
      }
      inline int numStoredOutcomesPerHost(int phase) const { return _numStoredOutcomesPerHost[phase]; }
      inline void numStoredOutcomesPerHost(int phase, long nso) { _numStoredOutcomesPerHost[phase] = nso; }
      inline size_t omega() const { return _maxInitialTeamSize; }
      inline double pAddProfilePoint() const { return _pAddProfilePoint; }
      inline double pAtomic() const { return _pAtomic; }
      inline double paretoEpsilonTeam() const { return _paretoEpsilonTeam; }
      inline bool partiallyObservable() const { return _partiallyObservable; }
      inline void partiallyObservable(bool po) { _partiallyObservable = po; }
      inline double pBidAdd() const { return _pBidAdd; }
      inline double pBidMutate() const { return _pBidMutate; }
      inline double pBidDelete() const { return _pBidDelete; }
      inline double pBidSwap() const { return _pBidSwap; }
      inline double pma() const { return _pma; }
      inline double pmd() const { return _pmd; }
      inline double pmm() const { return _pmm; }
      inline double pmn() const { return _pmn; }
      inline double pms() const { return _pms; }
      inline double pmw() const { return _pmw; }
      inline double pmx() const { return _pmx; }
      inline int phase() const { return _phase; }
      inline void phase(int p) { _phase = p; }
      inline bool replay() { return _replay; }
      inline void replay(bool r) { _replay = r; }
      inline void reproduce(bool r) { _reproduce = r; }
      void readGenTimes(char* filename){
         _genTimes.clear();
         ifstream t(filename);
         t.seekg(0, ios::end);
         string str;
         str.reserve(t.tellg());
         t.seekg(0, ios::beg);
         str.assign((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
         istringstream iss(str);
         string oneline;
         char delim=':';
         vector < string > outcomeFields;
         while (getline(iss, oneline)){
            outcomeFields.clear();
            split(oneline,delim,outcomeFields);
            if (outcomeFields[0].compare("genTimes") == 0)
               for (size_t gt = 1; gt < outcomeFields.size(); gt++)
                  _genTimes.push_back(atoi(outcomeFields[gt].c_str()));
         }
      }
      inline double rGap() const { return _Rgap; }
      inline size_t rSize() const { return _Rsize; }
      inline void rSize(size_t r) { _Rsize = r; }
      inline int seed() const { return _seed; }
      inline void seed(int s) { _seed = s; }
      inline int seed2() const { return _seed2; }
      inline void seed2(int s) { _seed2 = s; }
      inline void setMiniBatchIndexes(vector <int> &idx) { _miniBatchIndexes.clear(); _miniBatchIndexes.assign(idx.begin(), idx.end()); }
      inline void getMiniBatchIndexes(vector <int> &idx) const { idx = _miniBatchIndexes; }
      inline void setupTeamCount(int s) { _teamCount = 1000*s; }
      inline bool skipIntrons() const { return _skipIntrons; }
      inline void skipIntrons(bool s) {_skipIntrons = s; }
      inline bool stateful() const { return _stateful; }
      inline void stateful(bool s) {_stateful = s; }
      inline int taskSwitchMod() const { return _taskSwitchMod; }
      inline void taskSwitchMod(int i) { _taskSwitchMod = i; }
      inline void teams(set < team *, teamIdComp > &t) const { t = _M; }
      inline long tCurrent() const { return _tCurrent; }
      inline void tCurrent(long t) { _tCurrent = t; }
      inline long tMain() { return _tMain; }
      inline void tMain(long t) { _tMain = t; }
      inline long tPickup() const { return _tPickup; }
      inline void tPickup(long t) { _tPickup = t; }
      inline long tStart() { return _tStart; }
      inline void tStart(long t) { _tStart = t; }
      inline bool verbose() const { return _verbose; }
      inline void verbose(bool v){ _verbose = v; }
      inline bool visual() { return _visual; }
      inline void visual (bool v) { _visual = v; }
      inline bool writeCheckpoints() { return _writeCheckpoints; }
      inline void writeCheckpoints(bool wc) { _writeCheckpoints = wc; }

      /********************************************************************************************
       * Methods to implement the TPG algorithm.
       ********************************************************************************************/
      void checkRefCounts(const char *);
      void clearMemory(){
         for(auto meiter = _Memory.begin(); meiter != _Memory.end(); meiter++){
            (*meiter)->clear();
            (*meiter)->clearReadTime();
            (*meiter)->clearWriteTime();
         }
      }
      void countRefs();
      void finalize();
      void genTeams(long, mt19937 &);
      void genTeams(long, team *, team *, bool, team **, size_t &, mt19937 &);
      int genUniqueProgram(program *, set < program *, programIdComp >);

      program * getAction(team* tm, state *s, bool updateActive, set < team *, teamIdComp > &visitedTeams, long &decisionInstructions, int timeStep, vector <team*> &teamPath)
      {
         visitedTeams.clear();
         decisionInstructions = 0;
         teamPath.clear();
         return tm->getAction(s, _teamMap, updateActive, visitedTeams, decisionInstructions, timeStep, teamPath);
      }

      program * getAction(team* tm, state *s, bool updateActive, set < team *, teamIdComp > &visitedTeams, long &decisionInstructions, int timeStep, vector <program*> &allPrograms, vector <program*> &winningPrograms, vector < set <long> > &decisionFeatures, vector < set <memory*, memoryIdComp> > &decisionMemories, vector <team*> &teamPath)
      {
         allPrograms.clear();
         winningPrograms.clear();
         decisionInstructions = 0;
         decisionFeatures.clear();
         decisionMemories.clear();
         visitedTeams.clear();
         teamPath.clear();
         return tm->getAction(s, _teamMap, updateActive, visitedTeams, decisionInstructions, timeStep, allPrograms, winningPrograms, decisionFeatures, decisionMemories, teamPath);
      }
      inline void getAllNodes(team* tm, set < team *, teamIdComp > &teams, set < program *, programIdComp > &programs){
         teams.clear();
         programs.clear(); 
         tm->getAllNodes(_teamMap, teams, programs);
      }
      inline void getAllNodes(team* tm, set < team *, teamIdComp > &teams, set < program *, programIdComp > &programs, set < memory *, memoryIdComp > &memories){
         teams.clear();
         programs.clear();
         memories.clear();
         tm->getAllNodes(_teamMap, teams, programs, memories, false);
      }
      team * getBestTeam();
      inline void getTeams(vector <team*> &t, bool roots) const {
         if (roots)
            t.assign(_Mroot.begin(), _Mroot.end());
         else
            t.assign(_M.begin(), _M.end());
      }
      inline void getTeams(map <long, team*> &t, bool roots) const {
         t.clear();
         if (roots)
            for(auto teiter = _Mroot.begin(); teiter != _Mroot.end(); teiter++)
               t[(*teiter)->id()] = *teiter;
         else
            for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
               t[(*teiter)->id()] = *teiter;
      }
      void initTeams(mt19937 &);
      inline bool isElite(team * tm, int phase) const {
         if (_eliteTeams.find(tm) != _eliteTeams.end())
            return true;
         for (size_t t = 0; t < _numTask; t++)
           if (tm->id() == _eliteTeamsEachTask[phase][t]->id()) 
              return true;
         if (tm->id() == _eliteTeamMT[phase]->id())
            return true;
         return false;
      }
      inline long programCount() const { return _programCount; }
      inline void programCount(long lc){ _programCount = lc; }
      inline int lSize(){ return _L.size(); }
      void markEffectiveCode(team *tm, bool skip){
         set <team*, teamIdComp> teams;
         set <program*, programIdComp > programs;
         set <memory*, memoryIdComp > memories;

         tm->getAllNodes(_teamMap, teams, programs, memories, false);

         for(auto meiter = memories.begin(); meiter != memories.end(); meiter++){
            (*meiter)->clearActive();
            (*meiter)->refsPolicy(0);
         }

         //for(auto leiter = programs.begin(); leiter != programs.end(); leiter++)
         //   ((*leiter)->memGet())->refsPolicyInc();
        
         for(auto leiter = programs.begin(); leiter != programs.end(); leiter++){
            (*leiter)->markIntrons(_continuousOutput);
            (*leiter)->skipIntrons(skip);
            ((*leiter)->memGet())->refsPolicyInc();
         }

         //update active programs
         list < program * > rootMembers;
         tm->members(&rootMembers);
         for(auto leiter = rootMembers.begin(); leiter != rootMembers.end(); leiter++)
            if ((*leiter)->esize() > 0 && ((*leiter)->memGet())->refsPolicy() > 1)
               tm->setActive(*leiter);
      }
      inline long memoryCount() const { return _memoryCount; }
      inline void memoryCount(long mc){ _memoryCount = mc; }
      inline int mSize(){ return _M.size(); }
      inline int numAtomicActions() const {  return _numActions; }
      inline void numAtomicActions(int a) { _numActions = a; }
      void policyFeatures(int,set<long> &, bool);
      inline long pointCount() const { return _pointCount; }
      inline void pointCount(long pc){ _pointCount = pc; }
      void printGraphDot(team*, size_t frame, int episode, int step, size_t depth, vector <program*> allPrograms, vector <program*> winningPrograms, vector < set <long> > decisionFeatures, vector< set< memory*, memoryIdComp> > decisionMemories, vector <team*> teamPath, bool drawPath=true);
      void printHostGraphsDFS(long, long);
      void printHostGraphsHostsOnly(long, long);
      void printPhyloGraphDot();
      void printProgramInfo();
      void printOss() { cout << oss.str(); oss.str(""); }
      void printOss(ostringstream &o) { oss << o.str(); o.str(""); }
      void printTeamInfo(long, int, bool, long teamId=-1);
      /* Remove inactive programs. */
      void readCheckpoint (long, int, int, bool, const string &);
      void recalculateProgramRefs();
      inline void resetOutcomes(int phase, bool roots){
         if (roots)
            for(auto teiter = _Mroot.begin(); teiter != _Mroot.end(); teiter++)
               (*teiter)->resetOutcomes(phase);
         else
            for(auto teiter = _M.begin(); teiter != _M.end(); teiter++)
               (*teiter)->resetOutcomes(phase);
      }
      void selTeams(long, bool, int, bool);
      void setEliteTeams(long, int, bool);
      /* Team tm will store a clone of the point with the same id. */
      void setOutcome(team* tm, vector < behaviourType > &state, vector<double> &rewards, vector<int> &ints, long gtime){
         point *p = new point(gtime, _pointCount++, state, rewards, ints);
         p->key(rewards[_fitMode]);
         tm->setOutcome(p , rewards[_fitMode]);
      }
      void setParams(bool verbose = false);
      void setSingleTaskFitness(int);
      inline long teamCount() const { return _teamCount; }
      inline void teamCount(long tc){ _teamCount = tc; }
      inline void teamMap(map <long, team*> &tm) const { tm = _teamMap; }
      void teamTaskRank(int, const vector <int> &);
      void updateProgramProfiles();
      void updateMODESFilters(bool);
      void writeCheckpoint(long, bool);
      void writeCheckpoint(string &, vector < team*> &);

      /********************************************************************************************
       *  TPG member variables and data structures.
       ********************************************************************************************/

   protected:
      /* Populations */
      set < team*, teamIdComp > _M; // Teams 
      set < team*, teamIdComp > _Mroot; // Root teams for fast lookup 
      map < long, team* > _teamMap; // Map team id -> team* for program graph traversal 
      set < program*, programIdComp > _L; // Programs
      long _programCount; //number of programs that have ever existed
      set < memory *, memoryIdComp > _Memory;
      long _memoryCount; //number of memory banks that have ever existed
      long _pointCount; //number of points that have ever existed

      /* Phylogeny data */
      map <long, phyloRecord > _phyloGraph;
      map <long, modesRecord > _allComponentsA;
      map <long, modesRecord > _allComponentsAt;
      map <long, modesRecord > _persistenceFilterA;
      map <long, modesRecord > _persistenceFilterA_t;
      map <long, modesRecord > _persistenceFilterS;

      vector <int> _genTimes;
      vector <int> _miniBatchIndexes;

      //command line parameters
      bool _animate;
      bool _checkpoint; //wheather or not to start from a checkpoint file, default:false
      int _checkpointInPhase; //phase of the checkpoint file, default:0
      int _dim; //number of state variables, default:1
      int _distMode; //code for distance function used to compare agents, default:0
      set < team *, teamIdComp > _eliteTeams; //container for all elite teams
      vector < vector <team *> > _eliteTeamsEachTask; //container for single best team for each task
      vector < team * > _eliteTeamMT; //single best multi-task team
      int _fitMode; //code for various fitness functions, default:0
      int _function; //code for various symbolic regression problem functions
      long _hostToReplay; //agent id to replay; default:-1
      int _id; //tpg instance id, default:-1
      bool _partiallyObservable; //partially-observable state
      bool _replay; //replay an agent, defult:false
      bool _reproduce;
      int _seed; //random seed
      int _seed2; //another random seed
      bool _skipIntrons;
      bool _stateful; //should we use temporal memory, default:true
      long _teamCount; //number of teams that have ever existed
      int _taskSwitchMod; //rate of task switching
      long _tMain; //number of generations
      long _tPickup;//generation of checkpoint file
      long _tStart; //generation at which to start evolution
      bool _verbose;
      bool _visual;
      bool _writeCheckpoints;

      //parameters set in main method for experiment
      size_t _numTask;
      int _activeTask;
      size_t _numPointAuxDouble; //number of auxillary doubles stored with points, default:0
      size_t _numPointAuxInt;
      size_t _numActions; //number of atomic actions, default:2
      size_t _numFitMode; //number of fitness functions, default:1 
      size_t _numProfilePoints; //number of profile points used for neutrality test, default:0
      long _numStoredOutcomesPerHost[_NUM_PHASE];
      ostringstream oss; // logging, reporting
      int _phase; //0:train 1:validation 2:test
      long _tCurrent;

      //parameter set in genScript.pl
      bool _continuousOutput;
      int _diversityMode; //code for various diversity maintenance techniques, default:0
      size_t _initialTeamSize;
      int _knnNov; //default:15
      double _maxGenMillis;
      size_t _maxTeamsPerGraph;
      size_t _maxProgSize; 
      size_t _maxTeamSize; 
      size_t _maxRunTimeComplexityIns;
      size_t _maxRunTimeComplexityTms;
      size_t _numEvalPerGeneration;
      double _Rgap; //team generation gap as percentage. 
      size_t _numElite; //number of elite teams to protect for each task in multitasklearning 
      size_t _Rsize; //mumber of root teams in the team population. 
      size_t _maxInitialProgSize; 
      size_t _maxInitialTeamSize; 
      double _paretoEpsilonTeam; 
      double _pAddProfilePoint;  
      double _pAtomic; //probability of modified action being atomic 
      double _pBidAdd; //probability of adding program instruction 
      double _pBidDelete;  //probability of deleting program instruction 
      double _pBidMutate; //probability of modifying program instruction 
      double _pBidSwap; //probability of swapping position of two program instructions 
      double _pma; //probability of program addition 
      double _pmd; //probability of program deletion 
      double _pmm; //probability of program mutation 
      double _pmn; //probability of program action mutation 
      double _pms; //probability of program memory ref mutation 
      double _pmw; //probability of team exec order mutation 
      double _pmx; //probability of team crossover

      uniform_real_distribution<> _disR; //random reals in [0,1]    
};

#endif
