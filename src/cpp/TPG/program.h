#ifndef program_h
#define program_h
#include <state.h>
#include <memory.h>
#include <random>

#define MAX_GRAPH_DEPTH 1000.0
#define MAX_OP 16

class program
{
   public:
      int _action; /* Action index. */
      double _bidVal; /* Most recent bid value. */
      static long _count; /* Next id to use. */
      long _dim; /* Expected dimension of input feature vector. */
      double *feature; /* Pointer for input state. */
      set < long > _features; /* Features indexed by non-introns in this program, determined in markIntrons(). */
      set < long > _featuresMem; /* Features indexed by non-introns that write to memory, determined in markIntrons(). */
      long _gtime; /* Time step at which generated. */
      long _id; 
      double _key;
      int _lastCompareFactor;
      double * _mem;
      bool * _memActive;
      double * _memReadTime;
      double * _memWriteTime;
      memory * _memory;
      int _nrefs; /* Number of references by teams. */
      vector < int > _opCounts; // count for each operator over _bidEffective
      vector < double > _profile; /* Bid profile. */
      bool _skipIntrons;
      bool _stateful;
      bool _targetMem; /* Set to true in markIntrons if this program writes to stateful memory. */

      inline int action() { return _action; }
      inline void action(int a) { _action = a; }
      virtual double run(state *s, int timeStep, int graphDepth) = 0;
      inline double bidVal(){ return _bidVal; }
      inline void bidVal(double b){ _bidVal = b; }
      virtual string checkpoint(bool) = 0;
      inline long dim() { return _dim; }
      inline void dim(long d) { _dim = d; }
      inline void features(set < long > &f) { f = _features; }
      inline void featuresMem(set < long > &f) { f = _featuresMem; }
      inline void getProfile(vector < double > &p) { p = _profile; }
      inline long gtime() { return _gtime; }
      inline long id() { return _id; }
      inline void id(long i) { _id = i; }
      inline double key(){ return _key; }
      inline void key(double key) { _key = key; }
      inline int lastCompareFactor() { return _lastCompareFactor; }
      inline void lastCompareFactor(int c) { _lastCompareFactor = c; }
      virtual ~program(){};
      inline void activeMemories(set < double > &memories){
         double m = (double)_memory->id();
         for (int i = 1; i <= MEMORY_REGISTERS; i++)
            if (_memActive[i-1] == true)
               memories.insert(m + (i * 0.1));
      }
      virtual void markIntrons(bool) = 0;
      inline void memGet(memory *&m) { m = _memory; }
      inline void memSet(memory *m) { 
         _memory = m; 
         _mem = _memory->getMem(); 
         _memActive = _memory->getActive();
         _memReadTime = _memory->getReadTime();
         _memWriteTime = _memory->getWriteTime();
      }
      inline memory * memGet() { return _memory; }
      /* Mutate action, return true if the action was actually changed. */
      inline bool muAction(long action) { long a = _action; _action = action; return a != action; }
      virtual bool muBid(double, double, double, double, size_t, mt19937 &, uniform_real_distribution<> &) = 0; /* Mutate bid, return true if any changes occured. */
      inline long numFeatures() { return _features.size(); } /* Not counting introns. */
      inline void opCounts(vector <int> &v){ v = _opCounts; }
      double *_REG;
      inline void setId(long id) { _id = id; }
      inline void setNrefs(int nrefs) { _nrefs = nrefs; }
      inline void setProfile(vector < double > &p) { _profile = p; }
      virtual int size() = 0;
      virtual int esize() = 0;
      inline bool skipIntrons() { return _skipIntrons; }
      inline void skipIntrons(bool b) { _skipIntrons = b; }
      inline bool stateful() { return _stateful; }
      inline void stateful(bool s) { _stateful = s; }
      inline bool targetMem() { return _targetMem; }
      inline int refs() { return _nrefs; }
      inline int refDec() { return --_nrefs; }
      inline int refInc() { return ++_nrefs; }
};

struct programIdComp
{
   bool operator() (program* l1, program* l2) const
   {
      return l1->id() < l2->id();
   }
};

struct programIdEQ
{
   bool operator() (program* l1, program* l2) const
   {
      return l1->id() == l2->id();
   }
};

struct ProgramBidLexicalCompare {
   bool operator() (program* l1, program* l2) const {
      //most recent bid, higher is better
      if (l1->bidVal() != l2->bidVal()){ 
         //l1->lastCompareFactor(0); 
         //l2->lastCompareFactor(0); 
         return l1->bidVal() > l2->bidVal();
      }
      ////program size post intron removal, smaller is better (assumes markIntrons is up to date)
      //else if (l1->esize() != l2->esize()) { 
      //   l1->lastCompareFactor(1); 
      //   l2->lastCompareFactor(1);  
      //   return l1->esize() < l2->esize();
      //}
      ////number of references, less is better
      //else if (l1->refs() != l2->refs()) { 
      //   l1->lastCompareFactor(2); 
      //   l2->lastCompareFactor(2); 
      //   return l1->refs() < l2->refs();
      //}
      ////number of features indexed, less is better
      //else if (l1->numFeatures() != l2->numFeatures()) { 
      //   l1->lastCompareFactor(3); 
      //   l2->lastCompareFactor(3); 
      //   return l1->numFeatures() < l2->numFeatures();
      //}
      ////age, younger is better
      //else if (l1->gtime() != l2->gtime()) { 
      //   l1->lastCompareFactor(4); 
      //   l2->lastCompareFactor(4); 
      //   return l1->gtime() > l2->gtime();
      //}
      //correlated to age but technically arbirary, id is guaranteed to be unique and thus ensures deterministic comparison
      else { 
         //l1->lastCompareFactor(6); 
         //l2->lastCompareFactor(6); 
         return l1->id() < l2->id();
      }
   }
};

#endif
