#ifndef state_h
#define state_h

#include <vector>
#include <set>
#include "misc.h"

class state
{
   public: 

      /********************************************************************************************
       * Methods that set or query the state.
       ********************************************************************************************/
      inline void getState(vector <float> &v){
         v = _featVecFloat;
      }
      inline void getState(vector <double> &v){
         v = _featVecDouble;
      }
      inline double getStateVarDouble(int v){
         return _featVecDouble[v];
      }
      inline float* getStatePointerFloat(){
         return &_featVecFloat[0];
      }
      inline double* getStatePointerDouble(){
         return &_featVecDouble[0];
      }
      inline void setState(const vector<float> &s){
         _featVecFloat = s;
      }
      inline void setState(const vector<double> &s){
         _featVecDouble = s;
      }
      inline void setState(const deque<double> &s){
         for (size_t i = 0; i < s.size(); i++)
            _featVecDouble[i] = s[i];
      }
      /********************************************************************************************
       * Construct / Destruct
       ********************************************************************************************/
      state(){ }
      state(int dim){
         _featVecFloat.reserve(dim); _featVecFloat.resize(dim);
         _featVecDouble.reserve(dim); _featVecDouble.resize(dim);
      }
      ~state(){
      }

   protected:
      /********************************************************************************************
       *  Member variables and data structures.
       ********************************************************************************************/
      vector <float> _featVecFloat;
      vector <double> _featVecDouble;
};
#endif
