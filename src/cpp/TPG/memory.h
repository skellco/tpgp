#ifndef memory_h
#define memory_h

#define MEMORY_REGISTERS 8

#include "misc.h"

class memory
{
   public: 

      /********************************************************************************************
       * Methods...
       ********************************************************************************************/
      inline bool * getActive(){ return _active; }
      inline double * getMem() { return _mem; }
      inline double * getReadTime() { return _readTime; }
      inline double * getWriteTime() { return _writeTime; }
      inline void clear() { memset(_mem, 0, MEMORY_REGISTERS * sizeof(double)); }
      inline void clearActive(){ memset(_active, false, MEMORY_REGISTERS * sizeof(bool)); }
      inline void setActive(){ memset(_active, true, MEMORY_REGISTERS * sizeof(bool)); }
      inline void clearReadTime(){ memset(_readTime, -1.0, MEMORY_REGISTERS * sizeof(double)); }
      inline void clearWriteTime(){ memset(_writeTime, -1.0, MEMORY_REGISTERS * sizeof(double)); }
      inline long id() { return _id; }
      inline void getActiveReadTime(vector<double> &v){
         for (int i = 0; i < MEMORY_REGISTERS; i++)
            if (_active[i])
               v[i] = _readTime[i];
      }
      inline void getActiveWriteTime(vector<double> &v){
         for (int i = 0; i < MEMORY_REGISTERS; i++)
            if (_active[i])
               v[i] = _writeTime[i];
      }

      inline int refs() { return _nrefs; }
      inline void refs(int i) { _nrefs = i; }
      inline int refDec() { return --_nrefs; }
      inline int refInc() { return ++_nrefs; }
      inline int refsPolicy() { return _nrefsPolicy; }
      inline void refsPolicy(int i) { _nrefsPolicy = i; }
      inline int refsPolicyInc() { return ++_nrefsPolicy; }

      string checkpoint(){
         ostringstream oss;

         oss << "memory:" << _id << ":" << _nrefs << endl; 

         return oss.str();
      }

      /********************************************************************************************
       * Constructors and destructor.
       ********************************************************************************************/
      memory(long i){
         _id = i;
         _nrefs = 0;
         _mem = new double[MEMORY_REGISTERS];
         clear();
         _active = new bool[MEMORY_REGISTERS];
         clearActive();
         _readTime = new double[MEMORY_REGISTERS];
         clearReadTime();
         _writeTime = new double[MEMORY_REGISTERS];
         clearWriteTime();
      }

      memory(long i, int nr){
         _id = i;
         _nrefs = nr;
         _mem = new double[MEMORY_REGISTERS];
         clear();
         _active = new bool[MEMORY_REGISTERS];
         clearActive();
         _readTime = new double[MEMORY_REGISTERS];
         clearReadTime();
         _writeTime = new double[MEMORY_REGISTERS];
         clearWriteTime();
      }

      memory(memory *m){
         _id = m->id();
         _nrefs = m->refs();
         _mem = new double[MEMORY_REGISTERS];
         clear();
         _active = new bool[MEMORY_REGISTERS];
         memcpy(_active, m->getActive(), MEMORY_REGISTERS * sizeof(bool));
         _readTime = new double[MEMORY_REGISTERS];
         memcpy(_readTime, m->getReadTime(), MEMORY_REGISTERS * sizeof(double));
         _writeTime = new double[MEMORY_REGISTERS];
         memcpy(_writeTime, m->getWriteTime(), MEMORY_REGISTERS * sizeof(double));
      }

      ~memory()
      {
         delete[] _mem;
         delete[] _active;
         delete[] _readTime;
         delete[] _writeTime;
      }

   protected:

      /********************************************************************************************
       *  Member variables and data structures.
       ********************************************************************************************/
      long _id;
      double * _mem;
      bool * _active;
      double * _readTime;
      double * _writeTime;
      int _nrefs; // Number of references
      int _nrefsPolicy; // Number of references within a particular policy graph
};

struct memoryIdComp
{
   bool operator() (memory* m1, memory* m2) const
   {
      return m1->id() < m2->id();
   }
};

#endif
