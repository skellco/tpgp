#ifndef linearAV_h
#define linearAV_h
#include <random>
#include <bitset>
#include "memory.h"
#include "learner.h"

class linearAV: public learner
{
   public:
      //typedef bitset < 16 + 3 + 3 + 3 > instruction; //8 ops
      typedef bitset < 16 + 3 + 4 + 4 > instruction; //16 ops

      static const int _registers;
      /* Mode bits */
      //static const int _dest_bit;
      static const int _src1_bit;
      static const int _feature_bit;
      static const int  _src2_bit;
      /* Used to define unique bidding behaviour. */
      static const int  _bid_epsilon;
      vector < instruction * > _bid; // Bid program
      vector < instruction * > _bidEffective;
      vector < instruction * > * _program;

      /* Instruction definitions. */

      /* Masks. Apply before shifting. */
      static const instruction _modeMask;
      static const instruction _opMask;
      static const instruction _dstMask;
      static const instruction _srcMask;

      /* Modes. */
      static const instruction _mode0;
      static const instruction _mode1;

      /* Operations. */
      static const instruction _opSum;
      static const instruction _opDiff;
      static const instruction _opProd;
      static const instruction _opDiv;
      static const instruction _opCos;
      static const instruction _opLog;
      static const instruction _opExp;
      static const instruction _opCondA;

      static const instruction _opSqrt;
      static const instruction _opSin;
      static const instruction _opTanh;
      static const instruction _opCondB;
      static const instruction _opSqr;
      static const instruction _opPow;
      static const instruction _opAbs;
      static const instruction _opCube;

      /* Shift amounts. */
      static const short _modeShift;
      static const short _opShift;
      static const short _dstShift;
      static const short _srcShift;

      void markIntrons();
      double run(state *, int &, int);
      string checkpoint(bool);
      inline void getBid(vector <instruction *> &b) { b = _bid;}
      linearAV(long, long, int, long, long, mt19937 &); /* Create arbitrary linearAV. */
      linearAV(long, linearAV &, long); /* Create linearAV from another linearAV. */
      linearAV(long, long, int, long,long, long, vector < instruction * >); /* Create linearAV from checkpoint file. */
      ~linearAV();
      bool muBid(double, double, double, double, size_t, mt19937 &, uniform_real_distribution<> &); /* Mutate bid, return true if any changes occured. */
      inline int numInstructionBits() { return _bid[0]->size(); }
      inline int size() { return _bid.size(); }
      inline int esize() { return _bidEffective.size(); }
};

#endif
