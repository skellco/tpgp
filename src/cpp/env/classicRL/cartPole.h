#ifndef cartpole_h
#define cartpole_h

#include <classicRLEnv.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#if !defined(CCANADA) && !defined(HPCC)
#include <GL/gl.h>
#include <GL/glut.h>
#endif

using namespace std;

class cartPole: public classicRLEnv 
{
   protected:
      const double GRAVITY = 9.8;
      const double MASSCART = 1.0;
      const double MASSPOLE = 0.1;
      const double TOTAL_MASS = (MASSPOLE + MASSCART);
      const double LENGTH = 0.5;                /* actually half the pole's length */
      const double POLEMASS_LENGTH = (MASSPOLE * LENGTH);
      const double FORCE_MAG = 10.0;
      const double TAU = 0.02;                  /* seconds between state updates */
      const double FOURTHIRDS = 1.3333333333333;
      const double SIX_DEGREES = 0.1047198;
      const double SEVEN_DEGREES = 0.1221730;
      const double TEN_DEGREES = 0.1745329;
      const double TWELVE_DEGREES = 0.2094384;
      const double FIFTEEN_DEGREES = 0.2617993;
      const double TWELVE_DEGREES_SQR = TWELVE_DEGREES * TWELVE_DEGREES;

      const double MIN_X = -1;
      const double MAX_X = 1;

      //state array indexing
      const int _X = 0;
      const int _THETA = 1;
      const int _X_DOT = 2;
      const int _THETA_DOT = 3;

      int lastActionD = 0;

   public:

      cartPole(){
         disReset = uniform_real_distribution<>(-0.05, 0.05);
         actionsDiscrete.push_back(-FORCE_MAG);
         actionsDiscrete.push_back(0.0);
         actionsDiscrete.push_back(FORCE_MAG);
         _id = 1;
         max_step = 300;
         //min_eval = 3;

         state[ID_INDEX_FO] = ID_CARTPOLE;
         state[ID_INDEX_PO] = ID_CARTPOLE;
      }

      ~cartPole() {
         state.clear();
         state_po.clear();
         actionsDiscrete.clear();
         actionTrace.clear();
      }

      void reset(mt19937 &rng){
         state_po[_X] = state[_X] = disReset(rng);

         state_po[_THETA] = state[_THETA] = disReset(rng);

         state[_X_DOT] = disReset(rng);

         state[_THETA_DOT] = disReset(rng);

         reward = 0;
         state[REWARD_INDEX_FO] = reward;
         state_po[REWARD_INDEX_PO] = reward;

         step = 0;
         terminalState = false;
         normalizeState(true);
      }

      double update(int actionD, double actionC, mt19937 &rng){

         double xacc, thetaacc, force, costheta, sintheta, temp;

         (void)actionC;
         (void)rng;

         //action 1 is ignored
         if (actionD == 0){
            lastActionD = 0;
            force = actionsDiscrete[lastActionD];
         }
         else if (actionD == 2){
            lastActionD = 2;
            force = actionsDiscrete[lastActionD];
         }
         else
            force = actionsDiscrete[lastActionD];

         costheta = cos(state[_THETA]);
         sintheta = sin(state[_THETA]);

         temp = (force + POLEMASS_LENGTH * state[_THETA_DOT] * state[_THETA_DOT] * sintheta)
            / TOTAL_MASS;

         thetaacc = (GRAVITY * sintheta - costheta * temp)
            / (LENGTH * (FOURTHIRDS - MASSPOLE * costheta * costheta
                     / TOTAL_MASS));

         xacc  = temp - POLEMASS_LENGTH * thetaacc * costheta / TOTAL_MASS;

         /*** Update the four state variables, using Euler's method. ***/

         state[_X]  += TAU * state[_X_DOT];
         state_po[_X] = state[_X];

         state[_X_DOT] += TAU * xacc;

         state[_THETA] += TAU * state[_THETA_DOT];
         state_po[_THETA] = state[_THETA];

         state[_THETA_DOT] += TAU * thetaacc;

         step++;//terminal() depends on this, so do it before checking!

         ////reward 0
         //reward = 1.0;

         //reward 1
         //reward = terminal() ? ((double)step/max_step) * 100: 0;
         //reward = terminal() ? step: 0;

         //reward 2
         //reward = terminal() ? ((double)step/max_step) * 100 : -(pow(state[_THETA], 2) / TWELVE_DEGREES_SQR);
         reward = 1.0;//-(pow(state[_THETA], 2) / TWELVE_DEGREES_SQR);

         state[REWARD_INDEX_FO] = reward;
         state_po[REWARD_INDEX_PO] = reward;

         normalizeState(true);
         return reward;
      }


      bool terminal(){
         if (step >= max_step || abs(state[_THETA]) > TWELVE_DEGREES || abs(state[_X]) > MAX_X)
            terminalState = true;
         return terminalState;
      }

      //opengl
      void display_function(int episode, int actionD, double actionC)
      {
         (void)actionC;
#if !defined(CCANADA) && !defined(HPCC)
         double r1 = 1.0;
         double x2, y2;

         glClear(GL_COLOR_BUFFER_BIT);

         glLineWidth(5.0);

         //cart
         glColor3f(0.0 ,0.0,1.0);
         glBegin(GL_TRIANGLES);
         glVertex2f(state[_X] - 0.15, 0.075);
         glVertex2f(state[_X] - 0.15, -0.075);
         glVertex2f(state[_X] + 0.15, 0.075);
         glVertex2f(state[_X] + 0.15, 0.075);
         glVertex2f(state[_X] - 0.15, -0.075);
         glVertex2f(state[_X] + 0.15, -0.075);
         glEnd();

         //pole
         x2 = state[_X] + r1 * cos (M_PI/2 - state[_THETA]);
         y2 = r1 * sin(M_PI/2 - state[_THETA]);
         glColor3f(1.0 ,1.0, 1.0);
         glBegin (GL_LINES);
         glVertex2d(state[_X], 0.0);
         glVertex2d(x2, y2);

         //x bounds surface
         glVertex2d(MIN_X,0.0);
         glVertex2d(MAX_X,0.0);

         glEnd();

         //discrete action arrows

         if (step > 0){
            //action 1 is ignored
            double force = 0;
            if (actionD == 0)
               force = actionsDiscrete[0];
            else if (actionD == 2)
               force = actionsDiscrete[2];

            int dir = 1;
            if (actionD == 0)
               dir = -1;
            else if (actionD == 2)
               dir = 1;
            glBegin(GL_POLYGON);
            glVertex3f(dir * 0.12, -0.2, 0);
            glVertex3f(dir * 0.25, -0.25, 0);
            glVertex3f(dir * 0.12, -0.3, 0);
            glEnd();
            glLineWidth(2.0);
            drawTrace(0, "Action:", force/FORCE_MAG, -1.0);
         }

         glLineWidth(1.0);
         drawEpisodeStepCounter(episode, step, -1.9, -1.9);

         glColor3f(1.0 ,1.0, 1.0);
         char c[80];
         if (step == 0)
            sprintf(c, "Cartpole Initial Conditions%s", ":");
         else if (terminal())
            sprintf(c, "Cartpole Terminal%s", ":");
         else
            sprintf(c, "Cartpole%s", ":");
         drawStrokeText(c, -1.9, -1.7, 0);

         glFlush();
#endif
      }
      void normalizeState(bool po){
         if (po){
            state_po[_X] /= MAX_X;
            state_po[_THETA] /= TWELVE_DEGREES;
         }
      }
};

#endif
