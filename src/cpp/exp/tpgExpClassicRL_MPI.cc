#include "tpg_arg_parse.h"
#include "tpg_eval_rl_mpi.h"
//
#define CHECKPOINT_MOD 10000
#define TEST_MOD 100
#define PRINT_MOD 1
#define NUM_FIT_MODE 1

int main(int argc, char** argv) {
   mpi::environment env(argc, argv);
   mpi::communicator world;
   TPG tpg(-1);
   tpg_arg_parse_001(tpg, argc, argv);
   mt19937 rngTPG(tpg.seed());
   mt19937 rngEnv(tpg.seed2());
   ostringstream os; //logging

   /* task sets ********************************************************************************/
   vector < classicRLEnv* > tasks;
   string taskString = to_string(tpg.activeTask());
   if (taskString.find_first_of("1") != std::string::npos)
      tasks.push_back(new cartPole());
   if (taskString.find_first_of("2") != std::string::npos)
      tasks.push_back(new acrobot());
   if (taskString.find_first_of("3") != std::string::npos)
      tasks.push_back(new cartCentering());
   if (taskString.find_first_of("4") != std::string::npos)
      tasks.push_back(new pendulum());
   if (taskString.find_first_of("5") != std::string::npos)
      tasks.push_back(new mountainCar());
   if (taskString.find_first_of("6") != std::string::npos)
      tasks.push_back(new mountainCarContinuous());

   tpg.numTask(tasks.size());
   tpg.activeTask(0);//could be any task
   tpg.numFitMode(1);
   //tpg.dim(tpg.partiallyObservable() ? CLASSIC_RL_DIM_PO  : CLASSIC_RL_DIM);
   tpg.numPointAuxDouble(tpg.numFitMode() + 3); //fitness +  mean visitedTeams, decisionInstructions, membersRunEntropy;
   tpg.numPointAuxInt(4); //task, fitMode, phase, envSeed
   tpg.numAtomicActions(CLASSIC_RL_NUM_ACTION);
   tpg.phase(_TRAIN_PHASE);
   tpg.setParams(world.rank() == 0);

   if (tpg.replay()){
      tpg.readCheckpoint(tpg.tPickup(), tpg.checkpointInPhase(), -1, false, ""); //setRoots
      tpg.activeTask(0);
      tpg.phase(_TEST_PHASE);
      if (tpg.animate())
         tpg.numStoredOutcomesPerHost(_TEST_PHASE, 1);
      evaluate_sub(tpg, world, tasks, rngEnv);
      tpg.printTeamInfo(tpg.tPickup(), tpg.checkpointInPhase(), false, tpg.hostToReplay());
      tpg.printOss();
   }

   else if (world.rank() == 0) { //Master Process
      string my_string = "MAIN";

      //time logging
      auto startGen = chrono::system_clock::now(); chrono::duration<double> endGen = chrono::system_clock::now()-startGen;
      auto startInit = chrono::system_clock::now(); chrono::duration<double> endInit = chrono::system_clock::now()-startInit;
      auto startGenTeams = chrono::system_clock::now(); chrono::duration<double> endGenTeams;
      auto startSelTeams = chrono::system_clock::now(); chrono::duration<double> endSelTeams;
      auto startEval = chrono::system_clock::now(); chrono::duration<double> endEval;
      auto startChkp = chrono::system_clock::now(); chrono::duration<double> endChkp;
      auto startReport = chrono::system_clock::now(); chrono::duration<double> endReport;

      //initialization
      if (tpg.checkpoint())
         tpg.readCheckpoint(tpg.tPickup(), tpg.checkpointInPhase(), -1, false, ""); //setRoots
      else{
         startInit = chrono::system_clock::now();
         tpg.initTeams(rngTPG); //setRoots
         endInit = chrono::system_clock::now()-startInit;
      }

      team *eliteTeamMT = tpg.getMrootBegin();
      team *prevEliteTeamMT = tpg.getMrootBegin();

      /* Main training loop. */
      tpg.tCurrent(tpg.tStart());
      while (tpg.tCurrent() <= tpg.tMain()){

         /* replacement *********************************************************************/
         startGenTeams = chrono::system_clock::now();
         if (tpg.numRoot() < (tpg.numEliteTeams() + tpg.rSize())) tpg.genTeams(tpg.tCurrent(), rngTPG);
         endGenTeams = chrono::system_clock::now()-startGenTeams;

         /* evaluation **********************************************************************/
         startEval = chrono::system_clock::now();
         //evaluate on all tasks
         evaluate_main(tpg, os, world, tasks);
         endEval = chrono::system_clock::now()-startEval;

         /* selection ***********************************************************************/
         startSelTeams = chrono::system_clock::now();
         tpg.selTeams(tpg.tCurrent(), true, tpg.tCurrent() > 1 ? floor(chrono::duration_cast<chrono::milliseconds>(endGen).count()) : 0, 
               tpg.tCurrent() > TEST_MOD && tpg.tCurrent() > tpg.tStart() ? true : false);//also does some reporting
         prevEliteTeamMT = eliteTeamMT;
         eliteTeamMT = tpg.getEliteTeamMT(tpg.phase());
         endSelTeams = chrono::system_clock::now()-startSelTeams;

         /* accounting and reporting ********************************************************/
         startReport = chrono::system_clock::now();
         if (tpg.tCurrent() % TEST_MOD == 0){
            tpg.phase(_TEST_PHASE);
            evaluate_main(tpg, os, world, tasks);
            tpg.setEliteTeams(tpg.tCurrent(), _TEST_PHASE, true);
            tpg.writeCheckpoint(tpg.tCurrent(), true);//checkpoint single best program graph
            tpg.phase(_TRAIN_PHASE);
         }
         //if (tpg.tCurrent() == tpg.tStart() || tpg.tCurrent() % tpg.rSize() == 0)
         //   tpg.updateMODESFilters(false);
         endReport = chrono::system_clock::now()-startReport;

         /* checkpoint **********************************************************************/
         startChkp = chrono::system_clock::now();
         if (tpg.writeCheckpoints() && tpg.tCurrent() % CHECKPOINT_MOD == 0){
            tpg.writeCheckpoint(tpg.tCurrent(), false);//checkpoint everything
            tpg.printPhyloGraphDot();
         }
         else if (tpg.writeCheckpoints() && eliteTeamMT->id() != prevEliteTeamMT->id())
            tpg.writeCheckpoint(tpg.tCurrent(), true);//chekpoint single best program graph
         endChkp = chrono::system_clock::now()-startChkp;

         endGen = chrono::system_clock::now()-startGen;

         /* print generation timing *********************************************************/
         os << setprecision(5) << fixed << "gTime t " << tpg.tCurrent() << " sec " << endGen.count() << " evl " << endEval.count();
         os << " gTms " << endGenTeams.count() << " iTms " << endInit.count() << " sTms " << endSelTeams.count();
         os << " chkp " << endChkp.count() << " rprt " << endReport.count() << " lost " << endGen.count() -
            (endEval.count() + endGenTeams.count() + endInit.count() + endSelTeams.count() + endChkp.count() + endReport.count()) << endl;
         tpg.printOss(os);

         startGen = chrono::system_clock::now();
         if (tpg.tCurrent() % PRINT_MOD == 0) 
            tpg.printOss();
         tpg.tCurrent(tpg.tCurrent() + 1);
      }
      for (int ev = 1; ev <= world.size() - 1; ev++){
         string d = "done";
         world.send(ev, 0, d);
      }
      tpg.printOss();
      cout << "Goodbye cruel world. " << world.rank() << endl;
   }
   else  //Evaluator Process
      evaluate_sub(tpg, world, tasks, rngEnv);

   tpg.finalize();
   for (size_t tsk = 0; tsk < tasks.size(); tsk++)
      delete tasks[tsk];
   tasks.clear();
   return 0;
}

