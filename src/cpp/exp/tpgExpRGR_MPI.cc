#include <TPG.h>
#include "tpg_arg_parse.h"
#include <boost/mpi.hpp>
#include <chrono>
#include <boost/algorithm/string.hpp>

#define CHKP_MOD 10000
#define PRINT_MOD 1
#define TEST_MOD 10
#define NUM_POINT_AUX_DOUBLES 3 //mean squared error, mean visitedTeams, mean decisionInstructions
#define NUM_FIT_MODE 1

#define TRAIN_SAMPLES 1000
#define VALIDATION_SAMPLES 10000
#define TEST_SAMPLES 40000 
namespace mpi = boost::mpi;

//rgrDataPoint stores dim * inputs and the response (output) of some function
//pre-caluclating and saving data points prevents us from having to recalculate the functions during evolution
class rgrDataPoint
{
   double _response;
   vector <double> _point;
   public:
   rgrDataPoint(int dim, mt19937 &g, uniform_real_distribution< double > &urd)
   {
      for (int i = 0; i < dim; i++)
         _point.push_back(urd(g));
   }
   double response() { return _response; }
   void response(double d) { _response = d; }
   vector <double> * point() { return &_point; }
};

//functions used in "Evolving Graphs with Horizontal Gene Transfer", Atkinson, Plump, and Stepney
double rgrEval(vector <double> &point, int f){
   double sum;
   switch(f){
      case 1:
         return exp(-pow(point[0] - 1, 2)) / (1.2 + pow(point[1] - 2.5, 2));
         break;
      case 2:
         return exp(-point[0]) * pow(point[0], 3) * cos(point[0]) * sin(point[0]) * 
            (cos(point[0]) * pow(sin(point[0]), 2) - 1) * (point[1] - 5);
         break;
      case 3:
         sum = 0;
         for (int i = 0; i < 5; i++)
            sum += pow(point[i] - 3, 2); 
         return 10 / (5 + sum);
         break;
      case 4:
         return 30 * (((point[0] - 1) * (point[2] - 1)) / (pow(point[1], 2) * (point[0] - 10)));
         break;
      case 5:
         return 6 * sin(point[0]) * cos(point[1]);
         break;
      case 6:
         return (point[0] - 3) * (point[1] - 3) + 2 * sin((point[0] - 4) * (point[1] - 4));
         break;
      case 7:
         return (pow(point[0] - 3,4) + pow(point[1] - 3, 3) - (point[1] - 3)) / (pow(point[1] - 2, 4) + 10);
         break;
      case 8:
         return (1 / (1 + pow(point[0], -4))) + (1 / (1 + pow(point[1], -4)));
         break;
      case 9:
         return pow(point[0], 4) - pow(point[0], 3) + pow(point[1], 2) / 2 - point[1];
         break;
      case 10:
         return 8 / (2 + pow(point[0], 2) + pow(point[1], 2));
         break;
      case 11:
         return pow(point[0], 3) / 5 + pow(point[1], 3) / 2 - point[1] - point[0];
         break;
      case 12:
         return point[0]*point[1] + point[2]*point[3] + point[4]*point[5] + 
            point[0]*point[6]*point[8] + point[2]*point[5]*point[9];
         break;
      case 18:
         return  point[0] * point[1] * point[2] * point[3] * point[4];
         break;
      case 21:
         return 2 - 2.1 * cos(9.8 * point[0]) * sin(1.3 * point[4]);
         break;
      default:
         return numeric_limits<double>::lowest();
         break;
   }
}

int main(int argc, char** argv) {
   mpi::environment env(argc, argv);
   mpi::communicator world;
   TPG tpg(-1);
   tpg_arg_parse_002(tpg, argc, argv);
   ostringstream os; //logging

   tpg.numPointAuxDouble(NUM_POINT_AUX_DOUBLES);
   tpg.numFitMode(NUM_FIT_MODE);
   tpg.setParams(world.rank() == 0);
   tpg.numAtomicActions(2); 

   if (world.rank() == 0) { //Master Process
      mt19937 rng(tpg.seed());
      string my_string = "MAIN";

      //time logging
      auto startGen = chrono::system_clock::now(); chrono::duration<double> endGen = chrono::system_clock::now()-startGen;;
      auto startInit = chrono::system_clock::now(); chrono::duration<double> endInit;
      auto startGenTeams = chrono::system_clock::now(); chrono::duration<double> endGenTeams;
      auto startSelTeams = chrono::system_clock::now(); chrono::duration<double> endSelTeams;
      auto startEval = chrono::system_clock::now(); chrono::duration<double> endEval;
      auto startChkp = chrono::system_clock::now(); chrono::duration<double> endChkp;
      auto startReport = chrono::system_clock::now(); chrono::duration<double> endReport;

      map <long, team*> rootTeams; vector < team* > teamsToEval; vector < team* > teamsThisEval;
      vector < string > all_strings; vector <string> splitString; string resultLine; string s;
      vector <long> active; vector <behaviourType> tmpBehavSeq; vector <double> r_rewards;
      double currentTestScore = numeric_limits<double>::lowest();
      double maxTestScore = numeric_limits<double>::lowest();
      long evaluations = 0;
      //initialization
      if (tpg.checkpoint())
         tpg.readCheckpoint(tpg.tPickup(), tpg.checkpointInPhase(), -1, false, ""); //setRoots
      else{
         startInit = chrono::system_clock::now();
         tpg.initTeams(rng); //setRoots
         endInit = chrono::system_clock::now()-startInit;
      }

      /* Main training loop. */
      tpg.phase(_TRAIN_PHASE);
      long t = tpg.tStart();
      while (t <= tpg.tMain()){
         if (t % TEST_MOD == 0 && tpg.phase() == _TRAIN_PHASE)
            tpg.phase(_VALIDATION_PHASE);
         else if (t % TEST_MOD == 0 && tpg.phase() == _VALIDATION_PHASE)
            tpg.phase(_TEST_PHASE);
         else if (t % TEST_MOD == 0 && tpg.phase() == _TEST_PHASE)
            tpg.phase(_TRAIN_PHASE);

         startGen = chrono::system_clock::now();
         /* replacement *********************************************************************/
         startGenTeams = chrono::system_clock::now();
         if (tpg.phase() == _TRAIN_PHASE && tpg.numRoot() < tpg.rSize()) tpg.genTeams(t, rng);
         endGenTeams = chrono::system_clock::now()-startGenTeams;

         /* evaluation **********************************************************************/
         startEval = chrono::system_clock::now();
         //assign policies to evaluators
         tpg.getTeams(rootTeams,true);
         int evaluator = 1; teamsToEval.clear(); teamsThisEval.clear();
         if (tpg.phase() == _TEST_PHASE){
            tpg.setSingleTaskFitness(_VALIDATION_PHASE); //assign each team's fitness based on mean validation score
            teamsToEval.push_back(tpg.getBestTeam()); //only test the validation champion
         }
         else{
            for (auto it = rootTeams.begin(); it != rootTeams.end(); it++)
               if (it->second->numOutcomes(tpg.phase(), tpg.hostFitnessMode()) == 0)
                  teamsToEval.push_back(it->second);
         }

         if (tpg.phase() == _TRAIN_PHASE)
            evaluations += teamsToEval.size();

         size_t teamsPerEvaluator = teamsToEval.size()/(world.size() - 1);
         size_t remainder = teamsToEval.size() % (world.size() - 1);

         for (auto it = teamsToEval.begin(); it != teamsToEval.end(); it++){
            teamsThisEval.push_back(*it);
            if ((remainder > 0 && teamsThisEval.size() == teamsPerEvaluator+1) ||
                  (remainder == 0 && teamsThisEval.size() == teamsPerEvaluator) || 
                  next(it) == teamsToEval.end()){
               s = "";
               tpg.writeCheckpoint(s, teamsThisEval);
               world.send(evaluator++, 0, s);
               teamsThisEval.clear();
               if (remainder > 0) remainder--;
            }
         }

         //no more teams to evaluate, so let the rest of the procs know they are not needed this round
         while (evaluator <= (world.size() - 1)){
            s = "x"; 
            world.send(evaluator++, 0, s);
         }

         //collect evaluation result from each evaluator
         all_strings.clear();
         gather(world, my_string, all_strings, 0);
         for (int proc = 1; proc < world.size(); proc++){
            if (!all_strings[proc].empty()){
               istringstream f(all_strings[proc]);
               while(getline(f, resultLine)){
                  split(resultLine, ':', splitString);
                  //parse the string
                  size_t s = 0;
                  long rslt_id = atol(splitString[s++].c_str());
                  for (size_t i = 0; i < NUM_POINT_AUX_DOUBLES; i++)
                     r_rewards.push_back(atof(splitString[s++].c_str()));
                  if (tpg.phase() == _TEST_PHASE)
                     currentTestScore = r_rewards[0];
                  while (s < splitString.size())
                     active.push_back(atol(splitString[s++].c_str()));
                  rootTeams[rslt_id]->updateActiveMembersFromIds(active);
                  tpg.setOutcome(rootTeams[rslt_id], tmpBehavSeq, r_rewards, tpg.phase(), t);
                  active.clear();
                  r_rewards.clear();
               }
            }
         }
         endEval = chrono::system_clock::now()-startEval;

         /* selection **********************************************************************/
         startSelTeams = chrono::system_clock::now();
         if (tpg.phase() == _TRAIN_PHASE) 
            tpg.selTeams(t, true, t > 1 ? floor(chrono::duration_cast<chrono::milliseconds>(endGen).count()) : 0);//also does some reporting
         endSelTeams = chrono::system_clock::now()-startSelTeams; 

         /* accounting and reporting **********************************************************************/
         startReport = chrono::system_clock::now();
         tpg.printTeamInfo(t, tpg.phase(), true);
         if ((t == tpg.tStart() || t % tpg.rSize() == 0) && tpg.phase() == _TRAIN_PHASE)
            tpg.updateMODESFilters(t, t == tpg.tStart(), false);
         endReport = chrono::system_clock::now()-startReport;

         /* checkpoint **********************************************************************/
         startChkp = chrono::system_clock::now();
         //only write a checkpoint when we find a new test champion
         if (tpg.phase() == _TEST_PHASE && currentTestScore > maxTestScore){
            tpg.writeCheckpoint(t, _TRAIN_PHASE, false, -1);
            maxTestScore = currentTestScore;
         }
         endChkp = chrono::system_clock::now()-startChkp;

         endGen = chrono::system_clock::now()-startGen;

         /* print gen time ************************************************************************/

         os << setprecision(5) << fixed << "gTime t " << t << " sec " << endGen.count() << " evl " << endEval.count();
         os << " gTms " << endGenTeams.count() << " iTms " << endInit.count() << " sTms " << endSelTeams.count();
         os << " chkp " << endChkp.count() << " rprt " << endReport.count() << " lost " << endGen.count() -
            (endEval.count() + endGenTeams.count() + endInit.count() + endSelTeams.count() + endChkp.count() + endReport.count()) << endl;
         tpg.printOss(os);

         startGen = chrono::system_clock::now();
         
         if (t % PRINT_MOD == 0)
            tpg.printOss();

         if (tpg.phase() == _TRAIN_PHASE) t++;
      }
      for (int ev = 1; ev <= world.size() - 1; ev++){
         s = "done";
         world.send(ev, 0, s);
      }
      tpg.finalize();
      tpg.printOss();
      cout << "Goodbye cruel world." << endl;
   }
   else { //Evaluator Process
      state *worldState = new state(tpg.dim());
      vector<double> rewards; rewards.reserve(NUM_POINT_AUX_DOUBLES); rewards.resize(NUM_POINT_AUX_DOUBLES);
      program * atomicLearner;
      set <team*, teamIdComp> visitedTeams;
      long decisionInstructions;
      int step = 1;
      vector <team*> teams;

      //data
      vector< rgrDataPoint* > dataTrain;
      vector< rgrDataPoint* > dataValid;
      vector< rgrDataPoint* > dataTest;
      vector< rgrDataPoint* > *dataEval;

      mt19937 gen(777);
      uniform_real_distribution< double > dis(-5.0, nextafter(5.0, std::numeric_limits<double>::max()));

      rgrDataPoint * pt;
      for (int i = 0; i < TRAIN_SAMPLES; i++){
         pt = new rgrDataPoint(tpg.dim(), gen, dis);
         pt->response(rgrEval(*(pt->point()), tpg.function()));
         dataTrain.push_back(pt);
      }
      for (int i = 0; i < VALIDATION_SAMPLES; i++){
         pt = new rgrDataPoint(tpg.dim(), gen, dis);
         pt->response(rgrEval(*(pt->point()), tpg.function()));
         dataValid.push_back(pt);
      }
      for (int i = 0; i < TEST_SAMPLES; i++){
         pt = new rgrDataPoint(tpg.dim(), gen, dis);
         pt->response(rgrEval(*(pt->point()), tpg.function()));
         dataTest.push_back(pt);
      }

      string evalResult = "";
      string checkpointString = "";
      while (checkpointString.compare("done") != 0){
         if (tpg.phase() == _VALIDATION_PHASE) 
            dataEval = &dataValid;
         else if (tpg.phase() == _TEST_PHASE)
            dataEval = &dataTest;
         else 
            dataEval = &dataTrain;

         //receive from p0
         world.recv(0, 0, checkpointString);
         evalResult = "";
         set <program*, programIdComp> active;
         if (checkpointString.compare("x") != 0 && checkpointString.compare("done") != 0){
            tpg.readCheckpoint(-1, _TRAIN_PHASE, -1, true, checkpointString);
            tpg.getTeams(teams,true);
            for (size_t i = 0; i < teams.size(); i++){
               tpg.markEffectiveCode(teams[i], true);
               fill(rewards.begin(), rewards.end(), 0);
               //evaluate 
               for (auto sample = dataEval->begin(); sample != dataEval->end(); sample++){
                  tpg.clearMemory();
                  worldState->setState(*((*sample)->point()));
                  atomicLearner = tpg.getAction(teams[i], worldState, true, visitedTeams, decisionInstructions, step);
                  rewards[tpg.hostFitnessMode()] += pow((*sample)->response() - (atomicLearner->memGet())->getMem()[0], 2); //sum squared error
                  rewards[tpg.numFitMode()] += visitedTeams.size();
                  rewards[tpg.numFitMode() + 1] += decisionInstructions;
               }
               if (isfinite(rewards[tpg.hostFitnessMode()]))
                  rewards[tpg.hostFitnessMode()] = -1 * (rewards[tpg.hostFitnessMode()] / dataEval->size());
               else{
                  rewards[tpg.hostFitnessMode()] = numeric_limits<double>::lowest();
                  cerr << "TPG::eval infinite SSE phase " << tpg.phase() << " team " << teams[i]->id()  << endl;
               }
               rewards[tpg.numFitMode()] = rewards[tpg.numFitMode()] / dataEval->size(); //mean visited teams
               rewards[tpg.numFitMode() + 1] = rewards[tpg.numFitMode() + 1] / dataEval->size(); //mean decision instructions

               if (tpg.phase() == _TEST_PHASE) rewards[tpg.hostFitnessMode()] = abs(rewards[tpg.hostFitnessMode()]);

               evalResult += to_string(teams[i]->id());
               for (size_t r = 0; r < rewards.size(); r++)
                  evalResult += ":" + to_string(rewards[r]);
               teams[i]->getActiveMembersByRef(active);
               for (auto leiter = active.begin(); leiter != active.end(); leiter++)
                  evalResult += ":" + to_string((*leiter)->id());
               evalResult += "\n";
            }
         }
         gather(world, evalResult, 0);
      }
      delete worldState;
   }
   return 0;
}
