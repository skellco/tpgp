#include <TPG.h>
#include <classicRLEnv.h>
#include <cartPole.h>
#include <acrobot.h>
#include <cartCentering.h>
#include <pendulum.h>
#include <mountainCar.h>
#include <mountainCarContinuous.h>
#include <boost/mpi.hpp>
#include <chrono>
#include <thread>

#if !defined(CCANADA) && !defined(HPCC)
#include <GL/gl.h>
#include <GL/glut.h>
#endif

namespace mpi = boost::mpi;

void evaluate_main(TPG &tpg, ostringstream &os, mpi::communicator &world, vector < classicRLEnv * > &tasks){
   string my_string = "MAIN";
   map <long, team*> rootTeams; vector < team* > teamsToEval; vector < team* > teamsThisEval;
   vector < string > all_strings; vector <string> splitString; string resultLine; string s;
   vector <long> active; vector <behaviourType> tmpBehavSeq; vector <double> r_runTimeStats; vector <int> r_runTimeInts;

   int worldSizePerTask = (world.size() - 1)/tpg.numTask();
   int evaluator = 1;
   size_t numTeamsToEval = 0;
   for (size_t task = 0; task < tpg.numTask(); task++){
      tpg.activeTask(task);
      //assign policies to evaluators
      tpg.getTeams(rootTeams,true);
      teamsToEval.clear(); teamsThisEval.clear();
      for (auto it = rootTeams.begin(); it != rootTeams.end(); it++)
         if (it->second->numOutcomes(tpg.phase(),tpg.activeTask()) < tpg.numStoredOutcomesPerHost(tpg.phase())){
            if (tpg.phase() == _TRAIN_PHASE)
               it->second->numEval(min(tasks[tpg.activeTask()]->minEval(), (size_t)(tpg.numStoredOutcomesPerHost(tpg.phase()) - it->second->numOutcomes(tpg.phase(), tpg.activeTask()))));
            else
               it->second->numEval(tpg.numStoredOutcomesPerHost(tpg.phase()) - it->second->numOutcomes(tpg.phase(), tpg.activeTask()));
            teamsToEval.push_back(it->second);
            numTeamsToEval++;
         }
      size_t teamsPerEvaluator = teamsToEval.size()/worldSizePerTask;
      size_t remainder = teamsToEval.size() % worldSizePerTask;

      for (auto it = teamsToEval.begin(); it != teamsToEval.end(); it++){
         teamsThisEval.push_back(*it);
         if ((remainder > 0 && teamsThisEval.size() == teamsPerEvaluator+1) ||
               (remainder == 0 && teamsThisEval.size() == teamsPerEvaluator) || 
               next(it) == teamsToEval.end()){
            s = "";
            tpg.writeCheckpoint(s, teamsThisEval);
            world.send(evaluator++, 0, s);
            teamsThisEval.clear();
            if (remainder > 0) remainder--;
         }
      }
   }
   os << "t " << tpg.tCurrent() << " phs " << tpg.phase() << " tToEvl " << numTeamsToEval << endl;
   //no more teams to evaluate, so let the rest of the procs know they are not needed this round
   while (evaluator <= (world.size() - 1)){
      s = "x";
      world.send(evaluator++, 0, "x");
   }

   //collect evaluation result from each evaluator
   all_strings.clear();
   gather(world, my_string, all_strings, 0);
   for (int proc = 1; proc < world.size(); proc++){
      if (!all_strings[proc].empty()){
         istringstream f(all_strings[proc]);
         while(getline(f, resultLine)){
            split(resultLine, ':', splitString);
            //parse the string
            size_t s = 0;
            long rslt_id = atol(splitString[s++].c_str());
            tpg.activeTask(atoi(splitString[s++].c_str()));
            for (size_t i = 0; i < tpg.numPointAuxDouble(); i++)
               r_runTimeStats.push_back(atof(splitString[s++].c_str()));
            for (size_t i = 0; i < tpg.numPointAuxInt(); i++)
               r_runTimeInts.push_back(atoi(splitString[s++].c_str()));
            while (s < splitString.size())
               active.push_back(atol(splitString[s++].c_str()));
            rootTeams[rslt_id]->updateActiveMembersFromIds(active);
            tpg.setOutcome(rootTeams[rslt_id], tmpBehavSeq, r_runTimeStats, r_runTimeInts, tpg.tCurrent());
            //log this as a test outcome too since in classicRL there is no diff
            if (tpg.phase() == _TRAIN_PHASE && rootTeams[rslt_id]->numOutcomes(_TEST_PHASE, tpg.activeTask()) < tpg.numStoredOutcomesPerHost(_TEST_PHASE)){
               r_runTimeInts[POINT_AUX_INT_PHASE] = _TEST_PHASE;
               tpg.setOutcome(rootTeams[rslt_id], tmpBehavSeq, r_runTimeStats, r_runTimeInts, tpg.tCurrent());
            }
            active.clear();
            r_runTimeStats.clear();
            r_runTimeInts.clear();
         }
      }
   }
}

void evaluate_sub(TPG &tpg, mpi::communicator &world, vector < classicRLEnv * > &tasks, mt19937 &rng){
   classicRLEnv *game = tasks[0]; //gets updated prior to eval below
   size_t saveFrame = 0;
#if !defined(CCANADA) && !defined(HPCC)
   /* opengl setup for visual mode ********************************************************************************/
   if (tpg.visual()) {
      double _width = 1200;
      double _height = 1200;
      int argc = 1;
      char *argv[1] = {(char*)"null"};
      glutInit( &argc , argv);
      //glutInitDisplayMode(GLUT_SINGLE |GLUT_RGB);
      glutInitWindowSize(_width, _height);
      glutInitWindowPosition(100,100);
      glutCreateWindow("classicRL");
      //glutHideWindow();
      glScalef(0.5, 0.5, 0.0);
   }
   /**********************************************************************************************************/
#endif

   state *worldState = new state(CLASSIC_RL_DIM);
   vector<double> runTimeStats; runTimeStats.reserve(tpg.numPointAuxDouble()); runTimeStats.resize(tpg.numPointAuxDouble());
   vector<int> runTimeInts; runTimeInts.reserve(tpg.numPointAuxInt()); runTimeInts.resize(tpg.numPointAuxInt());
   //for graph animations
   vector <program*> allPrograms;
   vector <program*> winningPrograms;
   vector <program*> winningProgramsAll;
   vector <team*> visitedTeamsAll;
   vector < set <long> > decisionFeatures;
   vector < set <memory*, memoryIdComp> > decisionMemories;
   vector <team*> teamPath;

   program * atomicProgram = tpg.getLBegin();
   long decisionInstructions;
   set <team*, teamIdComp> visitedTeams;
   vector <team*> teams;
   string evalResult = "";
   string checkpointString = "";
   while (checkpointString.compare("done") != 0){
      //receive from p0
      if (!tpg.replay()) world.recv(0, 0, checkpointString);

      evalResult = "";
      set <program*, programIdComp> active;
      if (tpg.replay() || (checkpointString.compare("x") != 0 && checkpointString.compare("done") != 0)){
         if (!tpg.replay())
            tpg.readCheckpoint(-1, _TRAIN_PHASE, -1, true, checkpointString); //setRoots
         tpg.getTeams(teams,tpg.replay() ? false : true);
         game = tasks[tpg.activeTask()];
         for (size_t i = 0; i < teams.size(); i++){
            if (tpg.replay() && teams[i]->id() != tpg.hostToReplay()) continue;
            if (tpg.skipIntrons()) tpg.markEffectiveCode(teams[i], true);
            if (tpg.replay()) teams[i]->prunePrograms();
            teams[i]->clearMembersRunTally();

            if (tpg.replay()) 
               teams[i]->numEval(tpg.numStoredOutcomesPerHost(_TEST_PHASE));
            int eStart = 0;
            if (tpg.phase() == _TRAIN_PHASE)
               eStart = (tpg.tCurrent() - teams[i]->gtime()) * tasks[tpg.activeTask()]->minEval();
            else
               eStart = (tpg.tCurrent() - teams[i]->gtime() + 1) * tasks[tpg.activeTask()]->minEval();//plus one since test happens after eval in same gen
            for (size_t e = 0; e < teams[i]->numEval(); e++){
               if (tpg.seed2() < 1) rng.seed(e + eStart);
               fill(runTimeStats.begin(), runTimeStats.end(), 0);
               tpg.clearMemory();
               game->reset(rng);
               while (!game->terminal()){
                  worldState->setState(game->getStateVec(tpg.partiallyObservable()));
                  if (tpg.replay()){
                     atomicProgram = tpg.getAction(teams[i], worldState, true, visitedTeams, decisionInstructions, game->getStep(), allPrograms, winningPrograms, decisionFeatures, decisionMemories, teamPath);
                     winningProgramsAll.insert(winningProgramsAll.begin(), winningPrograms.begin(), winningPrograms.end());
                     visitedTeamsAll.insert(visitedTeamsAll.begin(), visitedTeams.begin(), visitedTeams.end());
                  }
                  else
                     atomicProgram = tpg.getAction(teams[i], worldState, true, visitedTeams, decisionInstructions, game->getStep(), teamPath);
                  if (tpg.animate()) {
                     size_t repeats = game->getStep() == 0 ? 40 : 1;
                     for (size_t r = 1; r <= repeats; r++){
                        for (size_t d = teamPath.size(); d <= teamPath.size(); d++){
                           tpg.printGraphDot(teams[i], saveFrame, e, repeats > 1 && r < repeats ? 0 : game->getStep(), d, allPrograms, winningPrograms, decisionFeatures, decisionMemories, teamPath, r==repeats ? true: false);
#if !defined(CCANADA) && !defined(HPCC)
                           if (tpg.visual())
                              game->display_function(e, game->getStep() == 0 && r < repeats ? 0 : (atomicProgram->action()*-1)-1, game->getStep() == 0 && r < repeats ? 0 : (atomicProgram->memGet())->getMem()[0]);
#endif
                           char filename[80];
                           sprintf(filename, "%s_%05d_%03d_%05d_%05d_%05d.tga","replay/frames/gl", (int)saveFrame, (int)e, game->getStep(), (int)d, 0);
#if !defined(CCANADA) && !defined(HPCC)
                           game->saveScreenshotToFile(filename, 1200, 1200);
#endif
                           saveFrame++;
                        }
                        this_thread::sleep_for(std::chrono::milliseconds(10));
                     }
                  }
                  runTimeStats[tpg.hostFitnessMode()] += game->update((atomicProgram->action()*-1)-1, (atomicProgram->memGet())->getMem()[0], rng);//atomic actions are represented as negatives in tpg
                  //if (game->id() == 1 && tpg.phase() == _TRAIN_PHASE){
                  //   worldState->setState(game->getStateVec(tpg.partiallyObservable()));
                  //   runTimeStats[tpg.hostFitnessMode()] -= pow(worldState->getStateVarDouble(1), 2); //reward shaping for cartPole
                  //}
                  runTimeStats[tpg.numFitMode()] += visitedTeams.size();
                  runTimeStats[tpg.numFitMode() + 1] += decisionInstructions;

                  //step-wise replay stats for plotting///////////////////////////////////////////////////////////////////////////////
                  if (tpg.replay()){
                     cout << "TPG::replay id " << teams[i]->id();
                     cout << " visitedTeams " << visitedTeams.size();
                     //cout << " activeNodes";
                     //for (auto it = visitedTeams.begin(); it != visitedTeams.end(); it++)
                     //   cout << " h" << (*it)->id();
                     //for (auto it = winningLearners.begin(); it != winningLearners.end(); it++)
                     //   cout << " s" << (*it)->id();
                     cout << " decisionInstructions " << decisionInstructions << " decisionFeatures";
                     for (auto it = decisionFeatures.begin(); it != decisionFeatures.end(); it++)
                        cout << " " << (*it).size();
                     cout << " decisionMemories";
                     //get all memories used this decision
                     set < memory* > mems;
                     for (auto it = decisionMemories.begin(); it != decisionMemories.end(); it++)
                        mems.insert((*it).begin(), (*it).end());

                     //get uniq read/write times as int (ignoring graph depth)
                     set <int> memActiveReadT;
                     set <int> memActiveWriteT;
                     vector <double> memReadTimes; 
                     vector <double> memWriteTimes;
                     for (int mr = 0; mr < MEMORY_REGISTERS; mr++){
                        memReadTimes.push_back(-1); 
                        memWriteTimes.push_back(-1); 
                     }
                     for (auto it = mems.begin(); it != mems.end(); it++){
                        (*it)->getActiveReadTime(memReadTimes);
                        (*it)->getActiveWriteTime(memWriteTimes);

                        for (auto rwit = memReadTimes.begin(); rwit != memReadTimes.end(); rwit++)
                           if (((int)(*rwit)) >= 0)//ignore untouched registers (-1)
                              memActiveReadT.insert((int)(*rwit)); 
                        for (auto rwit = memWriteTimes.begin(); rwit != memWriteTimes.end(); rwit++)
                           if (((int)(*rwit)) >= 0)//ignore untouched registers (-1)
                              memActiveWriteT.insert((int)(*rwit));
                     }
                     cout << " step " << game->getStep();
                     cout << " Dsize " << memActiveReadT.size();
                     vector<int> v;
                     for (auto it = memActiveReadT.begin(); it != memActiveReadT.end(); it++)
                        v.push_back(game->getStep() - (*it));

                     cout << " D " << vecToStr(v);

                     cout << " Dmax_med_min ";
                     cout << *(max_element(v.begin(), v.end()));
                     cout << " " << vecMedian(v);
                     cout << " " << *(min_element(v.begin(), v.end()));
                     cout << " v " << vecToStr(v);
                     //cout << " readTimes";
                     //for (auto it = memActiveReadT.begin(); it != memActiveReadT.end(); it++)
                     //   cout << " " << step - (*it);
                     //cout << " writeTimes";
                     //for (auto it = memActiveWriteT.begin(); it != memActiveWriteT.end(); it++)
                     //   cout << " " << step - (*it);
                     //cout << " teamPath"; 
                     //for(size_t i = 0; i < teamPath.size(); i++)
                     //   cout << " h" << teamPath[i]->id();
                     cout << endl;

                     //all memory
                     set < team *, teamIdComp > iTeams;
                     set < program *, programIdComp > iPrograms;
                     set < memory *, memoryIdComp > iMemories;
                     tpg.getAllNodes(teams[i], iTeams, iPrograms, iMemories);
                     cout << "TPG::replay iMem sz " << iMemories.size() << " step " << game->getStep() << " |";
                     for (auto it = iMemories.begin(); it != iMemories.end(); it++){
                        bool active = false;
                        for (int m = 0; m < MEMORY_REGISTERS; m++)
                           if ((*it)->getActive()[m])
                              active = true;
                        if(!active)
                           continue;
                        for (int m = 0; m < MEMORY_REGISTERS; m++)
                           if ((*it)->getActive()[m])
                              cout << " " << (*it)->getMem()[m];
                           else
                              cout << " " << 0;
                     }
                     cout << endl;

                  }//replay
                  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
               }
               if (tpg.animate()) {
                  size_t repeats = 40;
                  for (size_t r = 1; r <= repeats; r++){
                     for (size_t d = teamPath.size(); d <= teamPath.size(); d++){
                        tpg.printGraphDot(teams[i], saveFrame, e, game->getStep(), d, allPrograms, winningPrograms, decisionFeatures, decisionMemories, teamPath);
#if !defined(CCANADA) && !defined(HPCC)
                        if (tpg.visual())
                           game->display_function(e, (atomicProgram->action()*-1)-1, (atomicProgram->memGet())->getMem()[0]);
#endif
                        char filename[80];
                        sprintf(filename, "%s_%05d_%03d_%05d_%05d.tga","replay/frames/gl", (int)saveFrame, (int)e, game->getStep(), (int)d);
#if !defined(CCANADA) && !defined(HPCC)
                        game->saveScreenshotToFile(filename, 1200, 1200);
#endif
                        saveFrame++;
                     }
                     this_thread::sleep_for(std::chrono::milliseconds(10));
                  }
               }
               runTimeStats[tpg.numFitMode()] = runTimeStats[tpg.numFitMode()]/game->getStep();
               runTimeStats[tpg.numFitMode() + 1] = runTimeStats[tpg.numFitMode() + 1]/game->getStep();
               runTimeStats[tpg.numFitMode() + 2] = teams[i]->membersRunEntropy();
               runTimeInts[POINT_AUX_INT_TASK] = tpg.activeTask();
               runTimeInts[POINT_AUX_INT_FITMODE] = tpg.hostFitnessMode();
               runTimeInts[POINT_AUX_INT_PHASE] = tpg.phase();
               runTimeInts[POINT_AUX_INT_ENVSEED] = e + eStart;
               evalResult += to_string(teams[i]->id());
               evalResult += ":" + to_string(tpg.activeTask());
               for (size_t r = 0; r < runTimeStats.size(); r++)
                  evalResult += ":" + to_string(runTimeStats[r]);
               for (size_t r = 0; r < runTimeInts.size(); r++)
                  evalResult += ":" + to_string(runTimeInts[r]);
               teams[i]->getActiveMembersByRef(active);
               for (auto leiter = active.begin(); leiter != active.end(); leiter++)
                  evalResult += ":" + to_string((*leiter)->id());
               evalResult += "\n";
               if (tpg.replay()){
                  vector <behaviourType> tmpBehavSeq;
                  tpg.setOutcome(teams[i], tmpBehavSeq, runTimeStats, runTimeInts, tpg.tPickup());
               }
            }
            ////graph use
            //if (tpg.replay()) {
            //   tpg.printGraphDot(teams[i], -1, -1, -1, teamPath.size(), allPrograms, winningProgramsAll, decisionFeatures, decisionMemories, visitedTeamsAll);
            //   winningProgramsAll.clear();
            //   allPrograms.clear();
            //   teamPath.clear();
            //   tpg.printGraphDot(teams[i], -2, -2, -2, teamPath.size(), allPrograms, winningProgramsAll, decisionFeatures, decisionMemories, teamPath);
            //}
         }
      }
      if (tpg.replay()) break;
      gather(world, evalResult, 0);
   }
   delete worldState;
}
