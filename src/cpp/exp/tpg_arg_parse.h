#ifndef tpg_arg_parse_h
#define tpg_arg_parse_h
#include <TPG.h>
#include <unistd.h>

void tpg_arg_parse_001(TPG &tpg, int argc, char** argv){

   int option_char;
   while ((option_char = getopt (argc, argv, "Aa:C:D:d:F:f:g:Ii:pR:r:Ss:T:t:vVw")) != -1)
      switch (option_char)
      {
         case 'A': tpg.animate(true); break;
         case 'a': tpg.activeTask(atoi(optarg)); break;
         case 'C': tpg.checkpoint(true); tpg.checkpointInPhase(atoi(optarg)); break;
         case 'D': tpg.dim(atoi(optarg)); break;
         case 'd': tpg.taskSwitchMod(atoi(optarg)); break;
         case 'F': tpg.function(atoi(optarg)); break;
         case 'f': tpg.hostFitnessMode(atoi(optarg)); break;
         case 'g': tpg.seed2(atoi(optarg)); break;
         case 'I': tpg.skipIntrons(false); break;
         case 'p': tpg.partiallyObservable(true); break;
         case 'i': tpg.id(atoi(optarg)); break;
         case 'R': tpg.replay(true); tpg.hostToReplay(atoi(optarg)); break;
         case 'r': tpg.reproduce(true); tpg.readGenTimes(optarg); break;
         case 'S': tpg.stateful(true); break;
         case 's': tpg.seed(atoi(optarg)); break;
         case 'T': tpg.tMain(atoi(optarg)); break;
         case 't': tpg.tPickup(atol(optarg)); tpg.tStart(tpg.tPickup() + 1); break;
         case 'v': tpg.verbose(true); break;
         case 'V': tpg.visual(true); break;
         case 'w': tpg.writeCheckpoints(true); break;
         case '?': exit(0); break;
      }
}

#endif
