#include <TPG.h>
#include "tpg_arg_parse.h"
#include "../env/cartPole/cartPole.h"
#include <boost/mpi.hpp>
#include <chrono>

#define CHKP_MOD 100
#define PRINT_MOD 1
#define NUM_FIT_MODE 1
#define NUM_POINT_AUX_DOUBLES 3 //raw reward, mean visitedTeams, decisionInstructions);

namespace mpi = boost::mpi;

int main(int argc, char** argv) {
   mpi::environment env(argc, argv);
   mpi::communicator world;
   TPG tpg(-1);
   tpg_arg_parse_001(tpg, argc, argv);
   ostringstream os; //logging
   tpg.numFitMode(1);
   tpg.hostFitnessMode(0);
   tpg.dim(CARTPOLE_DIM_PO);
   tpg.numPointAuxDouble(NUM_POINT_AUX_DOUBLES);
   tpg.numAtomicActions(CARTPOLE_NUM_ACTION);
   tpg.phase(_TRAIN_PHASE);
   tpg.setParams(world.rank() == 0);

   if (world.rank() == 0) { //Master Process
      mt19937 rng(tpg.seed());
      string my_string = "MAIN";

      //time logging
      auto startGen = chrono::system_clock::now(); chrono::duration<double> endGen = chrono::system_clock::now()-startGen;
      auto startInit = chrono::system_clock::now(); chrono::duration<double> endInit;
      auto startGenTeams = chrono::system_clock::now(); chrono::duration<double> endGenTeams;
      auto startSelTeams = chrono::system_clock::now(); chrono::duration<double> endSelTeams;
      auto startEval = chrono::system_clock::now(); chrono::duration<double> endEval;
      auto startChkp = chrono::system_clock::now(); chrono::duration<double> endChkp;
      auto startReport = chrono::system_clock::now(); chrono::duration<double> endReport;

      map <long, team*> rootTeams; vector < team* > teamsToEval; vector < team* > teamsThisEval;
      vector < string > all_strings; vector <string> splitString; string resultLine; string s;
      vector <long> active; vector <behaviourType> tmpBehavSeq; vector <double> r_rewards;

      //initialization
      if (tpg.checkpoint())
         tpg.readCheckpoint(tpg.tPickup(), tpg.checkpointInPhase(), -1, false, ""); //setRoots
      else{
         startInit = chrono::system_clock::now();
         tpg.initTeams(rng); //setRoots
         endInit = chrono::system_clock::now()-startInit;
      }

      /* Main training loop. */
      for (long t = tpg.tStart(); t <= tpg.tMain(); t++)
      {
         startGen = chrono::system_clock::now();
         /* replacement *********************************************************************/
         startGenTeams = chrono::system_clock::now();
         if (tpg.numRoot() < tpg.rSize()) tpg.genTeams(t, rng);
         endGenTeams = chrono::system_clock::now()-startGenTeams;

         /* evaluation **********************************************************************/
         startEval = chrono::system_clock::now();
         tpg.phase(_TRAIN_PHASE);
         tpg.resetOutcomes(tpg.phase(),true);//roots only, all will be re-evaluated
         //assign policies to evaluators
         tpg.getTeams(rootTeams,true);
         int evaluator = 1; teamsToEval.clear(); teamsThisEval.clear();
         for (auto it = rootTeams.begin(); it != rootTeams.end(); it++)
            if (it->second->numOutcomes(tpg.phase(),tpg.hostFitnessMode()) < tpg.numStoredOutcomesPerHost(tpg.phase()))
               teamsToEval.push_back(it->second);
         os << "t " << t << " phs " << tpg.phase() << " hfm " << tpg.hostFitnessMode() << " aTsk " << tpg.activeTask() << " tToEvl " << teamsToEval.size() << endl;
         size_t teamsPerEvaluator = teamsToEval.size()/(world.size() - 1);
         size_t remainder = teamsToEval.size() % (world.size() - 1);

         for (auto it = teamsToEval.begin(); it != teamsToEval.end(); it++){
            teamsThisEval.push_back(*it);
            if ((remainder > 0 && teamsThisEval.size() == teamsPerEvaluator+1) ||
                  (remainder == 0 && teamsThisEval.size() == teamsPerEvaluator) || 
                  next(it) == teamsToEval.end()){
               s = "";
               tpg.writeCheckpoint(s, teamsThisEval);
               world.send(evaluator++, 0, s);
               teamsThisEval.clear();
               if (remainder > 0) remainder--;
            }
         }

         //no more teams to evaluate, so let the rest of the procs know they are not needed this round
         while (evaluator <= (world.size() - 1)){
            s = "x"; 
            world.send(evaluator++, 0, s);
         }

         //collect evaluation result from each evaluator
         all_strings.clear();
         gather(world, my_string, all_strings, 0);
         for (int proc = 1; proc < world.size(); proc++){
            if (!all_strings[proc].empty()){
               istringstream f(all_strings[proc]);
               while(getline(f, resultLine)){
                  split(resultLine, ':', splitString);
                  //parse the string
                  size_t s = 0;
                  long rslt_id = atol(splitString[s++].c_str());
                  for (size_t i = 0; i < tpg.numPointAuxDouble(); i++)
                     r_rewards.push_back(atof(splitString[s++].c_str()));
                  while (s < splitString.size())
                     active.push_back(atol(splitString[s++].c_str()));
                  rootTeams[rslt_id]->updateActiveMembersFromIds(active);
                  tpg.setOutcome(rootTeams[rslt_id], tmpBehavSeq, r_rewards, tpg.phase(), t);
                  active.clear();
                  r_rewards.clear();
               }
            }
         }
         endEval = chrono::system_clock::now()-startEval;

         /* selection **********************************************************************/
         startSelTeams = chrono::system_clock::now();
         tpg.selTeams(t, true, t > 1 ? floor(chrono::duration_cast<chrono::milliseconds>(endGen).count()) : 0);//also does some reporting
         endSelTeams = chrono::system_clock::now()-startSelTeams;

         /* accounting and reporting **********************************************************************/
         startReport = chrono::system_clock::now();
         tpg.printTeamInfo(t, tpg.phase(), true);
         if (t == tpg.tStart() || t % tpg.rSize() == 0)
            tpg.updateMODESFilters(t, t == tpg.tStart(), false);
         endReport = chrono::system_clock::now()-startReport;

         /* checkpoint **********************************************************************/
         startChkp = chrono::system_clock::now();
         if (t==1000 || t % CHKP_MOD == 0)
            tpg.writeCheckpoint(t, _TRAIN_PHASE, false, -1);
         endChkp = chrono::system_clock::now()-startChkp;

         endGen = chrono::system_clock::now()-startGen;

         /* print gen time ************************************************************************/

         os << setprecision(5) << fixed << "gTime t " << t << " sec " << endGen.count() << " evl " << endEval.count();
         os << " gTms " << endGenTeams.count() << " iTms " << endInit.count() << " sTms " << endSelTeams.count();
         os << " chkp " << endChkp.count() << " rprt " << endReport.count() << " lost " << endGen.count() -
            (endEval.count() + endGenTeams.count() + endInit.count() + endSelTeams.count() + endChkp.count() + endReport.count()) << endl;
         tpg.printOss(os);

         startGen = chrono::system_clock::now();
         if (t % PRINT_MOD == 0) 
         tpg.printOss();
         t++;
      }
      for (int ev = 1; ev <= world.size() - 1; ev++){
         s = "done";
         world.send(ev, 0, s);
      }
      tpg.finalize();
      tpg.printOss();
      cout << "Goodbye cruel world." << endl;
   }
   else { //Evaluator Process
      cartPole *game = new cartPole(tpg.seed());
      state *worldState = new state(CARTPOLE_DIM);
      vector<double> rewards; rewards.reserve(tpg.numPointAuxDouble()); rewards.resize(tpg.numPointAuxDouble());
      set <learner*, LearnerBidLexicalCompare> learnersRanked;
      learner * atomicLearner;
      long decisionInstructions;
      set <team*, teamIdComp> visitedTeams;
      int numEval = tpg.numStoredOutcomesPerHost(_TRAIN_PHASE);
      int step = 0;
      vector <team*> teams;
      string evalResult = "";
      string checkpointString = "";
      while (checkpointString.compare("done") != 0){
         //receive from p0
         world.recv(0, 0, checkpointString);
         evalResult = "";
         set <learner*, learnerIdComp> active;
         if (checkpointString.compare("x") != 0 && checkpointString.compare("done") != 0){
            tpg.readCheckpoint(-1, _TRAIN_PHASE, -1, true, checkpointString); //setRoots
            tpg.getTeams(teams,true);
            for (size_t i = 0; i < teams.size(); i++){
               tpg.markEffectiveCode(teams[i], true);
               for (int e = 0; e < numEval; e++){
                  fill(rewards.begin(), rewards.end(), 0);
                  tpg.clearMemory();
                  game->reset();
                  step = 0;
                  while (!game->fail() && step < CARTPOLE_MAX_STEP){
                     worldState->setState(game->getStateVec());
                     atomicLearner = tpg.getAction(teams[i], worldState, true, visitedTeams, decisionInstructions, step);
                     rewards[tpg.hostFitnessMode()] += game->update((atomicLearner->action()*-1)-1);//atomic actions are represented as negatives in tpg
                     rewards[tpg.numFitMode()] += visitedTeams.size();
                     rewards[tpg.numFitMode() + 1] += decisionInstructions;
                     step++;
                  }
                  rewards[tpg.numFitMode()] = rewards[tpg.numFitMode()]/step;
                  rewards[tpg.numFitMode() + 1] = rewards[tpg.numFitMode() + 1]/step;
                  evalResult += to_string(teams[i]->id());
                  for (size_t r = 0; r < rewards.size(); r++)
                     evalResult += ":" + to_string(rewards[r]);
                  teams[i]->getActiveMembersByRef(active);
                  for (auto leiter = active.begin(); leiter != active.end(); leiter++)
                     evalResult += ":" + to_string((*leiter)->id());
                  evalResult += "\n";
               }
            }
         }
         gather(world, evalResult, 0);
      }
      delete game;
      delete worldState;
   }
   return 0;
}
