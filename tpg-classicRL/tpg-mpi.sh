#!/bin/bash

# start tpg ###################################################

#from scratch
mode=$1
activeTask=$2
seed=$3
numProc=$4

if [ $mode -eq 3 ]; then
echo "Starting run $seed..."
./genScript.pl tpg $seed
#mini experiment to test the importance of temporal memory in partially-observable cartPole
mpirun --oversubscribe -np $numProc ../build/release/cpp/exp/tpgExpClassicRL_MPI -D 3 -w -p -T 500 -s $seed -a $activeTask 1> tpg.crl.$seed.noMemory.std 2> tpg.crl.$seed.noMemory.err # without memory
mpirun --oversubscribe -np $numProc ../build/release/cpp/exp/tpgExpClassicRL_MPI -D 3 -w -p -S -T 500 -s $seed -a $activeTask  1> tpg.crl.$seed.memory.std 2> tpg.crl.$seed.memory.err # with memory
fi

if [ $mode -eq 0 ]; then
   echo "Starting run $seed..."
   ./genScript.pl tpg $seed
   #run from scratch (in background)
   seed2=0
   mpirun --oversubscribe -np $numProc ../build/release/cpp/exp/tpgExpClassicRL_MPI -D 3 -w -p -S -T 1000000 -s $seed -g $seed2 -a $activeTask 1> tpg.crl.$seed.$$.std 2> tpg.crl.$seed.$$.err &
fi

if [ $mode -eq 1 ]; then
   ./genScript.pl tpg $seed
   #replay
   phs=2
   seed2=0
   if ls replay/frames/* 1> /dev/null 2>&1; then rm replay/frames/*; fi
   if ls replay/graphs/* 1> /dev/null 2>&1; then rm replay/graphs/*; fi
   bestScore=$(grep setElTmsMT tpg.crl.${seed}.*.std | grep " phs $phs " | awk -F"mnOutTst tsk_1_a" '{print $2}' | awk '{print $1}' | sort -n | uniq | tail -n 1)
   #seed=$(grep -iRl "tsk_1_a ${bestScore}" *.std | head -n 1 | cut -d '.' -f 3)
   t=$(grep  "mnOutTst tsk_1_a ${bestScore} " tpg.crl.${seed}.*.std | grep " phs $phs " | head -n 1 | awk -F" t " '{print $2}' | awk '{print $1}')
   tm=$(grep "mnOutTst tsk_1_a ${bestScore} " tpg.crl.${seed}.*.std | grep " phs $phs " | grep " t $t " | head -n 1 | awk -F" id " '{print $2}' | awk '{print $1}')
   mpirun --oversubscribe -np 1 ../build/release/cpp/exp/tpgExpClassicRL_MPI -D 3 -V -A -p -S -R $tm -C $phs -t $t -S -T 1000000 -s $seed -g $seed2 -a $activeTask \
      1> tpg.crl.$seed.t$activeTask.replay.std 2> tpg.crl.$seed.t$activeTask.replay.err &
fi
