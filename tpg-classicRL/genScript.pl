#!/usr/bin/perl

use strict;

my $maxRunTimeComplexityIns = 1000000;
my $maxRunTimeComplexityTms = 1000000;
#Team parameters
my $initialTeamSize = 15;
my $maxInitialTeamSize = 30; #max (initial) team size
my $maxTeamSize = 1000000;
my $maxTeamsPerGraph = 1000000;
#team mod probabilities
my $pAtomic = 0.99;
my $pmd = 0.7;
my $pma = 0.6;
my $pmm = 0.2;
my $pmn = 0.1;
my $pms = 0.1;
my $pmw = 0.1;
my $pmx = 0.2;

#Program parameters
my $continuousOutput = 1;
my $maxInitialProgSize = 10;
my $maxProgSize = 1000000; #max program size
#program mod probabilities
my $pBidMutate = 1.0;
my $pBidSwap = 1.0;
my $pBidDelete = 0.5;
my $pBidAdd = 0.4;

#GA parameters
my $Rsize = 200;
my $Rgap = 1.0; #fraction of Rsize - (numElite * numTask)
my $numElite = 50;
my $numEvalPerGeneration = 0;
my $numStoredOutcomesPerHost_TRAIN = 100;
my $numStoredOutcomesPerHost_VALIDATION = 100; #NA
my $numStoredOutcomesPerHost_TEST = 100; 
my $numProfilePoints = 100; #NA
my $pAddProfilePoint = 0.0; #NA
my $paretoEpsilonTeam = 0.001;

my $diversityMode = 0; 
my $maxGenMillis = 0; 

###################################################################################################
if(scalar(@ARGV) != 2)
{
   die "usage: genScript.pl prefix seed";
}

my $prefix = $ARGV[0];
my $seed = $ARGV[1];

my $argFile;

$argFile = "$prefix.$seed.arg";

open(ARG, ">$argFile") || die "cannot open $argFile";

print ARG "maxRunTimeComplexityIns $maxRunTimeComplexityIns\n";
print ARG "maxRunTimeComplexityTms $maxRunTimeComplexityTms\n";
print ARG "initialTeamSize $initialTeamSize\n";
print ARG "maxInitialTeamSize $maxInitialTeamSize\n";
print ARG "maxTeamSize $maxTeamSize\n";
print ARG "maxTeamsPerGraph $maxTeamsPerGraph\n";
print ARG "pAtomic $pAtomic\n";
print ARG "pmd $pmd\n";
print ARG "pma $pma\n";
print ARG "pmm $pmm\n";
print ARG "pmn $pmn\n";
print ARG "pms $pms\n";
print ARG "pmw $pmw\n";
print ARG "pmx $pmx\n";

print ARG "continuousOutput $continuousOutput\n";
print ARG "maxInitialProgSize $maxInitialProgSize\n";
print ARG "maxProgSize $maxProgSize\n";
print ARG "pBidMutate $pBidMutate\n";
print ARG "pBidSwap $pBidSwap\n";
print ARG "pBidDelete $pBidDelete\n";
print ARG "pBidAdd $pBidAdd\n";

print ARG "Rsize $Rsize\n";
print ARG "Rgap $Rgap\n";
print ARG "numElite $numElite\n";
print ARG "numEvalPerGeneration $numEvalPerGeneration\n";
print ARG "numStoredOutcomesPerHost_TRAIN $numStoredOutcomesPerHost_TRAIN\n";
print ARG "numStoredOutcomesPerHost_VALIDATION $numStoredOutcomesPerHost_VALIDATION\n";
print ARG "numStoredOutcomesPerHost_TEST $numStoredOutcomesPerHost_TEST\n";
print ARG "numProfilePoints $numProfilePoints\n";
print ARG "pAddProfilePoint $pAddProfilePoint\n";
print ARG "paretoEpsilonTeam $paretoEpsilonTeam\n";

print ARG "diversityMode $diversityMode\n";
print ARG "maxGenMillis $maxGenMillis\n";

close(ARG);

