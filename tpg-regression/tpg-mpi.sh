#!/bin/bash

# start tpg ###################################################

#from rgrratch 
seed=$1
numProc=$2
func=$3
dim=$4

echo "Starting run $seed..."
./genScript.pl tpg $seed

mpirun --oversubscribe -np $numProc ../build/release/cpp/exp/tpgExpRGR_MPI -S -T 1000000 -s $seed -f 0 -F $func -D $dim 1> tpg.rgr.$seed.$$.std 2> tpg.rgr.$seed.$$.err &

#debug
#mpirun --oversubscribe -np $numProc xterm -hold -e gdb -ex run --args ../build/release/cpp/exp/tpgExpRGR_MPI -S -T 1000000 -s $seed -f 0 -F $func -D $dim 1> tpg.rgr.$seed.$$.std 2> tpg.rgr.$seed.$$.err &

